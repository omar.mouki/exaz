import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class UserEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + "Auth/";

//?---------------------------------------Auth -------------------------

  static const String AUTH_API = FULL_URL + "Token";
  static const String REGISTER_API = FULL_URL + "Register";
  static const String IS_PHONE_EXISIT_API = FULL_URL + "IsPhoneNumberExist";
  static const String VERIFICATION_CODE_API =
      FULL_URL + "ValidateActivationCode";
  static const String RESEND_ACTIVATION_CODE_API =
      FULL_URL + "ResendActivationCode";
  static const String CHANGE_PHONE_API = FULL_URL + "ChangePhoneNumber";
  static const String CHANGE_PASSWORD_API = FULL_URL + "ChangePassword";
  static const String FORGET_PASSWORD_API = FULL_URL + "ForgetPassword";
  static const String VERIFY_FORGET_PASSWORD_CODE_API =
      FULL_URL + "VerifyForgetPasswordCode";
  static const String UPDATE_FCM_TOKEN_API = FULL_URL + "CreateUpdateFCMToken";
  static const String RESET_PASSWORD_API = FULL_URL + "ResetPassword";
}
