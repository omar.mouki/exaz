import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class StoreEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + 'Store/';

  static const String CREATE_UPDATE_STORE_API = FULL_URL + "CreateUpdateStore";
  static const String CREATE_UPDATE_PRODUCT_API =
      FULL_URL + "CreateUpdateProduct";
  static const String REMOVE_PRODUCT_API = FULL_URL + "RemoveProduct";
  static const String GET_CATEGORIES_API = FULL_URL + "GetCategories";
  static const String Get_Stores = FULL_URL + "GetStores";
  static const String Get_Store_Details = FULL_URL + "GetStoreDetails";
  static const String Get_Store_Branches = FULL_URL + "GetStoreBranches";
  static const String GET_STORE_PRODUCTS = FULL_URL + "GetStoreProducts";
  static const String Get_Products_Categories =
      FULL_URL + "GetProductsCategories";
  static const String Request_To_Be_Store = FULL_URL + "RequestToBeStore";
  static const String Get_Product_Details = FULL_URL + "GetProductDetails";
}
