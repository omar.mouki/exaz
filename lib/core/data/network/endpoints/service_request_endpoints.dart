import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class ServiceRequestEndPonts {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + 'ServiceRequest/';

  static const String CREATE_NEW_REQUEST = FULL_URL + "CreateNewRequest";
  static const String GetService_Request_Categories =
      FULL_URL + "GetServiceRequestCategories";
  static const String Get_My_Service_Requests =
      FULL_URL + "GetMyServiceRequests";
  static const String Get_Service_Request = FULL_URL + "GetServiceRequest";
  static const String Remove_Service_Request =
      FULL_URL + "RemoveServiceRequest";
  static const String Update_Service_Request =
      FULL_URL + "UpdateServiceRequest";
}
