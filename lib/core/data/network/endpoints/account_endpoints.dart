import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class AccountEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + 'Account/';

  static const String GET_PROFILE_BY_USERID_API =
      FULL_URL + "GetProfileInformation";
  static const String GET_MY_PROFILE_API = FULL_URL + "GetMyProfileInformation";
  static const String GET_MY_ACCOUNT_API = FULL_URL + "GetMyAccountInformation";
  static const String UPDATE_MY_ACCOUNT_API = FULL_URL + "UpdateMyAccount";
}
