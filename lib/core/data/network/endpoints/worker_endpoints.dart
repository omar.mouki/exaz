import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class WorkerEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + 'Worker/';

  static const String GET_WORKER_PROFRSSIONS_API =
      FULL_URL + "GetWorkerProfessions";
  static const String GET_WORKERS_API = FULL_URL + "GetWorkers";
  static const String CREATE_UPDATE_WORKER_API =
      FULL_URL + "CreateUpdateWorker";
  static const String GET_WORKER_DETAILS_API = FULL_URL + "GetWorkerDetails";
  static const String GET_MY_DETAILS_API = FULL_URL + "GetMyDetails";
}
