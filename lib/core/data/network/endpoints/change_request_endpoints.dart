import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class ChangeRequestEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + "ChangeRequest/";

  static const String GET_SERVICE_PROVIDER_TYPE_API =
      FULL_URL + "GetServiceProviderTypes";
  static const String REQUEST_TO_SERVICE_PROVIDER_API =
      FULL_URL + "RequestToBeServiceProvider";
}
