import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class ContractorEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + "Contractor/";

  static const String CREATE_UPDATE_CONTRACTOR_API =
      FULL_URL + "CreateUpdateContractor";
  static const String CREATE_UPDATE_PROJECT_API =
      FULL_URL + "CreateUpdateProject";

  static const String REMOVE_PROJECT_API = FULL_URL + "RemoveProject";
  static const String GET_CONTRACTORS_API = FULL_URL + "GetContractors";
  static const String GET_CATEGORIES_API = FULL_URL + "GetCategories";
  static const String GET_CONTRACTORS_PROJECTS_API =
      FULL_URL + "GetContractorProjects";
}
