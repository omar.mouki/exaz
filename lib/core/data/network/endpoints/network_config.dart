import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';

class NetworkConfig {
  static const String PAGE_SIZE = "20";
  static String getUserAgent() {
    // PackageInfo packageInfo = locator<PackageInfo>();
    // if (Platform.isIOS) {
    //   return "[EXA-IOS]/" + packageInfo.version;
    // } else if (Platform.isAndroid) {
    //   return "[EXA-Android]/" + packageInfo.version;
    //   ;
    // }
    return "";
  }

  static Map<String, String> getHeaders(Map<String, String> otherHeaders) {
    // String deviceId = locator<String>();

    Map<String, String> headers = {
      'User-Agent': getUserAgent(),
      'deviceId': '',
      //1 ar , 2 en
      'EXAA-Requested-Language': '2',
    };

    headers.addAll(otherHeaders);
    return headers;
  }

  static Map<String, String> getAuthHeaders(Map<String, String> otherHeaders) {
    String _authToken =
        locator<SharedPreferencesRepository>().getSavedTokenInfo().newToken;
    // String deviceId = locator<String>();
    Map<String, String> authHeaders = {
      'User-Agent': getUserAgent(),
      // 1 ar , 2 en
      'EXAA-Requested-Language': '2',
      'Authorization': "Bearer " + _authToken,
      'deviceId': ''
    };
    authHeaders.addAll(otherHeaders);
    return authHeaders;
  }
}
