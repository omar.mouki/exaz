import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class AddressEndpoints {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + 'Address/';

  static const String GET_CITIES_API = FULL_URL + "GetCities";
  static const String GET_AREAS_API = FULL_URL + "GetAreasByCity";
  static const String GET_NATIONALITIES_API = FULL_URL + "GetNationalities";
}
