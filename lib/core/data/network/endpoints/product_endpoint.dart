import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class Product_endpoint {
  static const String FULL_URL =
      NetworkConstants.BASE_URL + NetworkConstants.BASE_API + 'Product/';
}
