import 'package:exaaz/core/data/network/endpoints/network_constants.dart';

class SERVICEPROVIDEREndpoints {
  static const String FULL_URL = NetworkConstants.BASE_URL +
      NetworkConstants.BASE_API +
      'ServiceProviderApi/';

  static const String INCREASE_VISIT_COUNT_API =
      FULL_URL + "IncreaseVisitCount";
  static const String RATE_SERVICE_PROVIDER_API =
      FULL_URL + "RateServiceProvider";
  static const String CLEAR_RATE_SERVICE_PROVIDER_API =
      FULL_URL + "learMyRatingToServiceProvider";
  static const String ADD_TO_FAVORITE_API =
      FULL_URL + "AddServiceProviderToFavorite";
  static const String REMOVE_FROM_FAVORITE_API =
      FULL_URL + "RemoveServiceProviderToFavorite";
}
