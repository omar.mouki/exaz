import 'package:exaaz/core/data/network/endpoints/network_config.dart';
import 'package:exaaz/core/data/network/endpoints/store_endpoints.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';

class ProductRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> getCategories({required String storeId}) async {
    final query = "?storeId=$storeId";
    return NetworkUtil.get(
      url: StoreEndpoints.Get_Products_Categories + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getProductDetails({required int productId}) async {
    final query = '?productId=$productId';
    return NetworkUtil.get(
      url: StoreEndpoints.Get_Product_Details + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }
}
