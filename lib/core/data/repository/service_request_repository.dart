import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:exaaz/core/data/network/endpoints/network_config.dart';
import 'package:exaaz/core/data/network/endpoints/service_request_endpoints.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';

class ServiceRequestRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> createNewRequest({
    required ServiceRequestModel serviceRequestModel,
  }) async {
    Map<String, String> fields = {
      'title': serviceRequestModel.title,
      'description': serviceRequestModel.description,
      'startDate': serviceRequestModel.startDate,
      'categoryId': serviceRequestModel.requestCategoryId.toString(),
      'projectOwnerName': serviceRequestModel.projectOwnerName,
      'projectOwnerPhone': serviceRequestModel.projectOwnerName,
      'projectManagerName': serviceRequestModel.projectManagerName,
      'projectManagerPhone': serviceRequestModel.projectManagerPhone,
      'projectLocation': serviceRequestModel.projectLocation,
      'requestStatus': serviceRequestModel.requestState.toString(),
      'requestType': serviceRequestModel.requestType.toString(),
      'materialsType': serviceRequestModel.materialsType ?? '',
    };

    Map<String, List<String>?> files = {
      'images': serviceRequestModel.images,
      'pdFs': serviceRequestModel.pdFs,
    };
    return NetworkUtil.postMultipartArry(
      url: ServiceRequestEndPonts.CREATE_NEW_REQUEST,
      headers: NetworkConfig.getAuthHeaders(headers),
      fields: fields,
      files: files,
    ).then((dynamic response) {
      return CommonResponse.fromJson(response);
    });
  }

  Future<CommonResponse> getServiceRequestCategories() async {
    return NetworkUtil.get(
      url: ServiceRequestEndPonts.GetService_Request_Categories,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse.fromJson(response);
    });
  }

  Future<CommonResponse> getMyServiceRequests({
    required int pageIndex,
    required int pageSize,
  }) async {
    String query = '?pageIndex=$pageIndex&pageSize=$pageSize';

    return NetworkUtil.get(
      url: ServiceRequestEndPonts.Get_My_Service_Requests + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getServiceRequestDetails({
    required int id,
  }) async {
    String query = '?id=$id';

    return NetworkUtil.get(
      url: ServiceRequestEndPonts.Get_Service_Request + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> removeServiceRequest({
    required int id,
  }) async {
    String query = '?id=$id';

    return NetworkUtil.post(
      url: ServiceRequestEndPonts.Remove_Service_Request + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }
}
//GetServiceRequestCategories