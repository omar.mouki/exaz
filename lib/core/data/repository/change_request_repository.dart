import 'package:exaaz/core/data/network/endpoints/network_config.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/change_request_endpoints.dart';

class ChangeRequestRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> getServiceProviderTypes() async {
    return NetworkUtil.get(
      url: ChangeRequestEndpoints.GET_SERVICE_PROVIDER_TYPE_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> requestToBeServiceProvider({
    required int requestedServiceType,
    required String companyTitle,
    required String fullName,
    required String commercialRegistrationNumber,
    required String residenceNumber,
    required String residencePicture,
  }) async {
    Map<String, String> fields = {
      "requestedServiceType": requestedServiceType.toString(),
      "companyTitle": companyTitle,
      "fullName": fullName,
      "commercialRegistrationNumber": commercialRegistrationNumber,
      "residenceNumber": residenceNumber
    };

    Map<String, String> files = {
      'residencePicture': residencePicture,
    };

    return NetworkUtil.postMultipart(
      url: ChangeRequestEndpoints.REQUEST_TO_SERVICE_PROVIDER_API,
      headers: NetworkConfig.getAuthHeaders(headers),
      fields: fields,
      files: files,
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }
}
