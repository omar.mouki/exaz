import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/network/endpoints/network_config.dart';
import 'package:exaaz/core/data/network/endpoints/store_endpoints.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';

class StoreRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> getCategories() async {
    return NetworkUtil.get(
      url: StoreEndpoints.GET_CATEGORIES_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getStories(
      {required int pageIndex,
      required int pageSize,
      required int categoryId}) async {
    int? cityId =
        locator<SharedPreferencesRepository>().getProfileInfo()?.cityId;
    String query =
        '?pageIndex=$pageIndex&pageSize=$pageSize&CategoryId=$categoryId';
    if (cityId != null) {
      query = query + '&cityId=$cityId';
    }

    return NetworkUtil.get(
      url: StoreEndpoints.Get_Stores + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getStoreDetails({
    required int storeId,
  }) async {
    String query = '?storeId=$storeId';
    return NetworkUtil.get(
      url: StoreEndpoints.Get_Store_Details + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> getStoreBranches({
    required int storeId,
  }) async {
    String query = '?storeId=$storeId';
    return NetworkUtil.get(
      url: StoreEndpoints.Get_Store_Branches + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getStoreProducts(
      {required int pageIndex,
      required int pageSize,
      required int storeId,
      required int categoryId}) async {
    // GetStoreProducts?pageIndex=0&pageSize=1&storeId=1&categoryId=1"
    String query =
        '?pageIndex=$pageIndex&pageSize=$pageSize&storeId=$storeId&categoryId=$categoryId';
    return NetworkUtil.get(
      url: StoreEndpoints.GET_STORE_PRODUCTS + query,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> requestToBeStore({
    required String name,
    required String commercialRecord,
    required int categoryId,
  }) async {
    Map<String, String> fields = {
      "name": name,
      "categoryId": categoryId.toString(),
    };

    print("params$fields");

    Map<String, String> files = {
      'commercialRecord': commercialRecord,
    };

    return NetworkUtil.postMultipart(
            url: StoreEndpoints.Request_To_Be_Store,
            headers: NetworkConfig.getAuthHeaders(headers),
            fields: fields,
            files: files)
        .then((dynamic response) {
      print("UPDATE_MY_ACCOUNT_API : response is $response");
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  // Future<CommonResponse> requestToBeStore({
  //   required String name,
  //   required String commercialRecord,
  //   required int categoryId,
  // }) async {
  //   return NetworkUtil.post(
  //       url: StoreEndpoints.Request_To_Be_Store,
  //       headers: NetworkConfig.getAuthHeaders(headers),
  //       body: jsonEncode({
  //         "name": name,
  //         "commercialRecord": commercialRecord,
  //         "categoryId": categoryId,
  //       })).then((dynamic response) {
  //     return CommonResponse<Iterable>.fromJson(response);
  //   });
  // }

}
