import 'dart:async';
import 'dart:convert';

import 'package:exaaz/core/data/models/profile_info.dart';
import 'package:exaaz/core/data/models/token_info.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../app/locator.dart';
import '../models/user_profile.dart';

class SharedPreferencesRepository {
  static const String PREF_TYPE_BOOL = "BOOL";
  static const String PREF_TYPE_INTEGER = "INTEGER";
  static const String PREF_TYPE_DOUBLE = "DOUBLE";
  static const String PREF_TYPE_STRING = "STRING";

  static const String PREF_IS_LOGGED_IN = "IS_LOGGED_IN";
  static const String PREF_IS_GUEST_CLICKED = "IS_GUEST_CLICKED";
  static const String PREF_IS_LIVE_LOCATION = "IS_LIVE_LOCATION";
  static const String PREF_IS_SP = "IS_SP";
  static const String PREF_USER = "USER";
  static const String PREF_PROFILE = "PROFILE";
  static const String PREF_TOKEN_INFO = "TOKEN_INFO";
  static const String PREF_LOCAL = "local";

  SharedPreferences? _preferences;

  static final SharedPreferencesRepository _instance =
      SharedPreferencesRepository._internal();

  factory SharedPreferencesRepository() => _instance;

  SharedPreferencesRepository._internal() {
    _preferences = locator<SharedPreferences>();
  }
  Future<void> logout() async {
    setLoggedIn(isLoggedIn: false);
    saveTokenInfo(
        tokenInfo:
            TokenInfo(expirationDate: '', newToken: '', refrechToken: ''));
    saveProfileInfo(
        profile: ProfileInfo(
            accountCreatedAt: '',
            city: '',
            cityId: -1,
            email: '',
            fullName: '',
            isRequestedToBeServiceProvider: false,
            isServiceProvider: false,
            phoneNumber: '',
            picture: ''));
    saveUserInfo(
        user: UserProfile(
            cityName: '',
            email: '',
            fullName: '',
            isProfileComplete: false,
            isServiceProvidor: false,
            phoneNumber: '',
            picture: '',
            requestApproved: false,
            requestedToBeServiceProvider: false,
            serviceProviderTypeId: -1,
            serviceProviderTypeName: '',
            whatsappNumber: ''));
  }

  void setLoggedIn({required bool isLoggedIn}) => _setPreference(
      prefName: PREF_IS_LOGGED_IN,
      prefValue: isLoggedIn,
      prefType: PREF_TYPE_BOOL);

  void setGusetClicked({required bool isGuest}) => _setPreference(
      prefName: PREF_IS_GUEST_CLICKED,
      prefValue: isGuest,
      prefType: PREF_TYPE_BOOL);

  bool getLoggedIn() {
    if (_preferences!.containsKey(PREF_IS_LOGGED_IN)) {
      return _preferences!
          .getBool(SharedPreferencesRepository.PREF_IS_LOGGED_IN)!;
    } else {
      return false;
    }
  }

  void setIsSp({required bool isSP}) => _setPreference(
      prefName: PREF_IS_SP, prefValue: isSP, prefType: PREF_TYPE_BOOL);

  bool getIsSP() {
    if (_preferences!.containsKey(SharedPreferencesRepository.PREF_IS_SP)) {
      return _preferences!.getBool(SharedPreferencesRepository.PREF_IS_SP)!;
    } else {
      return false;
    }
  }

  void setIsLiveLocationEnabled({required bool isLiveLocationEnabled}) =>
      _setPreference(
          prefName: PREF_IS_LIVE_LOCATION,
          prefValue: isLiveLocationEnabled,
          prefType: PREF_TYPE_BOOL);

  bool getIsLiveLocationEnabled() {
    if (_preferences!
        .containsKey(SharedPreferencesRepository.PREF_IS_LIVE_LOCATION)) {
      return _preferences!
          .getBool(SharedPreferencesRepository.PREF_IS_LIVE_LOCATION)!;
    } else {
      return false;
    }
  }

  /// Save and Get Logged User Info
  void saveUserInfo({required UserProfile user}) {
    String strUser = user.toJson();
    _setPreference(
        prefName: PREF_USER, prefValue: strUser, prefType: PREF_TYPE_STRING);
  }

  UserProfile getUserInfo() {
    // Map userMap = jsonDecode();
    UserProfile user =
        UserProfile.fromJson(jsonDecode(_preferences!.getString(PREF_USER)!));
    return user;
  }

  void saveProfileInfo({required ProfileInfo profile}) {
    String strUser = profile.toJson();
    _setPreference(
        prefName: PREF_PROFILE, prefValue: strUser, prefType: PREF_TYPE_STRING);
  }

  ProfileInfo? getProfileInfo() {
    // Map userMap = jsonDecode();
    if (_preferences!.containsKey(PREF_PROFILE)) {
      ProfileInfo user = ProfileInfo.fromJson(
          jsonDecode(_preferences!.getString(PREF_PROFILE)!));
      return user;
    } else {
      return null;
    }
  }

  void saveTokenInfo({required TokenInfo tokenInfo}) {
    String strTokenInfo = tokenInfo.toJson();
    _setPreference(
        prefName: PREF_TOKEN_INFO,
        prefValue: strTokenInfo,
        prefType: PREF_TYPE_STRING);
  }

  TokenInfo getSavedTokenInfo() {
    final tokenInfoString = _preferences!.getString(PREF_TOKEN_INFO);

    TokenInfo tokenInfo = TokenInfo.fromJson(jsonDecode(tokenInfoString!));
    return tokenInfo;
  }

  void saveLocal({required String local}) => _setPreference(
      prefName: PREF_LOCAL, prefValue: local, prefType: PREF_TYPE_STRING);

  String getLocal() {
    if (_preferences!.containsKey(SharedPreferencesRepository.PREF_LOCAL)) {
      return _preferences!.getString(SharedPreferencesRepository.PREF_LOCAL)!;
    } else {
      return 'ar';
    }
  }

  Future<void> clear() async {
    _preferences!.clear();
    locator.reset();
    await setupLocator();
  }

  //--------------------------------------------------- Private Preference Methods ----------------------------------------------------

  void _setPreference(
      {required String prefName,
      required dynamic prefValue,
      required String prefType}) {
    // Make switch for Preference Type i.e. Preference-Value's data-type
    switch (prefType) {
      // prefType is bool
      case PREF_TYPE_BOOL:
        {
          _preferences!.setBool(prefName, prefValue);
          break;
        }
      // prefType is int
      case PREF_TYPE_INTEGER:
        {
          _preferences!.setInt(prefName, prefValue);
          break;
        }
      // prefType is double
      case PREF_TYPE_DOUBLE:
        {
          _preferences!.setDouble(prefName, prefValue);
          break;
        }
      // prefType is String
      case PREF_TYPE_STRING:
        {
          _preferences!.setString(prefName, prefValue);
          break;
        }
    }
  }

  // dynamic _getPreference({@required prefName}) => _preferences.get(prefName);
}
