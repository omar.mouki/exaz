import 'package:exaaz/core/data/network/endpoints/network_config.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/worker_endpoints.dart';

class WorkersRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> createUpdateWorker({
    required String description,
    required String workerFees,
    required String nationalityId,
    required String cityId,
    required String professionId,
    required String picture,
  }) async {
    Map<String, String> fields = {
      'description': description,
      'workerFees': workerFees,
      'nationalityId': nationalityId,
      'cityId': cityId,
      'professionId': professionId,
    };

    Map<String, String> files = {
      'picture': picture,
    };
    return NetworkUtil.postMultipart(
      url: WorkerEndpoints.CREATE_UPDATE_WORKER_API,
      headers: NetworkConfig.getAuthHeaders(headers),
      fields: fields,
      files: files,
    ).then((response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> getWorkers(
      {required int professionId, required int pageIndex}) async {
    Map<String, String> params;
    if (professionId != -1) {
      params = {
        "professionId": professionId.toString(),
        "pageIndex": pageIndex.toString(),
        "pageSize": NetworkConfig.PAGE_SIZE
      };
    } else {
      params = {
        //  "cityId": cityId.toString(),
        "pageIndex": pageIndex.toString(),
        "pageSize": NetworkConfig.PAGE_SIZE
      };
    }
    return NetworkUtil.get(
      url:
          NetworkUtil.getUrlWithParams(WorkerEndpoints.GET_WORKERS_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getWorkerProfessions() async {
    return NetworkUtil.get(
      url: WorkerEndpoints.GET_WORKER_PROFRSSIONS_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getWorkerDetails({required int id}) async {
    Map<String, String> params = {
      "id": id.toString(),
    };
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(
          WorkerEndpoints.GET_WORKER_DETAILS_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> getMyDetails() async {
    return NetworkUtil.get(
      url: WorkerEndpoints.GET_MY_DETAILS_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }
}
