import 'package:exaaz/core/data/network/endpoints/network_config.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/contractor_endpoints.dart';

class ContractorRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> createUpdateContractor({
    required String categoryId,
    required String cityId,
    required String commercialSijel,
    required String companyName,
    required String latitude,
    required String longitude,
    required String websiteUrl,
    required String companyProfileFile,
  }) async {
    Map<String, String> fields = {
      'categoryId': categoryId,
      'cityId': cityId,
      'commercialSijel': commercialSijel,
      'companyName': companyName,
      'latitude': latitude,
      'longitude': longitude,
      'websiteUrl': websiteUrl,
    };

    Map<String, String> files = {
      'companyProfileFile': companyProfileFile,
    };
    return NetworkUtil.postMultipart(
      url: ContractorEndpoints.CREATE_UPDATE_CONTRACTOR_API,
      headers: NetworkConfig.getAuthHeaders(headers),
      fields: fields,
      files: files,
    ).then((response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> createUpdateProject({
    required String id,
    required String title,
    required String description,
    required List<String> imagesPathes,
  }) async {
    Map<String, String> fields = {
      'id': id,
      'title': title,
      'description': description,
    };

    Map<String, String> files = {};
    for (int i = 0; i < imagesPathes.length; i++) {
      files.putIfAbsent("file " + i.toString(), () => imagesPathes[i]);
    }
    return NetworkUtil.postMultipart(
      url: ContractorEndpoints.CREATE_UPDATE_PROJECT_API,
      headers: NetworkConfig.getAuthHeaders(headers),
      fields: fields,
      files: files,
    ).then((response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> removeProject({required int projectId}) async {
    Map<String, String> params = {"projectId": projectId.toString()};
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(
          ContractorEndpoints.REMOVE_PROJECT_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> getContractors({required int pageIndex}) async {
    Map<String, String> params = {
      "pageIndex": pageIndex.toString(),
      "pageSize": NetworkConfig.PAGE_SIZE
    };
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(
          ContractorEndpoints.GET_CONTRACTORS_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getCategories() async {
    return NetworkUtil.get(
      url: ContractorEndpoints.GET_CATEGORIES_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getContractorProjects(
      {required int contractorId, required int pageIndex}) async {
    Map<String, String> params = {
      "contractorId": contractorId.toString(),
      "pageIndex": pageIndex.toString(),
      "pageSize": NetworkConfig.PAGE_SIZE
    };
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(
          ContractorEndpoints.GET_CONTRACTORS_PROJECTS_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }
}
