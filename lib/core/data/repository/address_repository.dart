import 'package:exaaz/core/data/network/endpoints/network_config.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/address_endpoints.dart';

class AddressRepository {
  static Map<String, String> headers = {};

  Future<CommonResponse> getCities() async {
    return NetworkUtil.get(
      url: AddressEndpoints.GET_CITIES_API,
      headers: NetworkConfig.getHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getAreasByCity({required int cityId}) async {
    Map<String, String> params = {"cityId": cityId.toString()};
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(AddressEndpoints.GET_AREAS_API, params),
      headers: NetworkConfig.getHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }

  Future<CommonResponse> getNationalities() async {
    return NetworkUtil.get(
      url: AddressEndpoints.GET_NATIONALITIES_API,
      headers: NetworkConfig.getHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Iterable>.fromJson(response);
    });
  }
}
