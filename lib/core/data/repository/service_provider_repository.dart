import 'package:exaaz/core/data/network/endpoints/network_config.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/service_provider_endpoints.dart';

class ServiceProviderRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> increaseVisitCount({
    required int serviceProviderId,
    required int serviceProviderType,
  }) async {
    Map<String, String> params = {
      'serviceProviderId': serviceProviderId.toString(),
      'serviceProviderType': serviceProviderType.toString(),
      // 'deviceId': locator<String>()
    };
    return NetworkUtil.post(
      url: NetworkUtil.getUrlWithParams(
          SERVICEPROVIDEREndpoints.INCREASE_VISIT_COUNT_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse.fromJson(response);
    });
  }

  Future<CommonResponse> addToFavorite({
    required int serviceProviderId,
    required int serviceProviderType,
  }) async {
    Map<String, String> params = {
      'serviceProviderId': serviceProviderId.toString(),
      'serviceProviderType': serviceProviderType.toString(),
    };
    return NetworkUtil.post(
      url: NetworkUtil.getUrlWithParams(
          SERVICEPROVIDEREndpoints.ADD_TO_FAVORITE_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse.fromJson(response);
    });
  }

  Future<CommonResponse> removeFromFavorite({
    required int serviceProviderId,
    required int serviceProviderType,
  }) async {
    Map<String, String> params = {
      'serviceProviderId': serviceProviderId.toString(),
      'serviceProviderType': serviceProviderType.toString(),
    };
    return NetworkUtil.post(
      url: NetworkUtil.getUrlWithParams(
          SERVICEPROVIDEREndpoints.REMOVE_FROM_FAVORITE_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse.fromJson(response);
    });
  }

  Future<CommonResponse> rateServiceProvider({
    required int serviceProviderId,
    required int serviceProviderType,
    required int rate,
  }) async {
    Map<String, String> params = {
      'serviceProviderId': serviceProviderId.toString(),
      'serviceProviderType': serviceProviderType.toString(),
      'rate': rate.toString()
    };
    return NetworkUtil.post(
      url: NetworkUtil.getUrlWithParams(
          SERVICEPROVIDEREndpoints.RATE_SERVICE_PROVIDER_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse.fromJson(response);
    });
  }
}
