import 'package:exaaz/core/data/network/endpoints/network_config.dart';

import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/account_endpoints.dart';

class AccountRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> getProfileByUserId({required String userId}) async {
    Map<String, String> params = {"userId": userId};
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(
          AccountEndpoints.GET_PROFILE_BY_USERID_API, params),
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> getMyProfile() async {
    return NetworkUtil.get(
      url: AccountEndpoints.GET_MY_PROFILE_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> getMyAccountInformation() async {
    return NetworkUtil.get(
      url: AccountEndpoints.GET_MY_ACCOUNT_API,
      headers: NetworkConfig.getAuthHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> updateMyAccount(
      {required String fullName,
      required String email,
      required int cityId,
      required String accountImage,
      required bool removeAccountImage,
      required String phoneNumber,
      required String currentPassword,
      required String newPassword}) async {
    Map<String, String> fields = {
      "fullName": "$fullName",
      "email": "$email",
      "cityId": "$cityId",
      "removeAccountImage": "$removeAccountImage",
      "phoneNumber": "$phoneNumber",
      "currentPassword": "$currentPassword",
      "newPassword": "$newPassword",
    };

    print("params$fields");

    Map<String, String> files = {
      'accountImage': accountImage,
    };

    return NetworkUtil.postMultipart(
            url: AccountEndpoints.UPDATE_MY_ACCOUNT_API,
            headers: NetworkConfig.getAuthHeaders(headers),
            fields: fields,
            files: files)
        .then((dynamic response) {
      print("UPDATE_MY_ACCOUNT_API : response is $response");
      return CommonResponse<Map<String, dynamic>>.fromJson(response);
    });
  }
}
