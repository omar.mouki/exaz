import 'dart:convert';

import 'package:exaaz/core/data/network/endpoints/network_config.dart';
import '../../../app/locator.dart';
import '../../utils/network_util.dart';
import '../models/common_response.dart';
import '../network/endpoints/user_endpoints.dart';

class AuthenticationRepository {
  static Map<String, String> headers = {"Content-Type": "application/json"};

  Future<CommonResponse> singup({
    required String fullName,
    required String phoneNumber,
    required String email,
    required String password,
    required int cityId,
  }) async {
    return NetworkUtil.post(
        url: UserEndpoints.REGISTER_API,
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({
          "fullName": fullName,
          "phoneNumber": phoneNumber,
          "email": email,
          "password": password,
          "cityId": cityId
        })).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> tokenByMobileNumber({
    required String phoneNumber,
    required String password,
  }) async {
    return NetworkUtil.post(
        url: UserEndpoints.AUTH_API,
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({
          "grant_type": 0,
          "username": phoneNumber,
          "password": password,
          // "deviceId": locator<String>(),
        })).then((dynamic response) {
      return CommonResponse<Map<dynamic, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> tokenByFacebookToken({
    required String token,
  }) async {
    return NetworkUtil.post(
        url: UserEndpoints.AUTH_API,
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({
          "grant_type": 2,
          "accessToken": token,
          "deviceId": locator<String>(),
        })).then((dynamic response) {
      return CommonResponse<Map<dynamic, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> tokenByGoogleToken({
    required String token,
  }) async {
    return NetworkUtil.post(
        url: UserEndpoints.AUTH_API,
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({
          "grant_type": 1,
          "accessToken": token,
          "deviceId": locator<String>(),
        })).then((dynamic response) {
      return CommonResponse<Map<dynamic, dynamic>>.fromJson(response);
    });
  }

  Future<CommonResponse> isPhoneNumberExist(
      {required String phoneNumber}) async {
    Map<String, String> params = {"number": phoneNumber};
    return NetworkUtil.get(
      url: NetworkUtil.getUrlWithParams(
          UserEndpoints.IS_PHONE_EXISIT_API, params),
      headers: NetworkConfig.getHeaders(headers),
    ).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> codeVerification(
      {required String phoneNumber, required String verificationCode}) async {
    return NetworkUtil.post(
            url: UserEndpoints.VERIFICATION_CODE_API,
            headers: NetworkConfig.getHeaders(headers),
            body: jsonEncode(
                {"phoneNumber": phoneNumber, "providedCode": verificationCode}))
        .then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> resendActivationCode(
      {required String phoneNumber}) async {
    Map<String, String> params = {
      "phoneNumber": phoneNumber,
    };
    return NetworkUtil.post(
        url: NetworkUtil.getUrlWithParams(
            UserEndpoints.RESEND_ACTIVATION_CODE_API, params),
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({})).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> forgetPassword(
      {required String emailOrPhonenumber}) async {
    Map<String, String> params = {
      "emailOrPhonenumber": emailOrPhonenumber,
    };
    return NetworkUtil.post(
        url: NetworkUtil.getUrlWithParams(
            UserEndpoints.FORGET_PASSWORD_API, params),
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({})).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> verifyForgetPasswordCode(
      {required String emailOrPhonenumber,
      required String verificationCode}) async {
    return NetworkUtil.post(
        url: UserEndpoints.VERIFY_FORGET_PASSWORD_CODE_API,
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({
          "phoneNumber": emailOrPhonenumber,
          "providedCode": verificationCode
        })).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> resetPassword({
    required String emailOrPhonenumber,
    required String verificationCode,
    required String password,
  }) async {
    return NetworkUtil.post(
        url: UserEndpoints.RESET_PASSWORD_API,
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({
          "phoneNumber": emailOrPhonenumber,
          "providedCode": verificationCode,
          "newPassword": password
        })).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }

  Future<CommonResponse> createUpdateFCMToken({
    required String fcmToken,
  }) async {
    Map<String, String> params = {
      "fcmToken": fcmToken,
      "deviceId": locator<String>(),
    };
    return NetworkUtil.post(
        url: NetworkUtil.getUrlWithParams(
            UserEndpoints.VERIFY_FORGET_PASSWORD_CODE_API, params),
        headers: NetworkConfig.getHeaders(headers),
        body: jsonEncode({})).then((dynamic response) {
      return CommonResponse<bool>.fromJson(response);
    });
  }
}
