import 'dart:convert';

import 'package:flutter/foundation.dart';

class Contractor {
  int contractorId;
  String userName;
  String commercialSijel;
  String companyName;
  String websiteUrl;
  String companyProfileFileUrl;
  String latitude;
  String longitude;
  String categoryName;
  String cityName;
  List<Projects> projects;
  Contractor({
    required this.contractorId,
    required this.userName,
    required this.commercialSijel,
    required this.companyName,
    required this.websiteUrl,
    required this.companyProfileFileUrl,
    required this.latitude,
    required this.longitude,
    required this.categoryName,
    required this.cityName,
    required this.projects,
  });

  Contractor copyWith({
    int? contractorId,
    String? userName,
    String? commercialSijel,
    String? companyName,
    String? websiteUrl,
    String? companyProfileFileUrl,
    String? latitude,
    String? longitude,
    String? categoryName,
    String? cityName,
    List<Projects>? projects,
  }) {
    return Contractor(
      contractorId: contractorId ?? this.contractorId,
      userName: userName ?? this.userName,
      commercialSijel: commercialSijel ?? this.commercialSijel,
      companyName: companyName ?? this.companyName,
      websiteUrl: websiteUrl ?? this.websiteUrl,
      companyProfileFileUrl:
          companyProfileFileUrl ?? this.companyProfileFileUrl,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      categoryName: categoryName ?? this.categoryName,
      cityName: cityName ?? this.cityName,
      projects: projects ?? this.projects,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'contractorId': contractorId,
      'userName': userName,
      'commercialSijel': commercialSijel,
      'companyName': companyName,
      'websiteUrl': websiteUrl,
      'companyProfileFileUrl': companyProfileFileUrl,
      'latitude': latitude,
      'longitude': longitude,
      'categoryName': categoryName,
      'cityName': cityName,
      'projects': projects.map((x) => x.toMap()).toList(),
    };
  }

  factory Contractor.fromMap(Map<String, dynamic> map) {
    return Contractor(
      contractorId: map['contractorId'],
      userName: map['userName'],
      commercialSijel: map['commercialSijel'],
      companyName: map['companyName'],
      websiteUrl: map['websiteUrl'],
      companyProfileFileUrl: map['companyProfileFileUrl'],
      latitude: map['latitude'],
      longitude: map['longitude'],
      categoryName: map['categoryName'],
      cityName: map['cityName'],
      projects:
          List<Projects>.from(map['projects']?.map((x) => Projects.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Contractor.fromJson(String source) =>
      Contractor.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Contractor(contractorId: $contractorId, userName: $userName, commercialSijel: $commercialSijel, companyName: $companyName, websiteUrl: $websiteUrl, companyProfileFileUrl: $companyProfileFileUrl, latitude: $latitude, longitude: $longitude, categoryName: $categoryName, cityName: $cityName, projects: $projects)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Contractor &&
        other.contractorId == contractorId &&
        other.userName == userName &&
        other.commercialSijel == commercialSijel &&
        other.companyName == companyName &&
        other.websiteUrl == websiteUrl &&
        other.companyProfileFileUrl == companyProfileFileUrl &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.categoryName == categoryName &&
        other.cityName == cityName &&
        listEquals(other.projects, projects);
  }

  @override
  int get hashCode {
    return contractorId.hashCode ^
        userName.hashCode ^
        commercialSijel.hashCode ^
        companyName.hashCode ^
        websiteUrl.hashCode ^
        companyProfileFileUrl.hashCode ^
        latitude.hashCode ^
        longitude.hashCode ^
        categoryName.hashCode ^
        cityName.hashCode ^
        projects.hashCode;
  }
}

class Projects {
  int id;
  String title;
  String description;
  int contractorId;
  String contractorCompanyName;
  List<ProjectPictures> projectPictures;
  Projects({
    required this.id,
    required this.title,
    required this.description,
    required this.contractorId,
    required this.contractorCompanyName,
    required this.projectPictures,
  });

  Projects copyWith({
    int? id,
    String? title,
    String? description,
    int? contractorId,
    String? contractorCompanyName,
    List<ProjectPictures>? projectPictures,
  }) {
    return Projects(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      contractorId: contractorId ?? this.contractorId,
      contractorCompanyName:
          contractorCompanyName ?? this.contractorCompanyName,
      projectPictures: projectPictures ?? this.projectPictures,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'description': description,
      'contractorId': contractorId,
      'contractorCompanyName': contractorCompanyName,
      'projectPictures': projectPictures.map((x) => x.toMap()).toList(),
    };
  }

  factory Projects.fromMap(Map<String, dynamic> map) {
    return Projects(
      id: map['id'],
      title: map['title'],
      description: map['description'],
      contractorId: map['contractorId'],
      contractorCompanyName: map['contractorCompanyName'],
      projectPictures: List<ProjectPictures>.from(
          map['projectPictures']?.map((x) => ProjectPictures.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Projects.fromJson(String source) =>
      Projects.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Projects(id: $id, title: $title, description: $description, contractorId: $contractorId, contractorCompanyName: $contractorCompanyName, projectPictures: $projectPictures)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Projects &&
        other.id == id &&
        other.title == title &&
        other.description == description &&
        other.contractorId == contractorId &&
        other.contractorCompanyName == contractorCompanyName &&
        listEquals(other.projectPictures, projectPictures);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        title.hashCode ^
        description.hashCode ^
        contractorId.hashCode ^
        contractorCompanyName.hashCode ^
        projectPictures.hashCode;
  }
}

class ProjectPictures {
  String pictureUrl;
  String description;
  int projectId;
  String projectTitle;
  ProjectPictures({
    required this.pictureUrl,
    required this.description,
    required this.projectId,
    required this.projectTitle,
  });

  ProjectPictures copyWith({
    String? pictureUrl,
    String? description,
    int? projectId,
    String? projectTitle,
  }) {
    return ProjectPictures(
      pictureUrl: pictureUrl ?? this.pictureUrl,
      description: description ?? this.description,
      projectId: projectId ?? this.projectId,
      projectTitle: projectTitle ?? this.projectTitle,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'pictureUrl': pictureUrl,
      'description': description,
      'projectId': projectId,
      'projectTitle': projectTitle,
    };
  }

  factory ProjectPictures.fromMap(Map<String, dynamic> map) {
    return ProjectPictures(
      pictureUrl: map['pictureUrl'],
      description: map['description'],
      projectId: map['projectId'],
      projectTitle: map['projectTitle'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectPictures.fromJson(String source) =>
      ProjectPictures.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProjectPictures(pictureUrl: $pictureUrl, description: $description, projectId: $projectId, projectTitle: $projectTitle)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProjectPictures &&
        other.pictureUrl == pictureUrl &&
        other.description == description &&
        other.projectId == projectId &&
        other.projectTitle == projectTitle;
  }

  @override
  int get hashCode {
    return pictureUrl.hashCode ^
        description.hashCode ^
        projectId.hashCode ^
        projectTitle.hashCode;
  }
}
