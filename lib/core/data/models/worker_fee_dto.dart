import 'dart:convert';

import 'package:flutter/foundation.dart';

class WorkerFeeDto {
  int? workerFeeId;
  int? feeType;
  double? value;
  int? feeIncludedTypes;
  List<FeeIncloudType> feeList = [];
  WorkerFeeDto({
    this.workerFeeId,
    this.feeType,
    this.value,
    this.feeIncludedTypes,
    required this.feeList,
  });

  WorkerFeeDto copyWith({
    int? workerFeeId,
    int? feeType,
    double? value,
    int? feeIncludedTypes,
    List<FeeIncloudType>? feeList,
  }) {
    return WorkerFeeDto(
      workerFeeId: workerFeeId ?? this.workerFeeId,
      feeType: feeType ?? this.feeType,
      value: value ?? this.value,
      feeIncludedTypes: feeIncludedTypes ?? this.feeIncludedTypes,
      feeList: feeList ?? this.feeList,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'workerFeeId': workerFeeId,
      'feeType': feeType,
      'value': value,
      'feeIncludedTypes': feeIncludedTypes,
      'feeList': feeList.map((x) => x.toMap()).toList(),
    };
  }

  factory WorkerFeeDto.fromMap(Map<String, dynamic> map) {
    return WorkerFeeDto(
      workerFeeId: map['workerFeeId'],
      feeType: map['feeType'],
      value: map['value'],
      feeIncludedTypes: map['feeIncludedTypes'],
      feeList: List<FeeIncloudType>.from(
          map['feeList']?.map((x) => FeeIncloudType.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkerFeeDto.fromJson(String source) =>
      WorkerFeeDto.fromMap(json.decode(source));

  @override
  String toString() {
    return 'WorkerFeeDto(workerFeeId: $workerFeeId, feeType: $feeType, value: $value, feeIncludedTypes: $feeIncludedTypes, feeList: $feeList)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WorkerFeeDto &&
        other.workerFeeId == workerFeeId &&
        other.feeType == feeType &&
        other.value == value &&
        other.feeIncludedTypes == feeIncludedTypes &&
        listEquals(other.feeList, feeList);
  }

  @override
  int get hashCode {
    return workerFeeId.hashCode ^
        feeType.hashCode ^
        value.hashCode ^
        feeIncludedTypes.hashCode ^
        feeList.hashCode;
  }

  void addToList(FeeIncloudType feeIncloudType) {
    feeList.removeWhere((it) => it.id == feeIncloudType.id);

    feeList.add(feeIncloudType);
  }

  void removeFromList(int id) {
    feeList.removeWhere((it) => it.id == id);
  }
}

class FeeIncloudType {
  int id;
  int value;
  FeeIncloudType({
    required this.id,
    required this.value,
  });

  FeeIncloudType copyWith({
    int? id,
    int? value,
  }) {
    return FeeIncloudType(
      id: id ?? this.id,
      value: value ?? this.value,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'value': value,
    };
  }

  factory FeeIncloudType.fromMap(Map<String, dynamic> map) {
    return FeeIncloudType(
      id: map['id'],
      value: map['value'],
    );
  }

  String toJson() => json.encode(toMap());

  factory FeeIncloudType.fromJson(String source) =>
      FeeIncloudType.fromMap(json.decode(source));

  @override
  String toString() => 'FeeIncloudType(id: $id, value: $value)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is FeeIncloudType && other.id == id && other.value == value;
  }

  @override
  int get hashCode => id.hashCode ^ value.hashCode;
}
