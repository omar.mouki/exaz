class CommonResponse<T> {
  bool? status;
  T? data;
  String? errors;

  CommonResponse.fromJson(dynamic json) {
    try {
      this.status = json["isResultExist"];
      if (this.status!) {
        this.data = json['result'];
      } else {
        this.errors = json["error"];
        print(json["error"]);
      }
    } catch (e) {
      // this.status = false;
      // this.data = null;
      // this.errors = json["error"]!=null? json["error"]: 'some thing went wrong';
      // this.errors = 'some thing went wrong';
      print(e.toString());
    }
  }

  bool get getStatus => this.status!;

  T get getData {
    return data!;
  }

  String get getError {
    return errors!;
  }
}
