class CommonResponse<T> {
  String? requestId;
  String? timestamp;
  bool? status;
  T? data;
  Map<dynamic, dynamic>? error;

  CommonResponse.fromJson(dynamic json) {
    try {
      this.requestId = json["request_id"];
      this.timestamp = json["timestamp"];
      this.status = json["status"];
      if (this.status!) {
        this.data = json["data"];
      } else {
        this.error = json["errors"];
      }
    } catch (e) {
      print(e.toString());
    }
  }

  bool get getStatus {
    return status!;
  }

  T get getData {
    return data!;
  }

  Map<dynamic, dynamic> get getError {
    if (error != null) {
      return error!;
    } else {
      return {'error': 'no error'};
    }
  }
}
