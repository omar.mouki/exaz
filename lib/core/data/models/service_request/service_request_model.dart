import 'dart:convert';

import 'package:flutter/foundation.dart';

class ServiceRequestModel {
  int? projectId;
  List<String>? pdFs;
  List<String>? images;
  String? requesterFullName;
  String? requesterPhoneNumber;
  String title;
  String description;
  String? displayEndDate;
  String startDate;
  String projectOwnerName;
  String projectOwnerPhone;
  String projectManagerName;
  String projectManagerPhone;
  String projectLocation;
  int requestState;
  int requestType;
  int requestCategoryId;
  String? requestCategoryName;
  bool? canEdit;
  String? materialsType;
  ServiceRequestModel(
      {this.projectId,
      this.pdFs,
      this.images,
      this.requesterFullName,
      this.requesterPhoneNumber,
      required this.title,
      required this.description,
      this.displayEndDate,
      required this.startDate,
      required this.projectOwnerName,
      required this.projectOwnerPhone,
      required this.projectManagerName,
      required this.projectManagerPhone,
      required this.projectLocation,
      required this.requestState,
      required this.requestType,
      required this.requestCategoryId,
      this.requestCategoryName,
      this.materialsType,
      this.canEdit = false});

  ServiceRequestModel copyWith({
    int? projectId,
    List<String>? pdFs,
    List<String>? images,
    String? requesterFullName,
    String? requesterPhoneNumber,
    String? title,
    String? description,
    String? displayEndDate,
    String? startDate,
    String? projectOwnerName,
    String? projectOwnerPhone,
    String? projectManagerName,
    String? projectManagerPhone,
    String? projectLocation,
    int? requestState,
    int? requestType,
    int? requestCategoryId,
    String? requestCategoryName,
    String? materialsType,
    bool? canEdit,
  }) {
    return ServiceRequestModel(
      projectId: projectId ?? this.projectId,
      pdFs: pdFs ?? this.pdFs,
      images: images ?? this.images,
      requesterFullName: requesterFullName ?? this.requesterFullName,
      requesterPhoneNumber: requesterPhoneNumber ?? this.requesterPhoneNumber,
      title: title ?? this.title,
      description: description ?? this.description,
      displayEndDate: displayEndDate ?? this.displayEndDate,
      startDate: startDate ?? this.startDate,
      projectOwnerName: projectOwnerName ?? this.projectOwnerName,
      projectOwnerPhone: projectOwnerPhone ?? this.projectOwnerPhone,
      projectManagerName: projectManagerName ?? this.projectManagerName,
      projectManagerPhone: projectManagerPhone ?? this.projectManagerPhone,
      projectLocation: projectLocation ?? this.projectLocation,
      requestState: requestState ?? this.requestState,
      requestType: requestType ?? this.requestType,
      requestCategoryId: requestCategoryId ?? this.requestCategoryId,
      requestCategoryName: requestCategoryName ?? this.requestCategoryName,
      materialsType: materialsType ?? this.materialsType,
      canEdit: canEdit ?? this.canEdit,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'projectId': projectId,
      'pdFs': pdFs,
      'images': images,
      'requesterFullName': requesterFullName,
      'requesterPhoneNumber': requesterPhoneNumber,
      'title': title,
      'description': description,
      'displayEndDate': displayEndDate,
      'startDate': startDate,
      'projectOwnerName': projectOwnerName,
      'projectOwnerPhone': projectOwnerPhone,
      'projectManagerName': projectManagerName,
      'projectManagerPhone': projectManagerPhone,
      'projectLocation': projectLocation,
      'requestState': requestState,
      'requestType': requestType,
      'requestCategoryId': requestCategoryId,
      'requestCategoryName': requestCategoryName,
      'materialsType': materialsType,
      'canEdit': canEdit,
    };
  }

  factory ServiceRequestModel.fromMap(Map<String, dynamic> map) {
    return ServiceRequestModel(
      projectId: map['projectId']?.toInt(),
      pdFs: map['pdFs'] == null ? null : List<String>.from(map['pdFs']),
      images: map['images'] == null ? null : List<String>.from(map['images']),
      requesterFullName: map['requesterFullName'],
      requesterPhoneNumber: map['requesterPhoneNumber'],
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      displayEndDate: map['displayEndDate'],
      startDate: map['startDate'] ?? '',
      projectOwnerName: map['projectOwnerName'] ?? '',
      projectOwnerPhone: map['projectOwnerPhone'] ?? '',
      projectManagerName: map['projectManagerName'] ?? '',
      projectManagerPhone: map['projectManagerPhone'] ?? '',
      projectLocation: map['projectLocation'] ?? '',
      requestState: map['requestState']?.toInt() ?? 0,
      requestType: map['requestType']?.toInt() ?? 0,
      requestCategoryId: map['requestCategoryId']?.toInt() ?? 0,
      requestCategoryName: map['requestCategoryName'],
      materialsType: map['materialsType'],
      canEdit: map['canEdit'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ServiceRequestModel.fromJson(Map<String, dynamic> source) =>
      ServiceRequestModel.fromMap(source);

  @override
  String toString() {
    return 'ServiceRequestModel(projectId: $projectId, pdFs: $pdFs, images: $images, requesterFullName: $requesterFullName, requesterPhoneNumber: $requesterPhoneNumber, title: $title, description: $description, displayEndDate: $displayEndDate, startDate: $startDate, projectOwnerName: $projectOwnerName, projectOwnerPhone: $projectOwnerPhone, projectManagerName: $projectManagerName, projectManagerPhone: $projectManagerPhone, projectLocation: $projectLocation, requestState: $requestState, requestType: $requestType, requestCategoryId: $requestCategoryId, requestCategoryName: $requestCategoryName, materialsType: $materialsType , canEdit: $canEdit)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ServiceRequestModel &&
        other.projectId == projectId &&
        listEquals(other.pdFs, pdFs) &&
        listEquals(other.images, images) &&
        other.requesterFullName == requesterFullName &&
        other.requesterPhoneNumber == requesterPhoneNumber &&
        other.title == title &&
        other.description == description &&
        other.displayEndDate == displayEndDate &&
        other.startDate == startDate &&
        other.projectOwnerName == projectOwnerName &&
        other.projectOwnerPhone == projectOwnerPhone &&
        other.projectManagerName == projectManagerName &&
        other.projectManagerPhone == projectManagerPhone &&
        other.projectLocation == projectLocation &&
        other.requestState == requestState &&
        other.requestType == requestType &&
        other.requestCategoryId == requestCategoryId &&
        other.requestCategoryName == requestCategoryName &&
        other.canEdit == canEdit &&
        other.materialsType == materialsType;
  }

  @override
  int get hashCode {
    return projectId.hashCode ^
        pdFs.hashCode ^
        images.hashCode ^
        requesterFullName.hashCode ^
        requesterPhoneNumber.hashCode ^
        title.hashCode ^
        description.hashCode ^
        displayEndDate.hashCode ^
        startDate.hashCode ^
        projectOwnerName.hashCode ^
        projectOwnerPhone.hashCode ^
        projectManagerName.hashCode ^
        projectManagerPhone.hashCode ^
        projectLocation.hashCode ^
        requestState.hashCode ^
        requestType.hashCode ^
        requestCategoryId.hashCode ^
        requestCategoryName.hashCode ^
        canEdit.hashCode ^
        materialsType.hashCode;
  }
}
