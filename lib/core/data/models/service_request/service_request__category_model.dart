class ServiceRequestCategoryModel {
  int? id;
  String? name;
  int? numberOfServiceRequestsInCategory;
  List<Translations>? translations;

  ServiceRequestCategoryModel(
      {this.id,
      this.name,
      this.numberOfServiceRequestsInCategory,
      this.translations});

  ServiceRequestCategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    numberOfServiceRequestsInCategory =
        json['numberOfServiceRequestsInCategory'];
    if (json['translations'] != null) {
      translations = <Translations>[];
      json['translations'].forEach((v) {
        translations!.add(new Translations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['numberOfServiceRequestsInCategory'] =
        this.numberOfServiceRequestsInCategory;
    if (this.translations != null) {
      data['translations'] = this.translations!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Translations {
  String? languageName;
  int? languageId;
  String? translatedName;

  Translations({this.languageName, this.languageId, this.translatedName});

  Translations.fromJson(Map<String, dynamic> json) {
    languageName = json['languageName'];
    languageId = json['languageId'];
    translatedName = json['translatedName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['languageName'] = this.languageName;
    data['languageId'] = this.languageId;
    data['translatedName'] = this.translatedName;
    return data;
  }
}
