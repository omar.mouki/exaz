import 'dart:convert';

class ProfileInfo {
  String? fullName;
  String? email;
  String? phoneNumber;
  String? city;
  String? picture;
  String? accountCreatedAt;
  int cityId;
  bool isServiceProvider;
  bool isRequestedToBeServiceProvider;
  ProfileInfo({
    this.fullName,
    this.email,
    this.phoneNumber,
    this.city,
    this.picture,
    this.accountCreatedAt,
    required this.cityId,
    required this.isServiceProvider,
    required this.isRequestedToBeServiceProvider,
  });

  ProfileInfo copyWith({
    String? fullName,
    String? email,
    String? phoneNumber,
    String? city,
    String? picture,
    String? accountCreatedAt,
    int? cityId,
    bool? isServiceProvider,
    bool? isRequestedToBeServiceProvider,
  }) {
    return ProfileInfo(
      fullName: fullName ?? this.fullName,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      city: city ?? this.city,
      picture: picture ?? this.picture,
      accountCreatedAt: accountCreatedAt ?? this.accountCreatedAt,
      cityId: cityId ?? this.cityId,
      isServiceProvider: isServiceProvider ?? this.isServiceProvider,
      isRequestedToBeServiceProvider:
          isRequestedToBeServiceProvider ?? this.isRequestedToBeServiceProvider,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'fullName': fullName,
      'email': email,
      'phoneNumber': phoneNumber,
      'city': city,
      'picture': picture,
      'accountCreatedAt': accountCreatedAt,
      'cityId': cityId,
      'isServiceProvider': isServiceProvider,
      'isRequestedToBeServiceProvider': isRequestedToBeServiceProvider,
    };
  }

  factory ProfileInfo.fromMap(Map<String, dynamic> map) {
    return ProfileInfo(
      fullName: map['fullName'],
      email: map['email'],
      phoneNumber: map['phoneNumber'],
      city: map['city'],
      picture: map['picture'],
      accountCreatedAt: map['accountCreatedAt'],
      cityId: map['cityId'],
      isServiceProvider: map['isServiceProvider'],
      isRequestedToBeServiceProvider: map['isRequestedToBeServiceProvider'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProfileInfo.fromJson(Map<String, dynamic> map) =>
      ProfileInfo.fromMap(map);

  @override
  String toString() {
    return 'ProfileInfo(fullName: $fullName, email: $email, phoneNumber: $phoneNumber, city: $city, picture: $picture, accountCreatedAt: $accountCreatedAt, cityId: $cityId, isServiceProvider: $isServiceProvider, isRequestedToBeServiceProvider: $isRequestedToBeServiceProvider)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProfileInfo &&
        other.fullName == fullName &&
        other.email == email &&
        other.phoneNumber == phoneNumber &&
        other.city == city &&
        other.picture == picture &&
        other.accountCreatedAt == accountCreatedAt &&
        other.cityId == cityId &&
        other.isServiceProvider == isServiceProvider &&
        other.isRequestedToBeServiceProvider == isRequestedToBeServiceProvider;
  }

  @override
  int get hashCode {
    return fullName.hashCode ^
        email.hashCode ^
        phoneNumber.hashCode ^
        city.hashCode ^
        picture.hashCode ^
        accountCreatedAt.hashCode ^
        cityId.hashCode ^
        isServiceProvider.hashCode ^
        isRequestedToBeServiceProvider.hashCode;
  }
}
