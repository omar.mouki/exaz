import 'dart:convert';

class ProductModel {
  String name;
  String categoryName;
  int id;
  int storeId;
  int price;
  String picture;
  bool isFavoriteByUser;
  ProductModel({
    required this.name,
    required this.categoryName,
    required this.id,
    required this.storeId,
    required this.price,
    required this.picture,
    required this.isFavoriteByUser,
  });

  ProductModel copyWith({
    String? name,
    String? categoryName,
    int? id,
    int? storeId,
    int? price,
    String? picture,
    bool? isFavoriteByUser,
  }) {
    return ProductModel(
      name: name ?? this.name,
      categoryName: categoryName ?? this.categoryName,
      id: id ?? this.id,
      storeId: storeId ?? this.storeId,
      price: price ?? this.price,
      picture: picture ?? this.picture,
      isFavoriteByUser: isFavoriteByUser ?? this.isFavoriteByUser,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'categoryName': categoryName,
      'id': id,
      'storeId': storeId,
      'price': price,
      'picture': picture,
      'isFavoriteByUser': isFavoriteByUser,
    };
  }

  factory ProductModel.fromMap(Map<String, dynamic> map) {
    return ProductModel(
      name: map['name'],
      categoryName: map['categoryName'],
      id: map['id'],
      storeId: map['storeId'],
      price: map['price'],
      picture: map['picture'],
      isFavoriteByUser: map['isFavoriteByUser'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductModel.fromJson(Map<String, dynamic> map) =>
      ProductModel.fromMap(map);

  @override
  String toString() {
    return 'ProductModel(name: $name, categoryName: $categoryName, id: $id, storeId: $storeId, price: $price, picture: $picture, isFavoriteByUser: $isFavoriteByUser)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductModel &&
        other.name == name &&
        other.categoryName == categoryName &&
        other.id == id &&
        other.storeId == storeId &&
        other.price == price &&
        other.picture == picture &&
        other.isFavoriteByUser == isFavoriteByUser;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        categoryName.hashCode ^
        id.hashCode ^
        storeId.hashCode ^
        price.hashCode ^
        picture.hashCode ^
        isFavoriteByUser.hashCode;
  }
}
