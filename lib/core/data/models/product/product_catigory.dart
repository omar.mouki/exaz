import 'dart:convert';

class ProductCatigory {
  int id;
  String name;
  ProductCatigory({
    required this.id,
    required this.name,
  });

  ProductCatigory copyWith({
    int? id,
    String? name,
  }) {
    return ProductCatigory(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory ProductCatigory.fromMap(Map<String, dynamic> map) {
    return ProductCatigory(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductCatigory.fromJson(Map<String, dynamic> map) =>
      ProductCatigory.fromMap(map);

  @override
  String toString() => 'ProductCatigory(id: $id, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductCatigory && other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
