import 'dart:convert';

class ProductAttributes {
  int id;
  String attributeName;
  String attributeValue;
  ProductAttributes({
    required this.id,
    required this.attributeName,
    required this.attributeValue,
  });

  ProductAttributes copyWith({
    int? id,
    String? attributeName,
    String? attributeValue,
  }) {
    return ProductAttributes(
      id: id ?? this.id,
      attributeName: attributeName ?? this.attributeName,
      attributeValue: attributeValue ?? this.attributeValue,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'attributeName': attributeName,
      'attributeValue': attributeValue,
    };
  }

  factory ProductAttributes.fromMap(Map<String, dynamic> map) {
    return ProductAttributes(
      id: map['id'],
      attributeName: map['attributeName'],
      attributeValue: map['attributeValue'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductAttributes.fromJson(String source) =>
      ProductAttributes.fromMap(json.decode(source));

  @override
  String toString() =>
      'ProductAttributes(id: $id, attributeName: $attributeName, attributeValue: $attributeValue)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductAttributes &&
        other.id == id &&
        other.attributeName == attributeName &&
        other.attributeValue == attributeValue;
  }

  @override
  int get hashCode =>
      id.hashCode ^ attributeName.hashCode ^ attributeValue.hashCode;
}
