import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:exaaz/core/data/models/product/product_attributes.dart';
import 'package:exaaz/core/data/models/product/product_pictures.dart';

class ProductDetailsModel {
  int id;
  String name;
  String description;
  int storeId;
  int categoryId;
  String categoryName;
  String originCountryName;
  int originCountryId;
  String brand;
  String productCode;
  int rating;
  int price;
  String storeCompanyName;
  int warrantyPeriod;
  String warrantyPeriodTypeName;
  int warrantyPeriodType;
  List<ProductPictures> productPictures;
  List<ProductAttributes> productAttributes;
  ProductDetailsModel({
    required this.id,
    required this.name,
    required this.description,
    required this.storeId,
    required this.categoryId,
    required this.categoryName,
    required this.originCountryName,
    required this.originCountryId,
    required this.brand,
    required this.productCode,
    required this.rating,
    required this.price,
    required this.storeCompanyName,
    required this.warrantyPeriod,
    required this.warrantyPeriodTypeName,
    required this.warrantyPeriodType,
    required this.productPictures,
    required this.productAttributes,
  });

  ProductDetailsModel copyWith({
    int? id,
    String? name,
    String? description,
    int? storeId,
    int? categoryId,
    String? categoryName,
    String? originCountryName,
    int? originCountryId,
    String? brand,
    String? productCode,
    int? rating,
    int? price,
    String? storeCompanyName,
    int? warrantyPeriod,
    String? warrantyPeriodTypeName,
    int? warrantyPeriodType,
    List<ProductPictures>? productPictures,
    List<ProductAttributes>? productAttributes,
  }) {
    return ProductDetailsModel(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      storeId: storeId ?? this.storeId,
      categoryId: categoryId ?? this.categoryId,
      categoryName: categoryName ?? this.categoryName,
      originCountryName: originCountryName ?? this.originCountryName,
      originCountryId: originCountryId ?? this.originCountryId,
      brand: brand ?? this.brand,
      productCode: productCode ?? this.productCode,
      rating: rating ?? this.rating,
      price: price ?? this.price,
      storeCompanyName: storeCompanyName ?? this.storeCompanyName,
      warrantyPeriod: warrantyPeriod ?? this.warrantyPeriod,
      warrantyPeriodTypeName:
          warrantyPeriodTypeName ?? this.warrantyPeriodTypeName,
      warrantyPeriodType: warrantyPeriodType ?? this.warrantyPeriodType,
      productPictures: productPictures ?? this.productPictures,
      productAttributes: productAttributes ?? this.productAttributes,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'storeId': storeId,
      'categoryId': categoryId,
      'categoryName': categoryName,
      'originCountryName': originCountryName,
      'originCountryId': originCountryId,
      'brand': brand,
      'productCode': productCode,
      'rating': rating,
      'price': price,
      'storeCompanyName': storeCompanyName,
      'warrantyPeriod': warrantyPeriod,
      'warrantyPeriodTypeName': warrantyPeriodTypeName,
      'warrantyPeriodType': warrantyPeriodType,
      'productPictures': productPictures.map((x) => x.toMap()).toList(),
      'productAttributes': productAttributes.map((x) => x.toMap()).toList(),
    };
  }

  factory ProductDetailsModel.fromMap(Map<String, dynamic> map) {
    return ProductDetailsModel(
      id: map['id'],
      name: map['name'],
      description: map['description'],
      storeId: map['storeId'],
      categoryId: map['categoryId'],
      categoryName: map['categoryName'],
      originCountryName: map['originCountryName'],
      originCountryId: map['originCountryId'],
      brand: map['brand'],
      productCode: map['productCode'],
      rating: map['rating'],
      price: map['price'],
      storeCompanyName: map['storeCompanyName'],
      warrantyPeriod: map['warrantyPeriod'],
      warrantyPeriodTypeName: map['warrantyPeriodTypeName'],
      warrantyPeriodType: map['warrantyPeriodType'],
      productPictures: List<ProductPictures>.from(
          map['productPictures']?.map((x) => ProductPictures.fromMap(x))),
      productAttributes: List<ProductAttributes>.from(
          map['productAttributes']?.map((x) => ProductAttributes.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductDetailsModel.fromJson(Map<String, dynamic> map) =>
      ProductDetailsModel.fromMap(map);

  @override
  String toString() {
    return 'ProductDetailsModel(id: $id, name: $name, description: $description, storeId: $storeId, categoryId: $categoryId, categoryName: $categoryName, originCountryName: $originCountryName, originCountryId: $originCountryId, brand: $brand, productCode: $productCode, rating: $rating, price: $price, storeCompanyName: $storeCompanyName, warrantyPeriod: $warrantyPeriod, warrantyPeriodTypeName: $warrantyPeriodTypeName, warrantyPeriodType: $warrantyPeriodType, productPictures: $productPictures, productAttributes: $productAttributes)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductDetailsModel &&
        other.id == id &&
        other.name == name &&
        other.description == description &&
        other.storeId == storeId &&
        other.categoryId == categoryId &&
        other.categoryName == categoryName &&
        other.originCountryName == originCountryName &&
        other.originCountryId == originCountryId &&
        other.brand == brand &&
        other.productCode == productCode &&
        other.rating == rating &&
        other.price == price &&
        other.storeCompanyName == storeCompanyName &&
        other.warrantyPeriod == warrantyPeriod &&
        other.warrantyPeriodTypeName == warrantyPeriodTypeName &&
        other.warrantyPeriodType == warrantyPeriodType &&
        listEquals(other.productPictures, productPictures) &&
        listEquals(other.productAttributes, productAttributes);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        description.hashCode ^
        storeId.hashCode ^
        categoryId.hashCode ^
        categoryName.hashCode ^
        originCountryName.hashCode ^
        originCountryId.hashCode ^
        brand.hashCode ^
        productCode.hashCode ^
        rating.hashCode ^
        price.hashCode ^
        storeCompanyName.hashCode ^
        warrantyPeriod.hashCode ^
        warrantyPeriodTypeName.hashCode ^
        warrantyPeriodType.hashCode ^
        productPictures.hashCode ^
        productAttributes.hashCode;
  }
}
