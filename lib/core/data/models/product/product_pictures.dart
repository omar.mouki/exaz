import 'dart:convert';

class ProductPictures {
  int id;
  String pictureUrl;
  int productId;
  String productName;
  ProductPictures({
    required this.id,
    required this.pictureUrl,
    required this.productId,
    required this.productName,
  });

  ProductPictures copyWith({
    int? id,
    String? pictureUrl,
    int? productId,
    String? productName,
  }) {
    return ProductPictures(
      id: id ?? this.id,
      pictureUrl: pictureUrl ?? this.pictureUrl,
      productId: productId ?? this.productId,
      productName: productName ?? this.productName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'pictureUrl': pictureUrl,
      'productId': productId,
      'productName': productName,
    };
  }

  factory ProductPictures.fromMap(Map<String, dynamic> map) {
    return ProductPictures(
      id: map['id'],
      pictureUrl: map['pictureUrl'],
      productId: map['productId'],
      productName: map['productName'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductPictures.fromJson(String source) =>
      ProductPictures.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProductPictures(id: $id, pictureUrl: $pictureUrl, productId: $productId, productName: $productName)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductPictures &&
        other.id == id &&
        other.pictureUrl == pictureUrl &&
        other.productId == productId &&
        other.productName == productName;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        pictureUrl.hashCode ^
        productId.hashCode ^
        productName.hashCode;
  }
}
