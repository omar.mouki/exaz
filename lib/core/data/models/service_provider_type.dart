import 'dart:convert';

class ServiceProviderType {
  int id;
  String name;
  String iconUrl;
  ServiceProviderType({
    required this.id,
    required this.name,
    required this.iconUrl,
  });

  ServiceProviderType copyWith({
    int? id,
    String? name,
    String? iconUrl,
  }) {
    return ServiceProviderType(
      id: id ?? this.id,
      name: name ?? this.name,
      iconUrl: iconUrl ?? this.iconUrl,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'iconUrl': iconUrl,
    };
  }

  factory ServiceProviderType.fromMap(Map<String, dynamic> map) {
    return ServiceProviderType(
      id: map['id'],
      name: map['name'],
      iconUrl: map['iconUrl'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ServiceProviderType.fromJson(Map<String, dynamic> map) =>
      ServiceProviderType.fromMap(map);

  @override
  String toString() =>
      'ServiceProviderType(id: $id, name: $name, iconUrl: $iconUrl)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ServiceProviderType &&
        other.id == id &&
        other.name == name &&
        other.iconUrl == iconUrl;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ iconUrl.hashCode;
}
