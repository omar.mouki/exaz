import 'dart:convert';

class Nationality {
  int id;
  String name;
  Nationality({
    required this.id,
    required this.name,
  });

  Nationality copyWith({
    int? id,
    String? name,
  }) {
    return Nationality(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Nationality.fromMap(Map<String, dynamic> map) {
    return Nationality(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Nationality.fromJson(String source) =>
      Nationality.fromMap(json.decode(source));

  @override
  String toString() => 'Nationality(id: $id, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Nationality && other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
