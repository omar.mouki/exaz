import 'dart:convert';

class TokenInfo {
  String newToken;
  String refrechToken;
  String expirationDate;
  TokenInfo({
    required this.newToken,
    required this.refrechToken,
    required this.expirationDate,
  });

  TokenInfo copyWith({
    String? newToken,
    String? refrechToken,
    String? expirationDate,
  }) {
    return TokenInfo(
      newToken: newToken ?? this.newToken,
      refrechToken: refrechToken ?? this.refrechToken,
      expirationDate: expirationDate ?? this.expirationDate,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'accessToken': newToken,
      'refreshToken': refrechToken,
      'tokenExpirationDate': expirationDate,
    };
  }

  factory TokenInfo.fromMap(Map<String, dynamic> map) {
    return TokenInfo(
      newToken: map['accessToken'],
      refrechToken: map['refreshToken'],
      expirationDate: map['tokenExpirationDate'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TokenInfo.fromJson(Map<String, dynamic> map) =>
      TokenInfo.fromMap(map);

  @override
  String toString() =>
      'TokenInfo(newToken: $newToken, refrechToken: $refrechToken, expirationDate: $expirationDate)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TokenInfo &&
        other.newToken == newToken &&
        other.refrechToken == refrechToken &&
        other.expirationDate == expirationDate;
  }

  @override
  int get hashCode =>
      newToken.hashCode ^ refrechToken.hashCode ^ expirationDate.hashCode;
}
