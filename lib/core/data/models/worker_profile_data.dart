import 'package:easy_localization/easy_localization.dart';

import 'package:exaaz/core/data/models/worker_fee_dto.dart';

class WorkerProfileData {
  WorkerFeeDto? houeFee = WorkerFeeDto(feeList: []);
  WorkerFeeDto? dayFee = WorkerFeeDto(feeList: []);
  WorkerFeeDto? monthFee = WorkerFeeDto(feeList: []);

  String? picture;
  int? professionId;
  String? professionName;
  int? cityId;
  int? nationalityId;

  String? workerFees;
  String? description;
  String? phoneNumber;
  String? whatsappNumber;

  Map<String, bool>? houeFeeMap = {};
  Map<String, bool>? dayFeeMap = {};
  Map<String, bool>? monthFeeMap = {};

  void addDefaultDataToMap() {
    houeFee = WorkerFeeDto(
      feeList: [],
    );
    dayFee = WorkerFeeDto(feeList: []);
    monthFee = WorkerFeeDto(feeList: []);

    houeFeeMap?.putIfAbsent(tr('include_food'), () => false);
    houeFeeMap?.putIfAbsent(tr('includes_transportation'), () => false);

    dayFeeMap?.putIfAbsent(tr('include_food'), () => false);
    dayFeeMap?.putIfAbsent(tr('includes_transportation'), () => false);

    monthFeeMap?.putIfAbsent(tr('include_food'), () => false);
    monthFeeMap?.putIfAbsent(tr('includes_transportation'), () => false);
    monthFeeMap?.putIfAbsent(tr('includes_housing'), () => false);
  }

  void clearDate() {
    houeFee = null;
    dayFee = null;
    monthFee = null;
    picture = null;
    professionId = null;
    cityId = null;
    nationalityId = null;
    workerFees = null;
    description = null;
    houeFeeMap = {};
    dayFeeMap = {};
    monthFeeMap = {};
  }
}
