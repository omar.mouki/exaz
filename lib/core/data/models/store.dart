import 'dart:convert';

class Store {
  int storeId;
  String name;
  String? email;
  String? phoneNumber;
  String? website;
  String areaName;
  String cityName;
  String accountUserName;
  String accountImageUrl;
  String storeImageUrl;
  String commercialRecordUrl;
  String profileFileUrl;
  String categoryName;
  int categoryId;
  String? mainBranchLocation;
  int favoriteCount;
  int hadBeenFavoriteByCount;
  int profileVisitCount;
  double? rating;
  Store({
    required this.storeId,
    required this.name,
    this.email,
    this.phoneNumber,
    this.website,
    required this.areaName,
    required this.cityName,
    required this.accountUserName,
    required this.accountImageUrl,
    required this.storeImageUrl,
    required this.commercialRecordUrl,
    required this.profileFileUrl,
    required this.categoryName,
    required this.categoryId,
    this.mainBranchLocation,
    required this.favoriteCount,
    required this.hadBeenFavoriteByCount,
    required this.profileVisitCount,
    this.rating,
  });

  Store copyWith({
    int? storeId,
    String? name,
    String? email,
    String? phoneNumber,
    String? website,
    String? areaName,
    String? cityName,
    String? accountUserName,
    String? accountImageUrl,
    String? storeImageUrl,
    String? commercialRecordUrl,
    String? profileFileUrl,
    String? categoryName,
    int? categoryId,
    String? mainBranchLocation,
    int? favoriteCount,
    int? hadBeenFavoriteByCount,
    int? profileVisitCount,
    double? rating,
  }) {
    return Store(
      storeId: storeId ?? this.storeId,
      name: name ?? this.name,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      website: website ?? this.website,
      areaName: areaName ?? this.areaName,
      cityName: cityName ?? this.cityName,
      accountUserName: accountUserName ?? this.accountUserName,
      accountImageUrl: accountImageUrl ?? this.accountImageUrl,
      storeImageUrl: storeImageUrl ?? this.storeImageUrl,
      commercialRecordUrl: commercialRecordUrl ?? this.commercialRecordUrl,
      profileFileUrl: profileFileUrl ?? this.profileFileUrl,
      categoryName: categoryName ?? this.categoryName,
      categoryId: categoryId ?? this.categoryId,
      mainBranchLocation: mainBranchLocation ?? this.mainBranchLocation,
      favoriteCount: favoriteCount ?? this.favoriteCount,
      hadBeenFavoriteByCount:
          hadBeenFavoriteByCount ?? this.hadBeenFavoriteByCount,
      profileVisitCount: profileVisitCount ?? this.profileVisitCount,
      rating: rating ?? this.rating,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'storeId': storeId,
      'name': name,
      'email': email,
      'phoneNumber': phoneNumber,
      'website': website,
      'areaName': areaName,
      'cityName': cityName,
      'accountUserName': accountUserName,
      'accountImageUrl': accountImageUrl,
      'storeImageUrl': storeImageUrl,
      'commercialRecordUrl': commercialRecordUrl,
      'profileFileUrl': profileFileUrl,
      'categoryName': categoryName,
      'categoryId': categoryId,
      'mainBranchLocation': mainBranchLocation,
      'favoriteCount': favoriteCount,
      'hadBeenFavoriteByCount': hadBeenFavoriteByCount,
      'profileVisitCount': profileVisitCount,
      'rating': rating,
    };
  }

  factory Store.fromMap(Map<String, dynamic> map) {
    return Store(
      storeId: map['storeId']?.toInt() ?? 0,
      name: map['name'] ?? '',
      email: map['email'],
      phoneNumber: map['phoneNumber'],
      website: map['website'],
      areaName: map['areaName'] ?? '',
      cityName: map['cityName'] ?? '',
      accountUserName: map['accountUserName'] ?? '',
      accountImageUrl: map['accountImageUrl'] ?? '',
      storeImageUrl: map['storeImageUrl'] ?? '',
      commercialRecordUrl: map['commercialRecordUrl'] ?? '',
      profileFileUrl: map['profileFileUrl'] ?? '',
      categoryName: map['categoryName'] ?? '',
      categoryId: map['categoryId']?.toInt() ?? 0,
      mainBranchLocation: map['mainBranchLocation'],
      favoriteCount: map['favoriteCount']?.toInt() ?? 0,
      hadBeenFavoriteByCount: map['hadBeenFavoriteByCount']?.toInt() ?? 0,
      profileVisitCount: map['profileVisitCount']?.toInt() ?? 0,
      rating: map['rating']?.toDouble(),
    );
  }

  String toJson() => json.encode(toMap());

  factory Store.fromJson(Map<String, dynamic> map) => Store.fromMap(map);

  @override
  String toString() {
    return 'Store(storeId: $storeId, name: $name, email: $email, phoneNumber: $phoneNumber, website: $website, areaName: $areaName, cityName: $cityName, accountUserName: $accountUserName, accountImageUrl: $accountImageUrl, storeImageUrl: $storeImageUrl, commercialRecordUrl: $commercialRecordUrl, profileFileUrl: $profileFileUrl, categoryName: $categoryName, categoryId: $categoryId, mainBranchLocation: $mainBranchLocation, favoriteCount: $favoriteCount, hadBeenFavoriteByCount: $hadBeenFavoriteByCount, profileVisitCount: $profileVisitCount, rating: $rating)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Store &&
        other.storeId == storeId &&
        other.name == name &&
        other.email == email &&
        other.phoneNumber == phoneNumber &&
        other.website == website &&
        other.areaName == areaName &&
        other.cityName == cityName &&
        other.accountUserName == accountUserName &&
        other.accountImageUrl == accountImageUrl &&
        other.storeImageUrl == storeImageUrl &&
        other.commercialRecordUrl == commercialRecordUrl &&
        other.profileFileUrl == profileFileUrl &&
        other.categoryName == categoryName &&
        other.categoryId == categoryId &&
        other.mainBranchLocation == mainBranchLocation &&
        other.favoriteCount == favoriteCount &&
        other.hadBeenFavoriteByCount == hadBeenFavoriteByCount &&
        other.profileVisitCount == profileVisitCount &&
        other.rating == rating;
  }

  @override
  int get hashCode {
    return storeId.hashCode ^
        name.hashCode ^
        email.hashCode ^
        phoneNumber.hashCode ^
        website.hashCode ^
        areaName.hashCode ^
        cityName.hashCode ^
        accountUserName.hashCode ^
        accountImageUrl.hashCode ^
        storeImageUrl.hashCode ^
        commercialRecordUrl.hashCode ^
        profileFileUrl.hashCode ^
        categoryName.hashCode ^
        categoryId.hashCode ^
        mainBranchLocation.hashCode ^
        favoriteCount.hashCode ^
        hadBeenFavoriteByCount.hashCode ^
        profileVisitCount.hashCode ^
        rating.hashCode;
  }
}
