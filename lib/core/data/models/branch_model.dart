import 'dart:convert';

class BranchModel {
  int branchId;
  String name;
  int membershipId;
  int areaId;
  String areaName;
  int cityId;
  String cityName;
  String? longitude;
  String? latitude;
  bool isMainBranch;
  String? phoneNumber;
  String? customerCareNumber;
  String? website;
  String? email;
  BranchModel({
    required this.branchId,
    required this.name,
    required this.membershipId,
    required this.areaId,
    required this.areaName,
    required this.cityId,
    required this.cityName,
    this.longitude,
    this.latitude,
    required this.isMainBranch,
    this.phoneNumber,
    this.customerCareNumber,
    this.website,
    this.email,
  });

  BranchModel copyWith({
    int? branchId,
    String? name,
    int? membershipId,
    int? areaId,
    String? areaName,
    int? cityId,
    String? cityName,
    String? longitude,
    String? latitude,
    bool? isMainBranch,
    String? phoneNumber,
    String? customerCareNumber,
    String? website,
    String? email,
  }) {
    return BranchModel(
      branchId: branchId ?? this.branchId,
      name: name ?? this.name,
      membershipId: membershipId ?? this.membershipId,
      areaId: areaId ?? this.areaId,
      areaName: areaName ?? this.areaName,
      cityId: cityId ?? this.cityId,
      cityName: cityName ?? this.cityName,
      longitude: longitude ?? this.longitude,
      latitude: latitude ?? this.latitude,
      isMainBranch: isMainBranch ?? this.isMainBranch,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      customerCareNumber: customerCareNumber ?? this.customerCareNumber,
      website: website ?? this.website,
      email: email ?? this.email,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'branchId': branchId,
      'name': name,
      'membershipId': membershipId,
      'areaId': areaId,
      'areaName': areaName,
      'cityId': cityId,
      'cityName': cityName,
      'longitude': longitude,
      'latitude': latitude,
      'isMainBranch': isMainBranch,
      'phoneNumber': phoneNumber,
      'customerCareNumber': customerCareNumber,
      'website': website,
      'email': email,
    };
  }

  factory BranchModel.fromMap(Map<String, dynamic> map) {
    return BranchModel(
      branchId: map['branchId'],
      name: map['name'],
      membershipId: map['membershipId'],
      areaId: map['areaId'],
      areaName: map['areaName'],
      cityId: map['cityId'],
      cityName: map['cityName'],
      longitude: map['longitude'] != null ? map['longitude'] : null,
      latitude: map['latitude'] != null ? map['latitude'] : null,
      isMainBranch: map['isMainBranch'],
      phoneNumber: map['phoneNumber'] != null ? map['phoneNumber'] : null,
      customerCareNumber:
          map['customerCareNumber'] != null ? map['customerCareNumber'] : null,
      website: map['website'] != null ? map['website'] : null,
      email: map['email'] != null ? map['email'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory BranchModel.fromJson(Map<String, dynamic> map) =>
      BranchModel.fromMap(map);

  @override
  String toString() {
    return 'BranchModel(branchId: $branchId, name: $name, membershipId: $membershipId, areaId: $areaId, areaName: $areaName, cityId: $cityId, cityName: $cityName, longitude: $longitude, latitude: $latitude, isMainBranch: $isMainBranch, phoneNumber: $phoneNumber, customerCareNumber: $customerCareNumber, website: $website, email: $email)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BranchModel &&
        other.branchId == branchId &&
        other.name == name &&
        other.membershipId == membershipId &&
        other.areaId == areaId &&
        other.areaName == areaName &&
        other.cityId == cityId &&
        other.cityName == cityName &&
        other.longitude == longitude &&
        other.latitude == latitude &&
        other.isMainBranch == isMainBranch &&
        other.phoneNumber == phoneNumber &&
        other.customerCareNumber == customerCareNumber &&
        other.website == website &&
        other.email == email;
  }

  @override
  int get hashCode {
    return branchId.hashCode ^
        name.hashCode ^
        membershipId.hashCode ^
        areaId.hashCode ^
        areaName.hashCode ^
        cityId.hashCode ^
        cityName.hashCode ^
        longitude.hashCode ^
        latitude.hashCode ^
        isMainBranch.hashCode ^
        phoneNumber.hashCode ^
        customerCareNumber.hashCode ^
        website.hashCode ^
        email.hashCode;
  }
}
