import 'dart:convert';

class WorkerProfession {
  int professionId;
  String? professionName;
  WorkerProfession({
    required this.professionId,
    this.professionName,
  });

  WorkerProfession copyWith({
    int? professionId,
    String? professionName,
  }) {
    return WorkerProfession(
      professionId: professionId ?? this.professionId,
      professionName: professionName ?? this.professionName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'professionId': professionId,
      'professionName': professionName,
    };
  }

  factory WorkerProfession.fromMap(Map<String, dynamic> map) {
    return WorkerProfession(
      professionId: map['professionId'],
      professionName: map['professionName'],
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkerProfession.fromJson(Map<String, dynamic> map) =>
      WorkerProfession.fromMap(map);

  @override
  String toString() =>
      'WorkerProfession(professionId: $professionId, professionName: $professionName)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WorkerProfession &&
        other.professionId == professionId &&
        other.professionName == professionName;
  }

  @override
  int get hashCode => professionId.hashCode ^ professionName.hashCode;
}
