import 'dart:convert';

import 'package:flutter/foundation.dart';

class WorkerModel {
  int workerId;
  String userName;
  String? userFullName;
  String? pictureUrl;
  String? description;
  String? professionName;
  List<Fees> fees;
  String? nationality;
  String? worksInCityName;
  String? shareProfileLink;
  int profileVisitCount;
  int? rating;
  bool? isFavorite;
  WorkerModel({
    required this.workerId,
    required this.userName,
    this.userFullName,
    this.pictureUrl,
    this.description,
    this.professionName,
    required this.fees,
    this.nationality,
    this.worksInCityName,
    this.shareProfileLink,
    required this.profileVisitCount,
    this.rating,
    this.isFavorite,
  });

  WorkerModel copyWith({
    int? workerId,
    String? userName,
    String? userFullName,
    String? pictureUrl,
    String? description,
    String? professionName,
    List<Fees>? fees,
    String? nationality,
    String? worksInCityName,
    String? shareProfileLink,
    int? profileVisitCount,
    int? rating,
    bool? isFavorite,
  }) {
    return WorkerModel(
      workerId: workerId ?? this.workerId,
      userName: userName ?? this.userName,
      userFullName: userFullName ?? this.userFullName,
      pictureUrl: pictureUrl ?? this.pictureUrl,
      description: description ?? this.description,
      professionName: professionName ?? this.professionName,
      fees: fees ?? this.fees,
      nationality: nationality ?? this.nationality,
      worksInCityName: worksInCityName ?? this.worksInCityName,
      shareProfileLink: shareProfileLink ?? this.shareProfileLink,
      profileVisitCount: profileVisitCount ?? this.profileVisitCount,
      rating: rating ?? this.rating,
      isFavorite: isFavorite ?? this.isFavorite,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'workerId': workerId,
      'userName': userName,
      'userFullName': userFullName,
      'pictureUrl': pictureUrl,
      'description': description,
      'professionName': professionName,
      'fees': fees.map((x) => x.toMap()).toList(),
      'nationality': nationality,
      'worksInCityName': worksInCityName,
      'shareProfileLink': shareProfileLink,
      'profileVisitCount': profileVisitCount,
      'rating': rating,
      'isFavorite': isFavorite,
    };
  }

  factory WorkerModel.fromMap(Map<String, dynamic> map) {
    return WorkerModel(
      workerId: map['workerId'],
      userName: map['userName'],
      userFullName: map['userFullName'] != null ? map['userFullName'] : null,
      pictureUrl: map['pictureUrl'] != null ? map['pictureUrl'] : null,
      description: map['description'] != null ? map['description'] : null,
      professionName:
          map['professionName'] != null ? map['professionName'] : null,
      fees: List<Fees>.from(map['fees']?.map((x) => Fees.fromMap(x))),
      nationality: map['nationality'] != null ? map['nationality'] : null,
      worksInCityName:
          map['worksInCityName'] != null ? map['worksInCityName'] : null,
      shareProfileLink:
          map['shareProfileLink'] != null ? map['shareProfileLink'] : null,
      profileVisitCount: map['profileVisitCount'],
      rating: map['rating'] != null ? map['rating'] : null,
      isFavorite: map['isFavorite'] != null ? map['isFavorite'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkerModel.fromJson(Map<String, dynamic> map) =>
      WorkerModel.fromMap(map);

  @override
  String toString() {
    return 'WorkerModel(workerId: $workerId, userName: $userName, userFullName: $userFullName, pictureUrl: $pictureUrl, description: $description, professionName: $professionName, fees: $fees, nationality: $nationality, worksInCityName: $worksInCityName, shareProfileLink: $shareProfileLink, profileVisitCount: $profileVisitCount, rating: $rating, isFavorite: $isFavorite)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is WorkerModel &&
        other.workerId == workerId &&
        other.userName == userName &&
        other.userFullName == userFullName &&
        other.pictureUrl == pictureUrl &&
        other.description == description &&
        other.professionName == professionName &&
        listEquals(other.fees, fees) &&
        other.nationality == nationality &&
        other.worksInCityName == worksInCityName &&
        other.shareProfileLink == shareProfileLink &&
        other.profileVisitCount == profileVisitCount &&
        other.rating == rating &&
        other.isFavorite == isFavorite;
  }

  @override
  int get hashCode {
    return workerId.hashCode ^
        userName.hashCode ^
        userFullName.hashCode ^
        pictureUrl.hashCode ^
        description.hashCode ^
        professionName.hashCode ^
        fees.hashCode ^
        nationality.hashCode ^
        worksInCityName.hashCode ^
        shareProfileLink.hashCode ^
        profileVisitCount.hashCode ^
        rating.hashCode ^
        isFavorite.hashCode;
  }
}

class Fees {
  int feeId;
  String feeTypeName;
  int feeType;
  double value;
  List<FeeIncludedFeatures> feeIncludedFeatures;
  Fees({
    required this.feeId,
    required this.feeTypeName,
    required this.feeType,
    required this.value,
    required this.feeIncludedFeatures,
  });

  Fees copyWith({
    int? feeId,
    String? feeTypeName,
    int? feeType,
    double? value,
    List<FeeIncludedFeatures>? feeIncludedFeatures,
  }) {
    return Fees(
      feeId: feeId ?? this.feeId,
      feeTypeName: feeTypeName ?? this.feeTypeName,
      feeType: feeType ?? this.feeType,
      value: value ?? this.value,
      feeIncludedFeatures: feeIncludedFeatures ?? this.feeIncludedFeatures,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'feeId': feeId,
      'feeTypeName': feeTypeName,
      'feeType': feeType,
      'value': value,
      'feeIncludedFeatures': feeIncludedFeatures.map((x) => x.toMap()).toList(),
    };
  }

  factory Fees.fromMap(Map<String, dynamic> map) {
    return Fees(
      feeId: map['feeId'],
      feeTypeName: map['feeTypeName'],
      feeType: map['feeType'],
      value: map['value'],
      feeIncludedFeatures: List<FeeIncludedFeatures>.from(
          map['feeIncludedFeatures']
              ?.map((x) => FeeIncludedFeatures.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Fees.fromJson(String source) => Fees.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Fees(feeId: $feeId, feeTypeName: $feeTypeName, feeType: $feeType, value: $value, feeIncludedFeatures: $feeIncludedFeatures)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Fees &&
        other.feeId == feeId &&
        other.feeTypeName == feeTypeName &&
        other.feeType == feeType &&
        other.value == value &&
        listEquals(other.feeIncludedFeatures, feeIncludedFeatures);
  }

  @override
  int get hashCode {
    return feeId.hashCode ^
        feeTypeName.hashCode ^
        feeType.hashCode ^
        value.hashCode ^
        feeIncludedFeatures.hashCode;
  }
}

class FeeIncludedFeatures {
  int id;
  String name;
  FeeIncludedFeatures({
    required this.id,
    required this.name,
  });

  FeeIncludedFeatures copyWith({
    int? id,
    String? name,
  }) {
    return FeeIncludedFeatures(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory FeeIncludedFeatures.fromMap(Map<String, dynamic> map) {
    return FeeIncludedFeatures(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory FeeIncludedFeatures.fromJson(String source) =>
      FeeIncludedFeatures.fromMap(json.decode(source));

  @override
  String toString() => 'FeeIncludedFeatures(id: $id, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is FeeIncludedFeatures && other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
