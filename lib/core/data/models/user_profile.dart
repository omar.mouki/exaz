import 'dart:convert';

class UserProfile {
  String fullName;
  String phoneNumber;
  String email;
  String picture;
  bool isServiceProvidor;
  bool isProfileComplete;
  bool requestApproved;
  bool requestedToBeServiceProvider;
  int serviceProviderTypeId;
  String serviceProviderTypeName;
  String whatsappNumber;
  String cityName;
  UserProfile({
    required this.fullName,
    required this.phoneNumber,
    required this.email,
    required this.picture,
    required this.isServiceProvidor,
    required this.isProfileComplete,
    required this.requestApproved,
    required this.requestedToBeServiceProvider,
    required this.serviceProviderTypeId,
    required this.serviceProviderTypeName,
    required this.whatsappNumber,
    required this.cityName,
  });

  UserProfile copyWith({
    String? fullName,
    String? phoneNumber,
    String? email,
    String? picture,
    bool? isServiceProvidor,
    bool? isProfileComplete,
    bool? requestApproved,
    bool? requestedToBeServiceProvider,
    int? serviceProviderTypeId,
    String? serviceProviderTypeName,
    String? whatsappNumber,
    String? cityName,
  }) {
    return UserProfile(
      fullName: fullName ?? this.fullName,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      email: email ?? this.email,
      picture: picture ?? this.picture,
      isServiceProvidor: isServiceProvidor ?? this.isServiceProvidor,
      isProfileComplete: isProfileComplete ?? this.isProfileComplete,
      requestApproved: requestApproved ?? this.requestApproved,
      requestedToBeServiceProvider:
          requestedToBeServiceProvider ?? this.requestedToBeServiceProvider,
      serviceProviderTypeId:
          serviceProviderTypeId ?? this.serviceProviderTypeId,
      serviceProviderTypeName:
          serviceProviderTypeName ?? this.serviceProviderTypeName,
      whatsappNumber: whatsappNumber ?? this.whatsappNumber,
      cityName: cityName ?? this.cityName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'fullName': fullName,
      'phoneNumber': phoneNumber,
      'email': email,
      'picture': picture,
      'isServiceProvidor': isServiceProvidor,
      'isProfileComplete': isProfileComplete,
      'requestApproved': requestApproved,
      'requestedToBeServiceProvider': requestedToBeServiceProvider,
      'serviceProviderTypeId': serviceProviderTypeId,
      'serviceProviderTypeName': serviceProviderTypeName,
      'whatsappNumber': whatsappNumber,
      'cityName': cityName,
    };
  }

  factory UserProfile.fromMap(Map<String, dynamic> map) {
    return UserProfile(
      fullName: map['fullName'],
      phoneNumber: map['phoneNumber'],
      email: map['email'],
      picture: map['picture'],
      isServiceProvidor: map['isServiceProvidor'],
      isProfileComplete: map['isProfileComplete'],
      requestApproved: map['requestApproved'],
      requestedToBeServiceProvider: map['requestedToBeServiceProvider'],
      serviceProviderTypeId: map['serviceProviderTypeId'],
      serviceProviderTypeName: map['serviceProviderTypeName'],
      whatsappNumber: map['whatsappNumber'],
      cityName: map['cityName'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserProfile.fromJson(Map<String, dynamic> map) =>
      UserProfile.fromMap(map);

  @override
  String toString() {
    return 'UserProfile(fullName: $fullName, phoneNumber: $phoneNumber, email: $email, picture: $picture, isServiceProvidor: $isServiceProvidor, isProfileComplete: $isProfileComplete, requestApproved: $requestApproved, requestedToBeServiceProvider: $requestedToBeServiceProvider, serviceProviderTypeId: $serviceProviderTypeId, serviceProviderTypeName: $serviceProviderTypeName, whatsappNumber: $whatsappNumber, cityName: $cityName)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserProfile &&
        other.fullName == fullName &&
        other.phoneNumber == phoneNumber &&
        other.email == email &&
        other.picture == picture &&
        other.isServiceProvidor == isServiceProvidor &&
        other.isProfileComplete == isProfileComplete &&
        other.requestApproved == requestApproved &&
        other.requestedToBeServiceProvider == requestedToBeServiceProvider &&
        other.serviceProviderTypeId == serviceProviderTypeId &&
        other.serviceProviderTypeName == serviceProviderTypeName &&
        other.whatsappNumber == whatsappNumber &&
        other.cityName == cityName;
  }

  @override
  int get hashCode {
    return fullName.hashCode ^
        phoneNumber.hashCode ^
        email.hashCode ^
        picture.hashCode ^
        isServiceProvidor.hashCode ^
        isProfileComplete.hashCode ^
        requestApproved.hashCode ^
        requestedToBeServiceProvider.hashCode ^
        serviceProviderTypeId.hashCode ^
        serviceProviderTypeName.hashCode ^
        whatsappNumber.hashCode ^
        cityName.hashCode;
  }
}
