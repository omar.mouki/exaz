import 'dart:io';

import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {
  static bool get isLoogedIn =>
      locator<SharedPreferencesRepository>().getLoggedIn();

  static openwhatsapp(String number) async {
    var whatsappURlAndroid = "whatsapp://send?phone=" + number + "&text=hello";
    var whatappURLIos = "https://wa.me/$number?text=${Uri.parse("hello")}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunch(whatappURLIos)) {
        await launch(whatappURLIos, forceSafariVC: false);
      } else {
        CustomToasts.showMessage(
            message: 'whatsapp no installed',
            messageType: MessageType.errorMessage);
      }
    } else {
      // android , web
      if (await canLaunch(whatsappURlAndroid)) {
        await launch(whatsappURlAndroid);
      } else {
        CustomToasts.showMessage(
            message: 'whatsapp no installed',
            messageType: MessageType.errorMessage);
      }
    }
  }
}

extension CapExtension on String {
  String get inCaps =>
      this.isEmpty ? '' : '${this[0].toUpperCase()}${this.substring(1)}';

  String get allInCaps => this.isEmpty ? '' : this.toUpperCase();

  String get capitalizeFirstofEach =>
      this.isEmpty ? '' : this.split(" ").map((str) => str.inCaps).join(" ");
}

extension Range on num {
  bool isBetween(num from, num to) {
    return from < this && this < to;
  }
}
