// import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:google_sign_in/google_sign_in.dart';

class SocialMediaUtil {
  Future<String?> signInWithGoogle() async {
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount!.authentication;
    return googleSignInAuthentication.idToken;
  }

  // Future<String> signInWithFacebook() async {
  // final facebookLogin = FacebookLogin();
  // final result = await facebookLogin.logIn(permissions: [
  //   FacebookPermission.publicProfile,
  //   FacebookPermission.email,
  // ]);
  // final token = result.accessToken!.token;
  // return token;
  // }
}
