import '../../UI/shared/colors.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

void showLoader() {
  BotToast.showLoading();
}

void hideLoader() {
  BotToast.closeAllLoading();
}

void showMapLoader() {
  BotToast.showCustomLoading(toastBuilder: (b) {
    return Expanded(
      child: Column(
        children: [
          CircularProgressIndicator(),
          Text(
            'Get your current location',
            style: const TextStyle(color: AppColors.blackColor, fontSize: 25),
          ),
        ],
      ),
    );
  });
}
