import 'package:basic_utils/basic_utils.dart';

class StringUtil {
  static String convertFirstLetterToCapital(String str) {
    var strOriginArray = str.split(" ");
    List<String> finalList = [];
    for (int i = 0; i < strOriginArray.length; i++) {
      if (strOriginArray[i] != "") {
        finalList.add(StringUtils.capitalize(strOriginArray[i]));
      }
    }
    return finalList.join(" ");
  }

  static bool isValidEmail(String email) {
    if (email.length > 0 &&
        RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$')
            .hasMatch(email)) {
      return true;
    } else {
      return false;
    }
  }
}

extension CapExtension on String {
  String get removePrecisionIfZero =>
      this.isEmpty ? '' : this.replaceAll(RegExp(r"([.]*0)(?!.*\d)"), "");

  String get inCaps =>
      this.isEmpty ? '' : '${this[0].toUpperCase()}${this.substring(1)}';

  // String get fullImageUrl => this.isEmpty
  //     ? ''
  //     : 'https://' +
  //         SwitchServerUtil().getBaseUrl() +
  //         NetworkConstants.BASE_IMAGE +
  // this;

  String get allInCaps => this.isEmpty ? '' : this.toUpperCase();

  String get capitalizeFirstofEach => [null, ''].contains(this)
      ? ''
      : this.split(" ").map((str) => str.inCaps).join(" ");

  String get textEllipsis => this.isEmpty
      ? ''
      : (this.length <= 25)
          ? this
          : '${this.substring(0, 25)}...';

  bool get isValidEmail => RegExp(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
      .hasMatch(this);
  bool get isValidName => RegExp(r'^[a-zA-Z\u0600-\u06FF]+$').hasMatch(this);
  bool get isValidPassword => this.length >= 8;
}
