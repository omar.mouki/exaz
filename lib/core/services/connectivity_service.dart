import 'dart:async';

import 'package:data_connection_checker_tv/data_connection_checker.dart';
import 'package:exaaz/app/locator.dart';

enum ConnectivityStatus { Online, Offline }

class ConnectivityService {
  // Create our public controller
  StreamController<ConnectivityStatus> connectionStatusController =
      StreamController<ConnectivityStatus>();

  ConnectivityService() {
    // Subscribe to the connectivity Changed Steam

    locator<DataConnectionChecker>().onStatusChange.listen((status) {
      connectionStatusController.add(_getStatusFromResult(status));
    });
  }

  // Convert from the third part enum to our own enum
  ConnectivityStatus _getStatusFromResult(DataConnectionStatus result) {
    switch (result) {
      case DataConnectionStatus.connected:
        return ConnectivityStatus.Online;
      case DataConnectionStatus.disconnected:
        return ConnectivityStatus.Offline;
      default:
        return ConnectivityStatus.Online;
    }
  }
}
