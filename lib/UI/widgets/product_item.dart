import 'package:cached_network_image/cached_network_image.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../shared/custom_widgets/svg_icon_button.dart';

class ProductItem extends StatelessWidget {
  final String category;
  final String name;
  final String price;
  final String imgUrl;
  final Function? deleteOnTab;
  final Function? settingOnTab;

  const ProductItem(
      {Key? key,
      required this.category,
      required this.name,
      required this.price,
      required this.imgUrl,
      this.deleteOnTab,
      this.settingOnTab})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(size.shortestSide / 40),
            child: CachedNetworkImage(
              imageUrl: imgUrl,
              fit: BoxFit.fill,
              width: size.width / 2.6,
              height: size.width / 4,
              placeholder: (context, s) =>
                  const Center(child: CircularProgressIndicator()),
              errorWidget: (context, s, o) => Container(
                color: AppColors.whiteColor,
              ),
            ),
          ),
          Expanded(
              child: Container(
            padding: EdgeInsets.symmetric(horizontal: size.width / 40),
            height: size.width / 4.5,
            decoration: BoxDecoration(
                borderRadius: BorderRadiusDirectional.horizontal(
                    end: Radius.circular(size.width / 40)),
                gradient: const LinearGradient(
                  colors: [
                    Color.fromRGBO(33, 32, 32, 1),
                    Color.fromRGBO(52, 52, 52, 1),
                  ],
                  end: AlignmentDirectional.centerStart,
                  begin: AlignmentDirectional.centerEnd,
                )),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      category,
                      style: TextStyle(
                          color: AppColors.grey300Color,
                          fontWeight: FontWeight.w500,
                          fontSize: size.shortestSide / 27),
                    ),
                    Text(
                      name,
                      style: TextStyle(
                          color: AppColors.whiteColor,
                          fontWeight: FontWeight.w500,
                          fontSize: size.shortestSide / 24),
                    ),
                    Text(
                      price,
                      style: TextStyle(
                          color: AppColors.yellowColor,
                          fontWeight: FontWeight.w500,
                          fontSize: size.shortestSide / 32),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    SvgIconButton(
                      onPressed: deleteOnTab,
                      svgPath: 'assets/icons/delete.svg',
                      width: size.width / 18,
                    ),
                    SvgIconButton(
                      onPressed: settingOnTab,
                      svgPath: 'assets/icons/settings.svg',
                      width: size.width / 18,
                    ),
                  ],
                )
              ],
            ),
          ))
        ],
      ),
    );
  }
}
