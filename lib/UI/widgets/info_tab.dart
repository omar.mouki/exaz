import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../shared/custom_widgets/svg_icon_button.dart';
import '../shared/custom_widgets/text_field_widget.dart';

class InfoTab extends StatelessWidget {
  const InfoTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            SvgIconButton(
              svgPath: 'assets/icons/info_phone.svg',
              width: size.shortestSide / 16,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
                child: TextFieldWidget(
              title: '',
              isDense: true,
            ))
          ],
        ),
        Row(
          children: <Widget>[
            SvgIconButton(
              svgPath: 'assets/icons/info_whatsapp.svg',
              width: size.shortestSide / 16,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
                child: TextFieldWidget(
              title: '',
              isDense: true,
            ))
          ],
        ),
        Row(
          children: <Widget>[
            SvgIconButton(
              svgPath: 'assets/icons/info_messages.svg',
              width: size.shortestSide / 16,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
                child: TextFieldWidget(
              title: '',
              isDense: true,
            ))
          ],
        ),
        Row(
          children: <Widget>[
            SvgIconButton(
              svgPath: 'assets/icons/info_location.svg',
              width: size.shortestSide / 16,
            ),
            const SizedBox(
              width: 20,
            ),
            InkWell(
              onTap: () {},
              child: Text(
                tr('locate_on_map'),
                style: TextStyle(
                    color: AppColors.grey300Color,
                    fontWeight: FontWeight.w500,
                    fontSize: size.shortestSide / 30),
              ),
            )
          ],
        )
      ],
    );
  }
}
