import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../shared/custom_widgets/svg_icon_button.dart';

class BioTab extends StatelessWidget {
  const BioTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: <Widget>[
          const TextField(
            maxLines: 4,
            decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(width: 1, color: AppColors.grey300Color),
                ),
                border: OutlineInputBorder(
                  borderSide:
                      BorderSide(width: 1, color: AppColors.grey300Color),
                ),
                filled: true,
                fillColor: AppColors.darkGreyColor,
                hintText: ''),
          ),
          Expanded(
            child: Center(
              child: FlatButton(
                color: AppColors.yellowColor,
                padding: EdgeInsets.symmetric(horizontal: size.width / 40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                onPressed: () {},
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SvgIconButton(
                      svgPath: 'assets/icons/navbar_add.svg',
                      width: size.width / 18,
                      color: AppColors.whiteColor,
                    ),
                    Text(tr('add_to_profile'),
                        style: TextStyle(
                            color: AppColors.whiteColor,
                            fontSize: size.shortestSide / 26,
                            fontWeight: FontWeight.w400)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
