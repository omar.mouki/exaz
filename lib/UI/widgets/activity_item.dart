import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ActivityItem extends StatelessWidget {
  final String svgPath;
  final String title;
  final Function onTab;
  ActivityItem(
      {required this.title, required this.svgPath, required this.onTab});
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: InkWell(
        onTap: () {
          onTab();
        },
        child: Column(
          children: [
            Container(
              width: 50,
              height: 50,
              decoration: const BoxDecoration(
                  color: AppColors.blackColor,
                  borderRadius: BorderRadius.all(Radius.circular(25))),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.network(
                    svgPath.isNotEmpty
                        ? svgPath
                        : 'https://www.iconsdb.com/icons/preview/white/businessman-xxl.png',
                    width: 25,
                    height: 25,
                    fit: BoxFit.fill,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: size.shortestSide / 32,
                  fontWeight: FontWeight.w500,
                  color: Colors.black),
            )
          ],
        ),
      ),
    );
  }
}
