import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../shared/custom_widgets/svg_icon_button.dart';

import 'product_item.dart';

class ProductsTab extends StatelessWidget {
  const ProductsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
              child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: 4,
            itemBuilder: (context, index) => const ProductItem(
              category: 'أدوات صحية',
              imgUrl: 'https://picsum.photos/100',
              name: 'صنبور ماء ايطالي',
              price: '٥٠ ريال',
            ),
          )),
          Padding(
            padding: EdgeInsets.symmetric(vertical: size.height / 30),
            child: FlatButton(
              color: AppColors.yellowColor,
              padding: EdgeInsets.symmetric(horizontal: size.width / 40),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              onPressed: () {},
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SvgIconButton(
                    svgPath: 'assets/icons/navbar_add.svg',
                    width: size.width / 18,
                    color: AppColors.whiteColor,
                  ),
                  Text(tr('add_to_profile'),
                      style: TextStyle(
                          color: AppColors.whiteColor,
                          fontSize: size.shortestSide / 26,
                          fontWeight: FontWeight.w400)),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
