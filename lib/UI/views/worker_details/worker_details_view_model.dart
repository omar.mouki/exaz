import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/locator.dart';
import '../../../core/data/repository/service_provider_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class WorkerDetailsViewModel extends BaseViewModel {
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  ServiceProviderRepository serviceProviderRepository =
      locator<ServiceProviderRepository>();
  NavigationService navigationService = locator<NavigationService>();

  bool _isFavorite = false;
  bool get isFavorite => _isFavorite;
  set setIsFavorite(bool value) {
    _isFavorite = value;
  }

  int _rate = 0;
  int get rate => _rate;
  set setRate(int value) {
    _rate = value;
  }

  void runAddToFavorite({
    required int serviceProviderId,
  }) {
    runBusyFuture(addToFavorite(serviceProviderId: serviceProviderId),
        busyObject: 'favorite');
    notifyListeners();
  }

  void runRemoveFromFavorite({
    required int serviceProviderId,
  }) {
    runBusyFuture(removeFromFavorite(serviceProviderId: serviceProviderId),
        busyObject: 'favorite');
    notifyListeners();
  }

  void runRateServiceProvider({
    required int serviceProviderId,
  }) {
    runBusyFuture(rateServiceProvider(serviceProviderId: serviceProviderId),
        busyObject: 'rate');
    notifyListeners();
  }

  Future<void> increaseVisitCount({
    required int serviceProviderId,
  }) async {
    try {
      setBusy(true);
      final commonResponse = await serviceProviderRepository.increaseVisitCount(
        serviceProviderId: serviceProviderId,
        serviceProviderType: 0,
      );
      if (commonResponse.getStatus) {
        setBusy(false);
      } else {
        setBusy(false);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.errorMessage);
      print(e);
    }
  }

  Future<void> addToFavorite({
    required int serviceProviderId,
  }) async {
    try {
      final commonResponse = await serviceProviderRepository.addToFavorite(
        serviceProviderId: serviceProviderId,
        serviceProviderType: 0,
      );
      if (commonResponse.getStatus) {
        setIsFavorite = true;
        CustomToasts.showMessage(
            message: 'تمت الإضافة الى المفضلة بنجاح',
            messageType: MessageType.successMessage);
      } else {
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.errorMessage);
      print(e);
    }
  }

  Future<void> removeFromFavorite({
    required int serviceProviderId,
  }) async {
    try {
      final commonResponse = await serviceProviderRepository.removeFromFavorite(
        serviceProviderId: serviceProviderId,
        serviceProviderType: 0,
      );
      if (commonResponse.getStatus) {
        setIsFavorite = false;
        CustomToasts.showMessage(
            message: 'تمت الإزالة من المفضلة بنجاح',
            messageType: MessageType.successMessage);
      } else {
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.errorMessage);
      print(e);
    }
  }

  Future<void> rateServiceProvider({
    required int serviceProviderId,
  }) async {
    try {
      final commonResponse =
          await serviceProviderRepository.rateServiceProvider(
              serviceProviderId: serviceProviderId,
              serviceProviderType: 0,
              rate: rate);
      if (commonResponse.getStatus) {
        CustomToasts.showMessage(
            message: 'تم حفظ التقييم بنجاح',
            messageType: MessageType.successMessage);
      } else {
        setRate = 0;
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.errorMessage);
      print(e);
    }
  }
}
