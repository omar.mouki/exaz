import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/star_rating.dart';
import 'package:exaaz/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';

import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart' show NavigationService;
import 'package:url_launcher/url_launcher.dart';

import '../../../app/locator.dart';
import '../../../core/data/models/profile_info.dart';
import '../../../core/data/models/worker.dart';
import '../../../core/data/models/worker_profile_data.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../shared/custom_widgets/custom_appbar.dart';
import '../../shared/custom_widgets/custom_image_network.dart';
import '../../shared/custom_widgets/custom_lodaer.dart';
import '../../shared/custom_widgets/custom_toasts.dart';
import '../../shared/custom_widgets/svg_icon_button.dart';
import 'worker_details_view_model.dart';
import 'worker_details_widgets/worker_details_services.dart';

class WorkerDetailsView extends StatefulWidget {
  final WorkerModel worker;
  WorkerDetailsView({Key? key, required this.worker}) : super(key: key);

  @override
  _WorkerProfileViewState createState() => _WorkerProfileViewState();
}

class _WorkerProfileViewState extends State<WorkerDetailsView>
    with SingleTickerProviderStateMixin {
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();

  late ProfileInfo profile;
  late WorkerModel worker;
  WorkerProfileData _workerProfileData = locator<WorkerProfileData>();
  late String imageUrl;
  bool isLoggedIn = Utils.isLoogedIn;
  @override
  void initState() {
    profile = sharedPreferencesRepository.getProfileInfo()!;

    _workerProfileData.addDefaultDataToMap();
    worker = widget.worker;

    for (int i = 0; i < worker.fees.length; i++) {
      for (int j = 0; j < worker.fees[i].feeIncludedFeatures.length; j++) {
        if (worker.fees[i].feeType == 1) {
          _workerProfileData.houeFee!.value = worker.fees[i].value;
          if (worker.fees[i].feeIncludedFeatures[j].id == 1) {
            _workerProfileData.houeFeeMap![tr('include_food')] = true;
          } else if (worker.fees[i].feeIncludedFeatures[j].id == 2) {
            _workerProfileData.houeFeeMap![tr('includes_transportation')] =
                true;
          }
        } else if (worker.fees[i].feeType == 2) {
          _workerProfileData.dayFee!.value = worker.fees[i].value;
          if (worker.fees[i].feeIncludedFeatures[j].id == 1) {
            _workerProfileData.dayFeeMap![tr('include_food')] = true;
          } else if (worker.fees[i].feeIncludedFeatures[j].id == 2) {
            _workerProfileData.dayFeeMap![tr('includes_transportation')] = true;
          }
        } else if (worker.fees[i].feeType == 3) {
          _workerProfileData.monthFee!.value = worker.fees[i].value;
          if (worker.fees[i].feeIncludedFeatures[j].id == 1) {
            _workerProfileData.monthFeeMap![tr('include_food')] = true;
          } else if (worker.fees[i].feeIncludedFeatures[j].id == 2) {
            _workerProfileData.monthFeeMap![tr('includes_transportation')] =
                true;
          } else if (worker.fees[i].feeIncludedFeatures[j].id == 4) {
            _workerProfileData.monthFeeMap![tr('includes_housing')] = true;
          }
        }
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    _workerProfileData.clearDate();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: AppColors.blackColor,
        body: ViewModelBuilder<WorkerDetailsViewModel>.reactive(
          viewModelBuilder: () => WorkerDetailsViewModel(),
          onModelReady: (model) {
            model.setIsFavorite = worker.isFavorite ?? false;
            if (isLoggedIn) {
              model.increaseVisitCount(serviceProviderId: worker.workerId);
            }
          },
          builder: (context, model, child) => model.isBusy
              ? Center(
                  child: CustomLoader(),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomAppBar(
                      backOnTab: () {
                        locator<NavigationService>().back();
                      },
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: size.width / 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const SizedBox(
                                height: 16,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: AspectRatio(
                                      aspectRatio: 1,
                                      child: CircularImageNetwork(
                                        imgFit: BoxFit.fill,

                                        // imageFile: model.avatarImgage,
                                        isSquare: true,
                                        imagePath: worker.pictureUrl,
                                        // imageErrorBackroundColor: AppColors.grey100Color,
                                        // imageErrorColor: AppColors.grey200Color,
                                        placeHolderSize: size.width / 12,
                                        placeHolderSvgPath:
                                            'assets/icons/photo_place_holder.svg',
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: size.width / 75),
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                tr('membership_number') + ': ',
                                                style: TextStyle(
                                                    color: AppColors.whiteColor,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize:
                                                        size.shortestSide / 30),
                                              ),
                                              Text(
                                                worker.workerId.toString(),
                                                style: TextStyle(
                                                    color: AppColors.whiteColor,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize:
                                                        size.shortestSide / 30),
                                              ),
                                              const Spacer(),
                                              SvgIconButton(
                                                svgPath:
                                                    'assets/icons/share.svg',
                                                width: size.width / 21,
                                                color: AppColors.whiteColor,
                                                onPressed: () {
                                                  if (worker.shareProfileLink !=
                                                      null) {
                                                    FlutterShare.share(
                                                        title: '',
                                                        text: '',
                                                        linkUrl: worker
                                                            .shareProfileLink,
                                                        chooserTitle: '');
                                                  } else {
                                                    CustomToasts.showMessage(
                                                        message:
                                                            'shareProfileLink is return null from api',
                                                        messageType: MessageType
                                                            .errorMessage);
                                                  }
                                                },
                                              ),
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Row(
                                                children: [
                                                  Icon(
                                                    Icons.visibility,
                                                    color: AppColors.whiteColor,
                                                    size: size.width / 21,
                                                  ),
                                                  const SizedBox(
                                                    width: 8,
                                                  ),
                                                  Text(
                                                    worker.profileVisitCount
                                                        .toString(),
                                                    style: TextStyle(
                                                        color: AppColors
                                                            .whiteColor,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize:
                                                            size.shortestSide /
                                                                30),
                                                  ),
                                                ],
                                              ),
                                              const Spacer(),
                                              if (isLoggedIn)
                                                model.busy('favorite')
                                                    ? CustomLoader()
                                                    : InkWell(
                                                        onTap: () {
                                                          if (model
                                                              .isFavorite) {
                                                            //!model.Remove
                                                            model.runRemoveFromFavorite(
                                                                serviceProviderId:
                                                                    worker
                                                                        .workerId);
                                                          } else {
                                                            //!model.add
                                                            model.runAddToFavorite(
                                                                serviceProviderId:
                                                                    worker
                                                                        .workerId);
                                                          }
                                                        },
                                                        child: Icon(
                                                          model.isFavorite
                                                              ? Icons.favorite
                                                              : Icons
                                                                  .favorite_border,
                                                          color: AppColors
                                                              .whiteColor,
                                                          size: size.width / 21,
                                                        ),
                                                      ),
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              if (isLoggedIn)
                                                model.busy('rate')
                                                    ? CustomLoader()
                                                    // smootrratinf
                                                    : StarRating(
                                                        // allowHalfRating: false,
                                                        onRatingChanged: (v) {
                                                          model.setRate =
                                                              v.toInt();
                                                          model.runRateServiceProvider(
                                                              serviceProviderId:
                                                                  worker
                                                                      .workerId);
                                                        },
                                                        starCount: 5,

                                                        rating: model.rate
                                                            .toDouble(),

                                                        // size: size.width / 17,

                                                        color: AppColors
                                                            .yellowColor,
                                                        // borderColor: AppColors
                                                        //     .yellowColor,
                                                        // spacing: 0.0
                                                      ),
                                              const Spacer(),
                                              Container(
                                                width: size.width / 7.8,
                                                height: size.width / 15,
                                                color: AppColors.yellowColor,
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 3),
                                                child: Row(
                                                  children: [
                                                    Icon(
                                                      Icons.star,
                                                      color:
                                                          AppColors.whiteColor,
                                                      size: size.width / 21,
                                                    ),
                                                    const Spacer(),
                                                    Text(
                                                      worker.rating
                                                              ?.toString() ??
                                                          '0.0',
                                                      style: TextStyle(
                                                          color: AppColors
                                                              .whiteColor,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize:
                                                              size.shortestSide /
                                                                  25),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              const Spacer(),
                                              SvgIconButton(
                                                svgPath:
                                                    'assets/icons/info_phone.svg',
                                                width: size.width / 15,
                                                color: AppColors.yellowColor,
                                                onPressed: () {
                                                  launch(
                                                      'tel:+${worker.userName}');
                                                },
                                              ),
                                              SvgIconButton(
                                                svgPath:
                                                    'assets/icons/info_whatsapp.svg',
                                                width: size.width / 15,
                                                color: AppColors.yellowColor,
                                                onPressed: () {
                                                  Utils.openwhatsapp(
                                                      worker.userName);
                                                },
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Text(
                                profile.fullName!,
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: size.shortestSide / 24),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: [
                                  Text(
                                    worker.professionName!,
                                    style: TextStyle(
                                        color: AppColors.yellowColor,
                                        fontWeight: FontWeight.w500,
                                        fontSize: size.shortestSide / 30),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    profile.city!,
                                    style: TextStyle(
                                        color: AppColors.grey200Color,
                                        fontWeight: FontWeight.w500,
                                        fontSize: size.shortestSide / 30),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              WorkerDetailsServices(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ));
  }
}
