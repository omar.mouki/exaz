import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../../../../app/locator.dart';
import '../../../../core/data/models/worker_fee_dto.dart';
import '../../../../core/data/models/worker_profile_data.dart';
import 'worker_details_service_item.dart';

class WorkerDetailsServices extends StatefulWidget {
  const WorkerDetailsServices({
    Key? key,
  }) : super(key: key);
  @override
  _WorkerServicesTabState createState() => _WorkerServicesTabState();
}

class _WorkerServicesTabState extends State<WorkerDetailsServices> {
  WorkerProfileData _workerProfileData = locator<WorkerProfileData>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          children: <Widget>[
            Text(
              'خدماتي',
              style: TextStyle(
                  color: AppColors.yellowColor,
                  fontSize: size.shortestSide / 24),
            ),
            const SizedBox(
              width: 12,
            ),
          ],
        ),
        WorkerDetailsServiceItem(
          value: _workerProfileData.houeFee!.value,
          title: tr('hour_fee'),
          valueCallback: (value) {
            _workerProfileData.houeFee!.value = double.parse(value);
          },
          checkItemsCallback: (list) {
            for (var item in list) {
              if (item.value!) {
                _workerProfileData.houeFee!.addToList(FeeIncloudType(
                    id: int.parse(item.id), value: int.parse(item.id)));
                _workerProfileData.houeFeeMap![item.name] = true;
              } else {
                _workerProfileData.houeFee!.removeFromList(int.parse(item.id));
                _workerProfileData.houeFeeMap![item.name] = false;
              }
            }
          },
          checkItems: [
            CheckItem(
              value:
                  _workerProfileData.houeFeeMap![tr('include_food')] ?? false,
              id: '1',
              name: tr('include_food'),
            ),
            CheckItem(
              value: _workerProfileData
                      .houeFeeMap![tr('includes_transportation')] ??
                  false,
              id: '2',
              name: tr('includes_transportation'),
            ),
          ],
        ),
        WorkerDetailsServiceItem(
          value: _workerProfileData.dayFee!.value,
          title: tr('day_fee'),
          valueCallback: (value) {
            _workerProfileData.dayFee!.value = double.parse(value);
          },
          checkItemsCallback: (list) {
            for (var item in list) {
              if (item.value!) {
                _workerProfileData.dayFee!.addToList(FeeIncloudType(
                    id: int.parse(item.id), value: int.parse(item.id)));

                _workerProfileData.dayFeeMap![item.name] = true;
              } else {
                _workerProfileData.dayFee!.removeFromList(int.parse(item.id));
                _workerProfileData.dayFeeMap![item.name] = false;
              }
            }
          },
          checkItems: [
            CheckItem(
              value: _workerProfileData.dayFeeMap![tr('include_food')] ?? false,
              id: '1',
              name: tr('include_food'),
            ),
            CheckItem(
              value: _workerProfileData
                      .dayFeeMap![tr('includes_transportation')] ??
                  false,
              id: '2',
              name: tr('includes_transportation'),
            ),
          ],
        ),
        WorkerDetailsServiceItem(
          value: _workerProfileData.monthFee!.value,
          title: tr('month_fee'),
          valueCallback: (value) {
            _workerProfileData.monthFee!.value = double.parse(value);
          },
          checkItemsCallback: (list) {
            for (var item in list) {
              if (item.value!) {
                _workerProfileData.monthFee!.addToList(FeeIncloudType(
                    id: int.parse(item.id), value: int.parse(item.id)));

                _workerProfileData.monthFeeMap![item.name] = true;
              } else {
                _workerProfileData.monthFee!.removeFromList(int.parse(item.id));

                _workerProfileData.monthFeeMap![item.name] = false;
              }
            }
          },
          checkItems: [
            CheckItem(
              value:
                  _workerProfileData.monthFeeMap![tr('include_food')] ?? false,
              id: '1',
              name: tr('include_food'),
            ),
            CheckItem(
              value: _workerProfileData
                      .monthFeeMap![tr('includes_transportation')] ??
                  false,
              id: '2',
              name: tr('includes_transportation'),
            ),
            CheckItem(
              value: _workerProfileData.monthFeeMap![tr('includes_housing')] ??
                  false,
              id: '4',
              name: tr('includes_housing'),
            )
          ],
        ),
      ],
    );
  }
}
