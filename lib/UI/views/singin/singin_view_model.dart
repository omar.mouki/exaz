import 'package:exaaz/app/router.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../core/data/models/token_info.dart';
import '../../../core/data/repository/auth_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../../core/utils/social_media_util.dart';
import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class SinginViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  NavigationService _navigationService = locator<NavigationService>();

  Future<void> singinWithFacebook() async {
    // String token = await SocialMediaUtil().signInWithFacebook();
    // if (token != null) {
    //   print(token);
    //   tokenByFacebookTokenRequest(token: token);
    // } else {
    //   CustomToasts.showMessage(
    //       message: 'Facebok token is null',
    //       messageType: MessageType.errorMessage);
    // }
  }

  Future<void> singinWithGoogle() async {
    String? token = await SocialMediaUtil().signInWithGoogle();
    if (token != null) {
      print(token);
      tokenByGoogleTokenRequest(token: token);
    } else {
      CustomToasts.showMessage(
          message: 'Google token is null',
          messageType: MessageType.errorMessage);
    }
  }

  void tokenByUsernameAndPassword(
      {required String phoneNumber, required String password}) async {
    try {
      setBusy(true);
      final commonResponse =
          await _authenticationRepository.tokenByMobileNumber(
        password: password,
        phoneNumber: phoneNumber,
      );
      if (commonResponse.getStatus) {
        sharedPreferencesRepository.saveTokenInfo(
            tokenInfo: TokenInfo.fromJson(commonResponse.getData));
        sharedPreferencesRepository.setLoggedIn(isLoggedIn: true);
        _navigationService.pushNamedAndRemoveUntil(Routes.homeView);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void tokenByFacebookTokenRequest({required String token}) async {
    try {
      setBusy(true);
      final commonResponse =
          await _authenticationRepository.tokenByFacebookToken(token: token);
      if (commonResponse.getStatus) {
        sharedPreferencesRepository.saveTokenInfo(
            tokenInfo: TokenInfo.fromJson(commonResponse.getData));
        sharedPreferencesRepository.setLoggedIn(isLoggedIn: true);
        _navigationService.pushNamedAndRemoveUntil(Routes.landingView);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void tokenByGoogleTokenRequest({required String token}) async {
    try {
      setBusy(true);
      final commonResponse =
          await _authenticationRepository.tokenByGoogleToken(token: token);
      if (commonResponse.getStatus) {
        sharedPreferencesRepository.saveTokenInfo(
            tokenInfo: TokenInfo.fromJson(commonResponse.getData));
        sharedPreferencesRepository.setLoggedIn(isLoggedIn: true);
        _navigationService.pushNamedAndRemoveUntil(Routes.landingView);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
