import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';
import 'package:exaaz/UI/shared/custom_widgets/text_field_widget.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import 'singin_view_model.dart';

class SignInView extends StatefulWidget {
  const SignInView({Key? key}) : super(key: key);

  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  TextEditingController _phonecontroller = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      // drawer: CustomDrawer(),
      body: ViewModelBuilder<SinginViewModel>.reactive(
        viewModelBuilder: () => SinginViewModel(),
        builder: (context, model, child) => model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/pngs/workshop_bg.png',
                    fit: BoxFit.cover,
                    width: size.width,
                    height: size.height,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: size.width / 20),
                    child: SingleChildScrollView(
                      child: SizedBox(
                        width: size.width,
                        height: size.height,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(flex: 3, child: const SizedBox()),
                            Text(
                              tr('sign_in'),
                              style: TextStyle(
                                fontSize: size.width / 22,
                                color: AppColors.blackColor,
                              ),
                            ),
                            SizedBox(
                              height: size.height / 40,
                            ),
                            TextFieldWidget(
                              textEditingController: _phonecontroller,
                              title: tr('phone_number'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              textColor: AppColors.blackColor,
                              keyboardType: TextInputType.phone,
                              maxLength: 10,
                            ),
                            TextFieldWidget(
                              textEditingController: _passwordController,
                              title: tr('password'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              textColor: AppColors.blackColor,
                              keyboardType: TextInputType.visiblePassword,
                            ),
                            InkWell(
                              onTap: () {
                                locator<NavigationService>()
                                    .navigateTo(Routes.forgetPasswordView);
                              },
                              child: Text(
                                tr('forget_password'),
                                style: TextStyle(
                                  fontSize: size.width / 28,
                                  color: AppColors.blackColor,
                                ),
                              ),
                            ),
                            const Expanded(flex: 1, child: SizedBox()),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  tr('or_enter_by'),
                                  style: TextStyle(
                                    fontSize: size.width / 28,
                                    color: AppColors.grey300Color,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SvgIconButton(
                                      onPressed: () async {
                                        model.singinWithFacebook();
                                      },
                                      svgPath: 'assets/icons/facebook_logo.svg',
                                      width: size.width / 12,
                                    ),
                                    SvgIconButton(
                                      onPressed: () async {
                                        model.singinWithGoogle();
                                      },
                                      svgPath: 'assets/icons/google_logo.svg',
                                      width: size.width / 12,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: size.height / 40,
                                ),
                                FlatButton(
                                  color: AppColors.blackColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  onPressed: () {
                                    if (_phonecontroller.text.length < 10) {
                                      CustomToasts.showMessage(
                                          message: tr(
                                              'ReportProblemController_invalidePhone'),
                                          messageType:
                                              MessageType.errorMessage);
                                    } else if (_passwordController
                                            .text.length ==
                                        0) {
                                      CustomToasts.showMessage(
                                          message: tr('please_enter_password'),
                                          messageType:
                                              MessageType.errorMessage);
                                    } else {
                                      model.tokenByUsernameAndPassword(
                                          phoneNumber: _phonecontroller.text,
                                          password: _passwordController.text);
                                    }
                                  },
                                  child: SizedBox(
                                    width: size.width / 1.2,
                                    child: Center(
                                      child: Text(tr('sign_in'),
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: size.shortestSide / 21,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                ),
                                FlatButton(
                                  color: AppColors.blackColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  onPressed: () {
                                    _navigationService
                                        .navigateTo(Routes.signUpView);
                                  },
                                  child: SizedBox(
                                    width: size.width / 1.2,
                                    child: Center(
                                      child: Text(tr('sign_up'),
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: size.shortestSide / 21,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                ),
                                FlatButton(
                                  color: AppColors.blackColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  onPressed: () {},
                                  child: SizedBox(
                                    width: size.width / 1.2,
                                    child: Center(
                                      child: Text(tr('pass'),
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: size.shortestSide / 21,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Expanded(flex: 2, child: const SizedBox()),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }
}
