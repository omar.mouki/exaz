import 'dart:developer';

import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/product/product_catigory.dart';
import 'package:exaaz/core/data/models/product/product_model.dart';

import 'package:exaaz/core/data/repository/product_repository.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:stacked/stacked.dart';

class ProductsListViewModel extends BaseViewModel {
  static const _pageSize = 10;
  int selectedCatigory = -1;
  int _catId = -1;
  List<ProductCatigory> productCatigoryList = [];

  late int _storeId;
  int get storeId => _storeId;
  set storeId(int val) {
    _storeId = val;
  }

  final PagingController<int, ProductModel> pagingController =
      PagingController(firstPageKey: 0);
  void listen() {
    // _catId = catigoryId;
    pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });

    // pagingController.re
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final commonResponse = await locator<StoreRepository>().getStoreProducts(
          pageIndex: pageKey,
          pageSize: _pageSize,
          categoryId: _catId,
          storeId: storeId);

      Iterable list = commonResponse.data;
      List<ProductModel> newList = [];

      list.forEach((element) {
        newList.add(ProductModel.fromJson(element));
      });
      // newList = [];
      final newItems = newList;
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  void loadProductCatigory(int storeId) async {
    setBusy(true);
    try {
      final commonResponse = await locator<ProductRepository>()
          .getCategories(storeId: storeId.toString());
      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());
        productCatigoryList.clear();
        Iterable list = commonResponse.data;
        list.forEach((element) {
          productCatigoryList.add(ProductCatigory.fromJson(element));
        });
        _catId = productCatigoryList.first.id;
        selectedCatigory = 0;

        listen();
        // listen(productCatigoryList.first.id);
        setBusy(false);

        notifyListeners();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }

  void catigorySelected(int index) {
    selectedCatigory = index;
    _catId = productCatigoryList[index].id;
    // productCatigoryList[index].id
    // listen(productCatigoryList[index].id);
    pagingController.refresh();
    notifyListeners();
  }
}
