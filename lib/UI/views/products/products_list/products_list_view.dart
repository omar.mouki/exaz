import 'dart:developer';

import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/share_appbar.dart';
import 'package:exaaz/UI/views/products/products_list/products_list_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/product/product_model.dart';

import 'package:exaaz/core/data/models/store.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProductsListView extends StatelessWidget {
  final Store store;
  const ProductsListView({Key? key, required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ViewModelBuilder<ProductsListViewModel>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : Column(
                children: [
                  ShareAppBar(
                    backOnTab: () {
                      locator<NavigationService>().back();
                    },
                    title: store.name,
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  SizedBox(
                    height: 40,
                    width: size.width,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: model.productCatigoryList.length,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      itemBuilder: (context, index) => InkWell(
                        onTap: () {
                          model.catigorySelected(index);
                        },
                        child: Container(
                          margin: const EdgeInsetsDirectional.only(end: 8),
                          padding: const EdgeInsets.symmetric(horizontal: 40),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: model.selectedCatigory == index
                                  ? AppColors.yellowColor
                                  : AppColors.whiteColor,
                            ),
                            borderRadius:
                                BorderRadius.circular(size.width / 40),
                          ),
                          child: Text(
                            model.productCatigoryList[index].name,
                            style: TextStyle(
                                fontSize: size.width / 22,
                                color: AppColors.whiteColor),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: PagedGridView<int, ProductModel>(
                      pagingController: model.pagingController,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisExtent: 350,
                        // maxCrossAxisExtent: 100,
                        // childAspectRatio: 0.55,
                        // crossAxisSpacing: 5,
                        // mainAxisSpacing: 5
                      ),
                      builderDelegate: PagedChildBuilderDelegate<ProductModel>(

                          // noItemsFoundIndicatorBuilder: (context) => Text(
                          //   'no item found',
                          //   style: const TextStyle(color: Colors.white),
                          // ),
                          // noMoreItemsIndicatorBuilder: (context) => Text(
                          //   'no more',
                          //   style: const TextStyle(color: Colors.white),
                          // ),
                          itemBuilder: (context, item, index) =>
                              ProductListTile(
                                product: item,
                              )),
                    ),
                  ),
                ],
              ),
      ),
      viewModelBuilder: () => ProductsListViewModel(),
      onModelReady: (model) {
        model.storeId = store.storeId;
        // model.listen();
        model.loadProductCatigory(store.storeId);
      },
    );
  }
}

class ProductListTile extends StatelessWidget {
  final ProductModel product;
  const ProductListTile({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        locator<NavigationService>().navigateTo(Routes.productDetailsView,
            arguments: ProductDetailsViewArguments(productId: product.id));
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
        ),
        padding: EdgeInsets.all(8),
        // height: 300,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              height: 260,
              width: double.infinity,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.network(product.picture, fit: BoxFit.cover),
              ),
            ),
            Container(
              padding: EdgeInsets.only(right: 7, left: 7),
              height: 70,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Colors.grey[800]!,
                      Colors.black54,
                    ],
                    begin: const FractionalOffset(1.0, 0.0),
                    end: const FractionalOffset(0.0, 0.0),
                    stops: const [0.0, 1.0],
                    tileMode: TileMode.clamp),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    product.name,
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 19,
                        fontWeight: FontWeight.bold),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        product.price.toString() + ' ' + 'ريال',
                        style: const TextStyle(
                            color: AppColors.yellowColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w500),
                      ),
                      InkWell(
                        onTap: () {
                          log('fav clicked');
                        },
                        child: Icon(
                          Icons.favorite_border,
                          color: AppColors.yellowColor,
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
