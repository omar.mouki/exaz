import 'dart:developer';

import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/product/product_details_model.dart';
import 'package:exaaz/core/data/repository/product_repository.dart';
import 'package:stacked/stacked.dart';

class ProductDetailsViewModl extends BaseViewModel {
  final _productRepo = locator<ProductRepository>();
  ProductDetailsModel? product;

  void loadData({required int productId}) async {
    setBusy(true);

    try {
      final commonResponse =
          await _productRepo.getProductDetails(productId: productId);
      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());
        product = ProductDetailsModel.fromJson(commonResponse.data);
        setBusy(false);

        notifyListeners();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }
}
