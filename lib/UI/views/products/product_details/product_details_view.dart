import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_image_swiper.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/star_rating.dart';
import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';
import 'package:exaaz/UI/views/products/product_details/product_details_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/product/product_details_model.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProductDetailsView extends StatelessWidget {
  final int productId;
  const ProductDetailsView({Key? key, required this.productId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ViewModelBuilder<ProductDetailsViewModl>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: model.isBusy
            ? CustomLoader()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    ProductDetailsAppBar(
                      size: size,
                      title: model.product?.name ?? 'title',
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    // , left: 8, right: 8

                    Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Column(
                        children: [
                          CustomImageSwiper(
                            size: size,
                            itemCount:
                                model.product?.productPictures.length ?? 0,
                            imageUrlList: model.product == null
                                ? []
                                : model.product!.productPictures
                                    .map((e) => e.pictureUrl)
                                    .toList(),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${model.product?.price.toString()} ريال',
                                style: const TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.share,
                                    color: AppColors.yellowColor,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    'تقييم المنتج',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        color: Colors.white60,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )
                            ],
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                      text: " كود المنتج ",
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.yellowColor,
                                        fontSize: 16,
                                      ),
                                    ),
                                    TextSpan(
                                      text: model.product?.productCode,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                              StarRating()
                            ],
                          ),
                          Divider(
                            color: Colors.white60,
                          ),
                          DefaultTabController(
                            length: 2,
                            // length: 3,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  child: TabBar(
                                      onTap: (index) {},
                                      indicatorColor: AppColors.yellowColor,
                                      isScrollable: false,
                                      tabs: [
                                        Tab(text: 'نظرة عامة'),
                                        Tab(text: 'المواصفات'),
                                        // Tab(text: 'آراء العملاء'),
                                      ]),
                                ),
                                ProductDetailsTabsSection(
                                  product: model.product,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
      viewModelBuilder: () => ProductDetailsViewModl(),
      onModelReady: (model) {
        model.loadData(productId: productId);
      },
    );
  }
}

class ProductDetailsTabsSection extends StatelessWidget {
  final ProductDetailsModel? product;
  const ProductDetailsTabsSection({
    Key? key,
    this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //Add this to give height
      height: MediaQuery.of(context).size.height / 3,
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                child: Text(
                  product?.description ?? '',
                  style: const TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              // Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ProductDetailsCirculs(
                    iconData: LineIcons.globe,
                    title: 'بلد المنشا',
                    subTitle: product?.originCountryName ?? '',
                  ),
                  ProductDetailsCirculs(
                    iconData: LineIcons.fingerprint,
                    title: 'ماركة المنتج',
                    subTitle: product?.brand ?? '',
                  ),
                  ProductDetailsCirculs(
                    iconData: LineIcons.medal,
                    title: 'مترة الضمان',
                    subTitle: product?.warrantyPeriodTypeName ?? '',
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
          ListView.builder(
              shrinkWrap: true,
              // physics:
              //     NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => Container(
                    color:
                        index.isEven ? Colors.grey[700] : AppColors.blackColor,
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            product?.productAttributes[index].attributeName ??
                                '',
                            style: const TextStyle(
                                fontSize: 20, color: Colors.white),
                          ),
                        ),
                        Text(
                          product?.productAttributes[index].attributeValue ??
                              '',
                          style: const TextStyle(
                              fontSize: 20, color: Colors.white60),
                        ),
                      ],
                    ),
                  ),
              itemCount: product?.productAttributes.length ?? 0),

          // customer review tab
          // Container(),
        ],
      ),
    );
  }
}

class ProductDetailsCirculs extends StatelessWidget {
  final IconData iconData;
  final String title;
  final String subTitle;
  const ProductDetailsCirculs({
    Key? key,
    required this.iconData,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            color: AppColors.yellowColor,
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Icon(
              iconData,
              size: 40,
            ),
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Text(
          title,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
        Text(
          subTitle,
          style: const TextStyle(
            color: Colors.white60,
          ),
        )
      ],
    );
  }
}

class ProductDetailsAppBar extends StatelessWidget {
  final String title;
  const ProductDetailsAppBar({
    Key? key,
    required this.size,
    required this.title,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: size.longestSide / 6,
      color: AppColors.darkGreyColor,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.symmetric(horizontal: size.width / 32, vertical: 8),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 19),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  'رجوع',
                  style: TextStyle(
                      color: AppColors.yellowColor,
                      fontWeight: FontWeight.w500,
                      fontSize: size.width / 18),
                ),
                SvgIconButton(
                  svgPath: 'assets/icons/arrow_back.svg',
                  width: size.width / 18,
                  onPressed: () {
                    locator<NavigationService>().back();
                  },
                ),
                // if (title != null)
              ],
            ),
          ),
          Text(
            title,
            style: TextStyle(
                color: AppColors.whiteColor,
                fontWeight: FontWeight.w500,
                fontSize: size.width / 18),
          ),
        ],
      ),
    );
  }
}
