import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class PdfView extends StatelessWidget {
  final String pdfUrl;

  const PdfView({Key? key, required this.pdfUrl}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: AppColors.blackColor,
      body: const PDF().fromUrl(
        pdfUrl,
        placeholder: (double progress) => Center(
            child: CircularProgressIndicator(
          value: progress,
          color: AppColors.yellowColor,
        )),
        errorWidget: (dynamic error) => Center(child: Text(error.toString())),
      ),
    );
  }
}

// const PDF().fromUrl(
//           pdfUrl,
//           placeholder: (double progress) => Center(
//               child: CircularProgressIndicator(
//             value: progress,
//             color: AppColors.yellowColor,
//           )),
//           errorWidget: (dynamic error) => Center(child: Text(error.toString())),
//         )


//     return Scaffold(
//       backgroundColor: AppColors.blackColor,
//       body: ColorFiltered(
//         colorFilter: ColorFilter.mode(Colors.white, BlendMode.difference),
//         child: const PDF().fromUrl(
//           pdfUrl,
//           placeholder: (double progress) => Center(
//               child: CircularProgressIndicator(
//             value: progress,
//             color: AppColors.yellowColor,
//           )),
//           errorWidget: (dynamic error) => Center(child: Text(error.toString())),
//         ),
//       ),
//     );
//   }
// }