import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'splash_screen_view_model.dart';

class SplashView extends StatefulWidget {
  SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ViewModelBuilder<SplashScreenViewModel>.reactive(
            viewModelBuilder: () => SplashScreenViewModel(),
            onModelReady: (model) {
              model.checkLogin();
            },
            builder: (context, model, child) => Container()));
  }
}
