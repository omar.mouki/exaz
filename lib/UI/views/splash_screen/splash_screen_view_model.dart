import 'package:exaaz/app/router.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../../app/locator.dart';

class SplashScreenViewModel extends BaseViewModel {
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();

  NavigationService _navigationService = locator<NavigationService>();

  checkLogin() async {
    await Future.delayed(Duration(milliseconds: 1200));

    if (sharedPreferencesRepository.getLoggedIn()) {
      _navigationService.replaceWith(Routes.homeView);
    } else {
      _navigationService.replaceWith(Routes.signInView);
    }
  }
}
