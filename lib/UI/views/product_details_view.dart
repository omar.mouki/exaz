import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../shared/custom_widgets/custom_appbar.dart';
import '../shared/custom_widgets/custom_image_network.dart';
import '../shared/custom_widgets/text_field_widget.dart';

class ProductDetailsView extends StatefulWidget {
  ProductDetailsView({Key? key}) : super(key: key);

  @override
  _ProductDetailsViewState createState() => _ProductDetailsViewState();
}

class _ProductDetailsViewState extends State<ProductDetailsView> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.blackColor,
      body: Column(
        children: <Widget>[
          CustomAppBar(),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: size.width / 22),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: size.height / 12,
                    ),
                    Row(
                      children: const <Widget>[
                        Expanded(
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: CircularImageNetwork(
                              // imageErrorPath: 'assets/icons/image_add.svg',
                              imagePath: '',
                              // errorImageSize: size.width / 12,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: CircularImageNetwork(
                              imagePath: '',
                              // errorImageSize: size.width / 12,
                              // imageErrorPath: 'assets/icons/image_add.svg',
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: CircularImageNetwork(
                              imagePath: '',
                              // errorImageSize: size.width / 12,
                              // imageErrorPath: 'assets/icons/image_add.svg',
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: size.height / 7,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            tr('product_type'),
                            style: TextStyle(
                                color: AppColors.grey200Color,
                                fontWeight: FontWeight.bold,
                                fontSize: size.shortestSide / 24),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                            flex: 5,
                            child: TextFieldWidget(
                              title: '',
                              isDense: true,
                            ))
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            tr('product_name'),
                            style: TextStyle(
                                color: AppColors.grey200Color,
                                fontWeight: FontWeight.bold,
                                fontSize: size.shortestSide / 24),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                            flex: 5,
                            child: TextFieldWidget(
                              title: '',
                              isDense: true,
                            ))
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            tr('product_code'),
                            style: TextStyle(
                                color: AppColors.grey200Color,
                                fontWeight: FontWeight.bold,
                                fontSize: size.shortestSide / 24),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                            flex: 5,
                            child: TextFieldWidget(
                              title: '',
                              isDense: true,
                            ))
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            tr('product_details'),
                            style: TextStyle(
                                color: AppColors.grey200Color,
                                fontWeight: FontWeight.bold,
                                fontSize: size.shortestSide / 24),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                            flex: 5,
                            child: TextFieldWidget(
                              title: '',
                              isDense: true,
                            ))
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Text(
                            tr('product_price'),
                            style: TextStyle(
                                color: AppColors.grey200Color,
                                fontWeight: FontWeight.bold,
                                fontSize: size.shortestSide / 24),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                            flex: 5,
                            child: TextFieldWidget(
                              title: '',
                              isDense: true,
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          tr('product_overview'),
                          style: TextStyle(
                              color: AppColors.grey200Color,
                              fontWeight: FontWeight.bold,
                              fontSize: size.shortestSide / 24),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const TextField(
                          maxLines: 4,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1, color: AppColors.grey300Color),
                              ),
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    width: 1, color: AppColors.grey300Color),
                              ),
                              filled: true,
                              fillColor: AppColors.darkGreyColor,
                              hintText: ''),
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
