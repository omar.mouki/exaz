import 'dart:developer';

import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/store.dart';
import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

class StoreListTile extends StatelessWidget {
  final Store store;
  const StoreListTile({
    Key? key,
    required this.store,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        locator<NavigationService>().navigateTo(Routes.storeDetailsView,
            arguments: StoreDetailsViewArguments(stoerId: store.storeId));
      },
      child: Container(
        margin: const EdgeInsets.all(8),
        height: 210,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Container(
              height: 130,
              width: double.infinity,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                child: Image.network(
                  store.storeImageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
                color: AppColors.yellowColor,
              ),
              width: double.infinity,
              height: 70,
              child: Padding(
                padding: const EdgeInsets.only(left: 12, right: 12),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          store.name,
                          style: const TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        Container(
                          color: Colors.white,
                          child: Row(
                            children: [
                              const Icon(
                                Icons.star,
                                color: AppColors.yellowColor,
                                size: 14,
                              ),
                              Text(
                                store.rating?.toString() ?? 0.0.toString(),
                                style: const TextStyle(
                                  color: AppColors.yellowColor,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(store.cityName),
                        Text(store.areaName),
                        Row(
                          children: [
                            InkWell(
                              onTap: () {
                                log('fav');
                              },
                              child: const Icon(
                                Icons.favorite_border,
                                size: 18,
                                color: Colors.white,
                              ),
                            ),
                            const SizedBox(
                              width: 6,
                            ),
                            InkWell(
                              onTap: () {
                                log('share');
                              },
                              child: const Icon(
                                Icons.share_outlined,
                                size: 18,
                                color: Colors.white,
                              ),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
