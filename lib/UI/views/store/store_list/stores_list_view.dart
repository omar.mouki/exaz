import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/list_error_widget.dart';
import 'package:exaaz/UI/shared/custom_widgets/list_no_data_widget.dart';
import 'package:exaaz/UI/shared/custom_widgets/share_appbar.dart';
import 'package:exaaz/UI/views/store/store_list/componants/store_list_tile.dart';
import 'package:exaaz/UI/views/store/store_list/stores_list_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/category.dart';
import 'package:exaaz/core/data/models/store.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class StoreListView extends StatelessWidget {
  final Category category;
  const StoreListView({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StoreListViewModel>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : Column(
                children: [
                  ShareAppBar(
                    backOnTab: () {
                      locator<NavigationService>().back();
                    },
                    title: category.name,
                  ),
                  Expanded(
                    child: PagedListView<int, Store>(
                      pagingController: model.pagingController,
                      builderDelegate: PagedChildBuilderDelegate<Store>(
                        noItemsFoundIndicatorBuilder: (context) {
                          return const ListNoDataWidget();
                        },
                        firstPageErrorIndicatorBuilder: (context) =>
                            ListErrorWidget(
                          pagingController: model.pagingController,
                        ),
                        itemBuilder: (context, item, index) => StoreListTile(
                          store: item,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
      ),
      viewModelBuilder: () => StoreListViewModel(),
      onModelReady: (model) {
        model.catId = category.id;
        model.listen();
      },
    );
  }
}
