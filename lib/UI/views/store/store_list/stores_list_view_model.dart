import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/store.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:stacked/stacked.dart';

class StoreListViewModel extends BaseViewModel {
  static const _pageSize = 5;
  // late int catId ;
  // set  catId(int val){

  // }
  late int _catId;
  int get catId => _catId;
  set catId(int val) {
    _catId = val;
  }

  final PagingController<int, Store> pagingController =
      PagingController(firstPageKey: 0);
  void listen() {
    pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final commonResponse = await locator<StoreRepository>().getStories(
          pageIndex: pageKey, pageSize: _pageSize, categoryId: catId);
      Iterable list = commonResponse.data;
      List<Store> newList = [];
      list.forEach((element) {
        newList.add(Store.fromJson(element));
      });
      final newItems = newList;
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }
}
