import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/views/store/store_details/store_branchs_list/store_branchs_list_model.dart';
import 'package:exaaz/core/data/models/branch_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:url_launcher/url_launcher.dart';

class StoreBranchsListView extends StatelessWidget {
  final int storeId;
  const StoreBranchsListView({
    Key? key,
    required this.storeId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StoreBranchListViewModel>.reactive(
      builder: (context, model, _) => model.isBusy
          ? CustomLoader()
          : ListView.separated(
              shrinkWrap: true,
              // physics:
              //     NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => LocationListTile(
                    branch: model.branchesList[index],
                  ),
              separatorBuilder: (context, index) =>
                  Divider(color: Colors.white60),
              itemCount: model.branchesList.length),
      viewModelBuilder: () => StoreBranchListViewModel(),
      onModelReady: (model) {
        model.loadLocations(storeId);
      },
    );
  }
}

class LocationListTile extends StatelessWidget {
  final BranchModel branch;
  const LocationListTile({
    Key? key,
    required this.branch,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          branch.isMainBranch ? 'المركز الرئيسي' : 'الفروع',
          style: TextStyle(color: AppColors.yellowColor, fontSize: 20),
        ),
        Text(
          branch.name + ' ' + branch.cityName + ' ' + branch.areaName,
          style: TextStyle(color: Colors.white70, fontSize: 18),
        ),
        if (branch.phoneNumber != null)
          Text(
            branch.phoneNumber!,
            style: TextStyle(color: Colors.white54, fontSize: 16),
          ),
        if (branch.customerCareNumber != null)
          Text(
            'خدمة العملاء ' + branch.customerCareNumber!,
            style: TextStyle(color: Colors.white54, fontSize: 16),
          ),
        if (branch.website != null)
          Text(
            branch.website!,
            style: TextStyle(color: Colors.white54, fontSize: 16),
          ),
        if (branch.email != null)
          Text(
            branch.email!,
            style: TextStyle(color: Colors.white54, fontSize: 16),
          ),
        if (branch.latitude != null)
          InkWell(
            onTap: () {
              _launchMapsUrl(branch.latitude!, branch.longitude!);
            },
            child: Row(
              children: [
                Text(
                  'الموقع على الخريطة',
                  style: TextStyle(color: Colors.white54, fontSize: 16),
                ),
                SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.location_on,
                  color: Colors.white70,
                  size: 20,
                )
              ],
            ),
          )
      ],
    );
  }

  void _launchMapsUrl(String lat, String lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    await launch(url);

    // if (await canLaunch(url)) {
    //   await launch(url);
    // } else {
    //   throw 'Could not launch $url';
    // }
  }
}
