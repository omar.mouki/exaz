import 'dart:developer';

import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/branch_model.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:stacked/stacked.dart';

class StoreBranchListViewModel extends BaseViewModel {
  final _storeService = locator<StoreRepository>();
  List<BranchModel> branchesList = [];

  void loadLocations(int storeId) async {
    setBusy(true);
    try {
      final commonResponse =
          await _storeService.getStoreBranches(storeId: storeId);
      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());
        branchesList.clear();
        Iterable list = commonResponse.data;
        list.forEach((element) {
          branchesList.add(BranchModel.fromJson(element));
        });
        setBusy(false);
        notifyListeners();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }
}
