import 'dart:developer';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/store.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:stacked/stacked.dart';

class StoreDetailsViewModel extends BaseViewModel {
  final _storeService = locator<StoreRepository>();
  Store? _store;
  Store? get store => _store;

  void loadData(int storeId) async {
    setBusy(true);
    try {
      final commonResponse =
          await _storeService.getStoreDetails(storeId: storeId);
      // log(commonResponse.data.toString());

      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());

        _store = Store.fromJson(commonResponse.data);
        setBusy(false);

        notifyListeners();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }

  void downloadFile(String url) async {
    final extention = url.split('.').last;
    final name = url.split('.')[2].split('/').last;
    log(name);
    final storagePermission = Permission.storage;
    final status = await storagePermission.request();
    if (status.isGranted) {
      try {
        BotToast.showLoading();
        final appStoragePath = await getApplicationDocumentsDirectory();
        final file = File('${appStoragePath.path}/$name.$extention');
        final response = await Dio().get(url,
            options: Options(
                responseType: ResponseType.bytes,
                followRedirects: true,
                receiveTimeout: 0));
        BotToast.closeAllLoading();
        final raf = file.openSync(mode: FileMode.write);
        raf.writeFromSync(response.data);
        await raf.close();
        OpenFile.open(file.path);
        notifyListeners();
      } catch (e) {
        BotToast.closeAllLoading();
      }
    } else {}
  }
}
