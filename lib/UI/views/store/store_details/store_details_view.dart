import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/share_appbar.dart';
import 'package:exaaz/UI/shared/custom_widgets/star_rating.dart';
import 'package:exaaz/UI/views/store/store_details/store_branchs_list/store_branchs_list.dart';
import 'package:exaaz/UI/views/store/store_details/store_details_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/store.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class StoreDetailsView extends StatelessWidget {
  final int stoerId;
  const StoreDetailsView({Key? key, required this.stoerId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StoreDetailsViewModel>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : SingleChildScrollView(
                child: Column(
                  children: [
                    ShareAppBar(
                      backOnTab: () {
                        locator<NavigationService>().back();
                      },
                      title: model.store?.categoryName,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: Column(
                        children: [
                          Container(
                            width: double.infinity,
                            height: 200,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                image: NetworkImage(model.store!.storeImageUrl),
                                fit: BoxFit.cover,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Icon(
                                  Icons.remove_red_eye_rounded,
                                  size: 18,
                                  color: AppColors.yellowColor,
                                ),
                                SizedBox(width: 5),
                                Text(
                                    model.store?.profileVisitCount.toString() ??
                                        ''),
                                SizedBox(
                                  width: 10,
                                )
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                model.store?.name ?? '',
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              IconButton(
                                onPressed: () {},
                                icon: Icon(Icons.favorite_border),
                                color: AppColors.whiteColor,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                      text: model.store?.cityName ?? '',
                                      // text: 'store.cityName',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                          color: Colors.white60),
                                    ),
                                    TextSpan(
                                      text: "   ${model.store?.categoryName}",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.yellowColor),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  StarRating(
                                    color: AppColors.yellowColor,
                                    // rating: 2,
                                    onRatingChanged: (val) {},
                                    starCount: 5,
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(2),
                                    color: AppColors.yellowColor,
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.star,
                                          color: AppColors.whiteColor,
                                          size: 14,
                                        ),
                                        Text(
                                          model.store?.rating?.toString() ??
                                              0.0.toString(),
                                          style: const TextStyle(
                                            color: AppColors.whiteColor,
                                            fontSize: 12,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: [
                              Text(
                                'رقم العضوية ${model.store?.storeId}',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Divider(
                            color: Colors.white60,
                          ),
                          DefaultTabController(
                            length: 3,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  child: TabBar(
                                      onTap: (index) {
                                        if (index == 1 && model.store != null) {
                                          locator<NavigationService>()
                                              .navigateTo(
                                                  Routes.productsListView,
                                                  arguments:
                                                      ProductsListViewArguments(
                                                          store: model.store!));
                                        }
                                      },
                                      indicatorColor: AppColors.yellowColor,
                                      isScrollable: false,
                                      tabs: [
                                        Tab(text: 'لمحة'),
                                        Tab(text: 'منتجاتنا'),
                                        Tab(text: 'معلومات التواصل'),
                                      ]),
                                ),
                                model.store != null
                                    ? StoreDetailsTabSection(
                                        store: model.store!,
                                        downloadFile: model.downloadFile,
                                      )
                                    : Container(),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
      viewModelBuilder: () => StoreDetailsViewModel(),
      onModelReady: (model) {
        model.loadData(stoerId);
      },
    );
  }
}

class StoreDetailsTabSection extends StatelessWidget {
  final Function(String url) downloadFile;
  const StoreDetailsTabSection({
    Key? key,
    required this.store,
    required this.downloadFile,
  }) : super(key: key);

  final Store store;

  @override
  Widget build(BuildContext context) {
    return Container(
      //Add this to give height
      height: MediaQuery.of(context).size.height / 3,
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          Column(
            children: [
              Container(
                child: Text(
                  'لمحة',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Spacer(),
              ElevatedButton(
                  onPressed: () {
                    downloadFile(store.commercialRecordUrl);
                  },
                  child: Container(
                    width: 150,
                    child: Row(
                      children: [
                        Text(
                          'عرض الملف التعريفي',
                          style: TextStyle(
                              color: AppColors.blackColor, fontSize: 14),
                        ),
                        Icon(
                          LineIcons.pdfFile,
                          color: Colors.red,
                        ),
                      ],
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey[300],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                  )),
              SizedBox(
                height: 20,
              )
            ],
          ),
          Container(
            child: Text(
              'منتجاتنا',
              style: TextStyle(color: Colors.white),
            ),
          ),
          StoreBranchsListView(
            storeId: store.storeId,
          )
        ],
      ),
    );
  }
}
