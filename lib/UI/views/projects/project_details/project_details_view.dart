import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_image_swiper.dart';
import 'package:exaaz/UI/shared/custom_widgets/file_slider.dart';
import 'package:exaaz/UI/shared/custom_widgets/share_appbar.dart';
import 'package:exaaz/UI/views/projects/project_details/componants/project_details_custom_button.dart';
import 'package:exaaz/UI/views/projects/project_details/componants/project_details_description_section.dart';
import 'package:exaaz/UI/views/projects/project_details/componants/project_details_material_section.dart';
import 'package:exaaz/UI/views/projects/project_details/componants/project_type_widget.dart';
import 'package:exaaz/UI/views/projects/project_details/componants/project_details_text_row.dart';
import 'package:exaaz/UI/views/projects/project_details/project_details_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProjectDetailsView extends StatelessWidget {
  const ProjectDetailsView(
      {Key? key, required this.id, required this.serviceRequestModel})
      : super(key: key);
  final int id;
  final ServiceRequestModel serviceRequestModel;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ViewModelBuilder<ProjectDetailsViewModel>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              ShareAppBar(
                backOnTab: () {},
                title: model.serviceRequest.title,
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: [
                    CustomImageSwiper(
                      size: size,
                      itemCount: model.serviceRequest.images?.length ?? 0,
                      imageUrlList: model.serviceRequest.images ?? [],
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    ProjectDetailsTextRow(
                        size: size,
                        title: 'رقم المشروع',
                        content: model.serviceRequest.projectId.toString()),
                    ProjectDetailsTextRow(
                        size: size,
                        title: 'مالك المشروع',
                        content: model.serviceRequest.projectOwnerName),
                    ProjectDetailsTextRow(
                        size: size,
                        title: 'مسؤول المشروع',
                        content: model.serviceRequest.projectManagerName),
                    ProjectDetailsTextRow(
                        size: size,
                        title: 'رقم الهاتف',
                        content: model.serviceRequest.projectManagerPhone),
                    const SizedBox(
                      height: 10,
                    ),
                    ProjectTypeWidget(
                      //TODO
                      export: model.serviceRequest.requestType == 2 ||
                          model.serviceRequest.requestType == 1,
                      excute: model.serviceRequest.requestType == 2 ||
                          model.serviceRequest.requestType == 0,
                      enabled: false,
                    ),
                    ProjectDetailsDescriptionSection(
                      size: size,
                      content: model.serviceRequest.description,
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Visibility(
                      visible: model.serviceRequest.pdFs != null &&
                          model.serviceRequest.pdFs!.isNotEmpty,
                      child: Row(
                        children: const [
                          Text(
                            'المسستندات المرفقة',
                            style: TextStyle(color: AppColors.yellowColor),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                    ),
                    FileSliderWidget(
                      fileType: FileType.Pdf,
                      viewType: ViewType.ReadOnly,
                      filesUrls: model.serviceRequest.pdFs ?? [],
                      height: model.serviceRequest.pdFs == null ||
                              model.serviceRequest.pdFs!.length == 0
                          ? 0
                          : 100,
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Visibility(
                      visible: model.serviceRequest.images != null &&
                          model.serviceRequest.images!.isNotEmpty,
                      child: Row(
                        children: const [
                          Text(
                            'الصور المرفقة',
                            style: TextStyle(color: AppColors.yellowColor),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                    ),
                    FileSliderWidget(
                      fileType: FileType.Image,
                      viewType: ViewType.ReadOnly,
                      filesUrls: model.serviceRequest.images ?? [],
                      height: model.serviceRequest.images == null ||
                              model.serviceRequest.images!.length == 0
                          ? 0
                          : 100,
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    ProjectDetailsTextRow(
                        size: size,
                        title: 'تاريخ بدئ العمل',
                        textColor: AppColors.whiteColor,
                        content: model.serviceRequest.startDate),
                    ProjectDetailsMaterialSection(
                      content: model.serviceRequest.materialsType ?? '',
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Visibility(
                      visible: !serviceRequestModel.canEdit!,
                      child: ProjectDetailsCustomButton(
                        title: 'تواصل مع مسؤول المشروع',
                        onTap: () {},
                      ),
                    ),
                    Visibility(
                      visible: serviceRequestModel.canEdit!,
                      child: ProjectDetailsCustomButton(
                        title: 'تعديل المشروع',
                        onTap: () {
                          locator<NavigationService>().navigateTo(
                              Routes.newProjectView,
                              arguments: NewProjectViewArguments(
                                  serviceRequestModel: model.serviceRequest));
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      viewModelBuilder: () => ProjectDetailsViewModel(),
      onModelReady: (model) {
        model.loadData(id, serviceRequestModel);
      },
    );
  }
}
