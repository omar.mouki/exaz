import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ProjectDetailsTextRow extends StatelessWidget {
  const ProjectDetailsTextRow({
    Key? key,
    required this.size,
    required this.title,
    required this.content,
    this.textColor,
  }) : super(key: key);

  final Size size;
  final title;
  final content;
  final Color? textColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            title,
            style: const TextStyle(color: AppColors.yellowColor),
          ),
        ),
        // SizedBox(
        //   width: size.width * 0.2,
        // ),
        Expanded(
          flex: 2,
          child: Text(
            content,
            style: TextStyle(color: textColor ?? AppColors.greyColor),
          ),
        ),
      ],
    );
  }
}
