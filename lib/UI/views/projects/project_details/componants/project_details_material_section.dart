import 'package:auto_size_text/auto_size_text.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ProjectDetailsMaterialSection extends StatelessWidget {
  ProjectDetailsMaterialSection({
    Key? key,
    required this.content,
  }) : super(key: key);

  final String content;
  final ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: AutoSizeText(
                'نوع المواد المعتمدة ( للمشروع أو العينة)',
                maxLines: 1,
                style: TextStyle(
                    fontSize: 18,
                    color: AppColors.yellowColor,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        Container(
          constraints: BoxConstraints(minHeight: 20, maxHeight: 100),
          child: Scrollbar(
            // isAlwaysShown: true,
            controller: _scrollController,
            child: SingleChildScrollView(
              controller: _scrollController,
              scrollDirection: Axis.vertical, //.horizontal
              child: Row(
                children: [
                  Flexible(
                    child: Text(
                      content.isNotEmpty ? content : 'لايوجد نص',
                      style: const TextStyle(
                          fontSize: 16, color: AppColors.whiteColor),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
