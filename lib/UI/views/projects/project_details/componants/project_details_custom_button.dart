import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ProjectDetailsCustomButton extends StatelessWidget {
  const ProjectDetailsCustomButton({
    Key? key,
    required this.title,
    required this.onTap,
  }) : super(key: key);
  final String title;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 13),
        height: 50,
        width: 200,
        color: AppColors.yellowColor,
        child: Center(
          child: Text(
            title,
            style: const TextStyle(
                color: AppColors.whiteColor, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
