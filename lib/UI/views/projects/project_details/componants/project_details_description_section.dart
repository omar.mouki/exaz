import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ProjectDetailsDescriptionSection extends StatelessWidget {
  ProjectDetailsDescriptionSection({
    Key? key,
    required this.size,
    required this.content,
  }) : super(key: key);

  final Size size;
  final String? content;

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              'وصف المشروع',
              style: const TextStyle(color: AppColors.yellowColor),
              textAlign: TextAlign.start,
            ),
          ],
        ),
        Container(
          constraints: BoxConstraints(minHeight: 20, maxHeight: 100),
          child: Scrollbar(
            // isAlwaysShown: true,
            controller: _scrollController,
            child: SingleChildScrollView(
              controller: _scrollController,
              scrollDirection: Axis.vertical, //.horizontal
              child: Row(
                children: [
                  Flexible(
                    child: Text(
                      content ?? 'لا يوجد نص',
                      style: const TextStyle(
                          fontSize: 16, color: AppColors.whiteColor),
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
