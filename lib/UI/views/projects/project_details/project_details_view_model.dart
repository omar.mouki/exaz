import 'dart:developer';

import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:exaaz/core/data/repository/service_request_repository.dart';
import 'package:exaaz/core/utils/loader_util.dart';
import 'package:stacked/stacked.dart';

class ProjectDetailsViewModel extends BaseViewModel {
  final _serviceRequestRepository = locator<ServiceRequestRepository>();
  late ServiceRequestModel serviceRequest;
  void loadData(int id, ServiceRequestModel serviceRequestModel) async {
    serviceRequest = serviceRequestModel;
    setBusy(true);
    try {
      showLoader();
      final commonResponse =
          await _serviceRequestRepository.getServiceRequestDetails(id: id);
      // log(commonResponse.data.toString());

      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());

        serviceRequest = ServiceRequestModel.fromJson(commonResponse.data);
        setBusy(false);
        hideLoader();
        notifyListeners();
      } else {
        setBusy(false);
        hideLoader();

        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      hideLoader();

      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }
}
