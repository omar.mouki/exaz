import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/default_app_bar.dart';
import 'package:exaaz/UI/views/projects/componants/project_list_view_item.dart';
import 'package:exaaz/UI/views/projects/service_provider_project_list/service_provider_project_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class ServiceProviderProjectListView extends StatelessWidget {
  const ServiceProviderProjectListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ViewModelBuilder<ServiceProviderProjectListViewModel>.reactive(
        builder: (context, model, _) => Scaffold(
            backgroundColor: AppColors.blackColor,
            body: model.isBusy
                ? Center(
                    child: CustomLoader(),
                  )
                : Column(
                    children: [
                      DefaultAppBar(
                        title: 'قائمة المشاريع',
                        showBack: true,
                        action: Container(),
                      ),
                      SizedBox(height: 12),
                      CatigoryHorizantalSlider(
                        size: size,
                        catigoriesList: model.cateigoriesList,
                        catigorySelected: model.catigorySelected,
                        selectedCatigory: model.selectedIndex,
                      ),
                      Expanded(
                          child: ListView.builder(
                        // itemBuilder: (context, index) => ProjectListViewItem(),
                        itemBuilder: (context, index) => Container(),
                        itemCount: 4,
                      )),
                    ],
                  )),
        viewModelBuilder: () => ServiceProviderProjectListViewModel());
  }
}

class CatigoryHorizantalSlider extends StatelessWidget {
  const CatigoryHorizantalSlider({
    Key? key,
    required this.size,
    required this.catigorySelected,
    required this.selectedCatigory,
    required this.catigoriesList,
  }) : super(key: key);

  final Size size;

  final Function(int) catigorySelected;
  final int selectedCatigory;
  final List<String> catigoriesList;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      width: size.width,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: catigoriesList.length,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        itemBuilder: (context, index) => InkWell(
          onTap: () {
            catigorySelected(index);
          },
          child: Container(
            margin: const EdgeInsetsDirectional.only(end: 8),
            padding: const EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(
              border: Border.all(
                color: selectedCatigory == index
                    ? AppColors.yellowColor
                    : AppColors.whiteColor,
              ),
              borderRadius: BorderRadius.circular(size.width / 40),
            ),
            child: Text(
              catigoriesList[index],
              style: TextStyle(
                  fontSize: size.width / 22, color: AppColors.whiteColor),
            ),
          ),
        ),
      ),
    );
  }
}
