import 'dart:developer';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/service_request/service_request__category_model.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:exaaz/core/data/repository/service_request_repository.dart';
import 'package:exaaz/core/enums/service_request_enum.dart';
import 'package:exaaz/core/utils/loader_util.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class NewProjectViewModel extends BaseViewModel {
  String locale = 'ar';
  final _formKey = GlobalKey<FormState>();
  final _serviceRequestRepository = locator<ServiceRequestRepository>();
  GlobalKey<FormState> get formKey => _formKey;

  int _selectedCatigory = -1;
  int get selectedCatigory => _selectedCatigory;
  bool _export = false;
  bool get export => _export;
  bool _excute = false;
  bool get excute => _excute;

  List<String> _pdfList = [];
  List<String> get pdfList => _pdfList;

  List<String> _imageList = [];
  List<String> get imageList => _imageList;

  List<ServiceRequestCategoryModel> _catigories = [];
  List<ServiceRequestCategoryModel> get catigories => _catigories;

  String? projectOwnerName;
  String? projectManagerName;
  String? phoneNumber;
  String pickedDate = '';
  String? description;
  String? material;
  String? location;
  String? subject;
  bool isEdit = false;
  ServiceRequestType get getServiceRequestType {
    if (_excute && _export) return ServiceRequestType.Both;
    if (_excute && !_export) return ServiceRequestType.Executing;
    if (!_excute && _export) return ServiceRequestType.Supply;
    return ServiceRequestType.None;
  }

  String? projectOwnerValidator(String? val) {
    if (val != null && val.isNotEmpty) return null;
    return tr('pease_insert_project_owner_name');
  }

  String? projectManagerValidator(String? val) {
    if (val != null && val.isNotEmpty) return null;
    return tr('pease_insert_project_manager_name');
  }

  String? phoneValidator(String? val) => val != null && val.length > 9
      ? null
      : tr('ReportProblemController_invalidePhone');

  String? locationValidator(String? val) {
    if (val != null && val.isNotEmpty) return null;
    return tr('pease_insert_project_location');
  }

  String? subjectValidator(String? val) {
    if (val != null && val.isNotEmpty) return null;
    return tr('pease_insert_project_subject');
  }

  String? dateValidator(String? val) {
    if (val != null && val.isNotEmpty) {
      try {
        final date = DateFormat("dd/MM/yyyy").parse(pickedDate);
        final currentDate = DateTime.now();
        if (date.year < currentDate.year) return tr('cant_choose_previos_date');
        if (date.year == currentDate.year && date.month < currentDate.month) {
          return tr('cant_choose_previos_date');
        }
        if (date.year == currentDate.year &&
            date.month == currentDate.month &&
            date.day < currentDate.day) return tr('cant_choose_previos_date');

        return null;
      } catch (e) {
        return tr('please_choose_correct_date');
      }
    }
    return tr('please_choose_date');
  }

  String? descriptionValidator(String? val) {
    if (val != null && val.isNotEmpty) return null;
    return tr('please_insert_project_description');
  }

  // String? materialValidator(String? val) {
  //   if (val != null && val.isNotEmpty) return null;
  //   return tr('please_insert_project_material');
  // }

  projectOwnerOnSave(String? val) {
    if (val != null && val.isNotEmpty) projectOwnerName = val;
  }

  projectManagerOnSave(String? val) {
    if (val != null && val.isNotEmpty) projectManagerName = val;
  }

  phoneOnSave(String? val) {
    if (val != null && val.isNotEmpty) phoneNumber = val;
  }

  locationOnSave(String? val) {
    if (val != null && val.isNotEmpty) location = val;
  }

  subjectOnSave(String? val) {
    if (val != null && val.isNotEmpty) subject = val;
  }

  dateOnSave(String? val) {
    if (val != null && val.isNotEmpty) {
      pickedDate = val;
    }
  }

  descriptionOnSave(String? val) {
    if (val != null && val.isNotEmpty) description = val;
  }

  materialOnSave(String? val) {
    if (val != null && val.isNotEmpty) material = val;
  }

  dynamic onChange(DateTime date) {
    // pickedDate = DateTime(date.year, date.month, date.day, DateTime.now().hour,
    //         DateTime.now().minute, DateTime.now().second)
    //     .toIso8601String();
    // pickedDate = date.toString();
    pickedDate = DateFormat("dd/MM/yyyy").format(date);
    // log(DateTime.now().toIso8601String());
    notifyListeners();
  }

  void onWorkTypeChanged(int? newVal) {
    _selectedCatigory = newVal != null ? newVal : _selectedCatigory;
    notifyListeners();
  }

  onExcuteChanged(bool? newVal) {
    if (newVal != null) _excute = newVal;
    notifyListeners();
  }

  onExportChanged(bool? newVal) {
    if (newVal != null) _export = newVal;
    notifyListeners();
  }

  removePdfItem(List<String> list) {
    _pdfList = list;
    notifyListeners();
  }

  removeImageItem(List<String> list) {
    _imageList = list;
    notifyListeners();
  }

  void getLocale(BuildContext context) {
    final Locale appLocale = Localizations.localeOf(context);
    locale = appLocale.languageCode;
  }

  void addImage(int type) async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.camera,
      Permission.storage,
    ].request();
    if (statuses.containsValue(PermissionStatus.denied)) {
      log('no permission');
    } else {
      final result = await ImagePicker().pickImage(
        imageQuality: 70,
        maxWidth: 1440,
        source: type == 1 ? ImageSource.camera : ImageSource.gallery,
      );
      if (result != null) {
        log(result.path);
        imageList.add(result.path);
        notifyListeners();

        print(result.path);
      } else {
        // User canceled the picker
      }
    }
  }

  void addPdfFile() async {
    final statuse = await Permission.storage.request();
    if (statuse.isDenied) {
      log('no permission');
    } else {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf'],
      );

      if (result != null) {
        File file = File(result.files.single.path!);
        pdfList.add(result.files.single.path!);
        notifyListeners();
      } else {
        // User canceled the picker
      }
    }
  }

  void loadCategories() async {
    // setBusy(true);
    try {
      final commonResponse =
          await _serviceRequestRepository.getServiceRequestCategories();

      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());

        if (commonResponse.data != null) {
          final _apiCat = <ServiceRequestCategoryModel>[];

          commonResponse.data.forEach((v) {
            _apiCat.add(ServiceRequestCategoryModel.fromJson(v));
          });
          _catigories = _apiCat;

          if (_catigories.isNotEmpty) _selectedCatigory = _catigories[0].id!;
        }

        // setBusy(false);

        notifyListeners();
      } else {
        // setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      // setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }

  void submit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      setBusy(true);
      showLoader();
      try {
        final serviceRequestModel = ServiceRequestModel(
            title: subject!,
            description: description!,
            startDate: pickedDate,
            projectOwnerName: projectOwnerName!,
            //TODO ask obada
            projectOwnerPhone: '',
            projectManagerName: projectManagerName!,
            projectManagerPhone: phoneNumber!,
            projectLocation: location!,
            pdFs: pdfList,
            images: imageList,
            materialsType: material,
            requestState: 0,
            requestType: getServiceRequestType.index,
            requestCategoryId: _selectedCatigory);
        final commonResponse = await _serviceRequestRepository.createNewRequest(
            serviceRequestModel: serviceRequestModel);

        if (commonResponse.getStatus) {
          log(commonResponse.data.toString());

          final _serviceRequestModel =
              ServiceRequestModel.fromJson(commonResponse.data);
          setBusy(false);
          hideLoader();
          locator<NavigationService>().back(result: _serviceRequestModel);

          notifyListeners();
        } else {
          setBusy(false);
          hideLoader();

          CustomToasts.showMessage(
              message: commonResponse.getError.values.elementAt(0),
              messageType: MessageType.errorMessage);
        }
      } catch (e) {
        setBusy(false);
        hideLoader();

        CustomToasts.showMessage(
            message: e.toString(), messageType: MessageType.alertMessage);
        log(e.toString());
      }
    }
  }

  setEdit(ServiceRequestModel serviceRequestModel) {
    isEdit = true;

    projectOwnerName = serviceRequestModel.projectOwnerName;
    projectManagerName = serviceRequestModel.projectManagerName;
    phoneNumber = serviceRequestModel.projectManagerName;
    pickedDate = serviceRequestModel.startDate.split(" ")[0];
    description = serviceRequestModel.description;
    material = serviceRequestModel.materialsType;
    location = serviceRequestModel.projectLocation;
    subject = serviceRequestModel.title;
    _imageList = serviceRequestModel.images ?? [];
    _pdfList = serviceRequestModel.pdFs ?? [];
  }
}
