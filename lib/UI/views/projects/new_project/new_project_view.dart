import 'package:auto_size_text/auto_size_text.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/default_app_bar.dart';
import 'package:exaaz/UI/shared/custom_widgets/file_slider.dart';
import 'package:exaaz/UI/views/projects/new_project/componants/new_project_date_picker.dart';
import 'package:exaaz/UI/views/projects/new_project/componants/new_project_work_type.dart';
import 'package:exaaz/UI/views/projects/new_project/componants/new_project_submit_button.dart';
import 'package:exaaz/UI/views/projects/new_project/componants/new_project_text_feild.dart';
import 'package:exaaz/UI/views/projects/project_details/componants/project_type_widget.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/service_request/service_request__category_model.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import 'new_project_view_model.dart';

class NewProjectView extends StatelessWidget {
  const NewProjectView({Key? key, this.serviceRequestModel}) : super(key: key);
  final ServiceRequestModel? serviceRequestModel;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return ViewModelBuilder<NewProjectViewModel>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              DefaultAppBar(
                backOnTab: () => locator<NavigationService>().back(),
                title: 'عرض مشروع',
                showBack: true,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Form(
                  key: model.formKey,
                  child: Column(
                    children: [
                      NewProjectTextFeild(
                          initialValue: model.projectOwnerName,
                          icon: Icons.person,
                          hintText: 'اسم مالك المشروع',
                          onSaved: model.projectOwnerOnSave,
                          validator: model.projectOwnerValidator),
                      const SizedBox(height: 9),
                      NewProjectTextFeild(
                        initialValue: model.projectManagerName,
                        icon: Icons.people,
                        hintText: 'مسؤول المشروع',
                        onSaved: model.projectManagerOnSave,
                        validator: model.projectManagerValidator,
                      ),
                      const SizedBox(height: 9),
                      NewProjectTextFeild(
                        initialValue: model.phoneNumber,
                        maxLength: 10,
                        icon: Icons.phone,
                        hintText: 'رقم الهاتف',
                        validator: model.phoneValidator,
                        onSaved: model.phoneOnSave,
                        keyboardType: TextInputType.phone,
                      ),
                      const SizedBox(height: 9),
                      NewProjectTextFeild(
                        initialValue: model.location,
                        icon: Icons.location_on,
                        hintText: 'موقع المشروع',
                        onSaved: model.locationOnSave,
                        validator: model.locationValidator,
                      ),
                      const SizedBox(height: 9),
                      NewProjectTextFeild(
                        initialValue: model.subject,
                        icon: LineIcons.fileInvoice,
                        hintText: 'موضوع المشروع',
                        onSaved: model.subjectOnSave,
                        validator: model.subjectValidator,
                      ),
                      const SizedBox(height: 9),
                      NewProjectDatePicker(
                        pickedDate: model.pickedDate,
                        onChanged: model.onChange,
                        validator: model.dateValidator,
                        onSaved: model.dateOnSave,
                        locale: model.locale,
                      ),
                      const SizedBox(height: 9),
                      NewProjectTextFeild(
                        initialValue: model.description,
                        icon: Icons.subject,
                        hintText: 'وصف العمل',
                        maxLins: 4,
                        validator: model.descriptionValidator,
                        onSaved: model.descriptionOnSave,
                      ),
                      const SizedBox(height: 9),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 7),
                        child: Row(
                          children: const [
                            Text(
                              'صور موقع المشروع',
                              style: TextStyle(
                                  fontSize: 14, color: AppColors.greyColor),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 9),
                      FileSliderWidget(
                        key: UniqueKey(),
                        // addNew: model.addImage,
                        addNewImage: model.addImage,
                        fileType: FileType.Image,
                        viewType: ViewType.Edit,
                        filesUrls: model.imageList,
                        height: 100,
                        onCancelClicked: model.removeImageItem,
                      ),
                      const SizedBox(height: 9),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 7),
                        child: Row(
                          children: const [
                            Text(
                              'ارفاق المستندات والمخططات وجدول الكميات',
                              style: TextStyle(
                                  fontSize: 14, color: AppColors.greyColor),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 9),
                      FileSliderWidget(
                        fileType: FileType.Pdf,
                        viewType: ViewType.Edit,
                        filesUrls: model.pdfList,
                        onCancelClicked: model.removePdfItem,
                        addNewPdf: () {
                          model.addPdfFile();
                        },
                        height: 100,
                      ),
                      const SizedBox(height: 9),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: ProjectTypeWidget(
                          export: model.export,
                          excute: model.excute,
                          textColor: AppColors.whiteColor,
                          enabled: true,
                          onExcuteChanged: model.onExcuteChanged,
                          onExportChanged: model.onExportChanged,
                        ),
                      ),
                      const SizedBox(height: 9),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 7),
                        child: Row(
                          children: const [
                            Expanded(
                              child: AutoSizeText(
                                'نوع المواد المعتمدة ( للمشروع أو العينة)',
                                maxLines: 1,
                                style: TextStyle(
                                    fontSize: 18,
                                    color: AppColors.whiteColor,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: NewProjectTextFeild(
                          hintText: '',
                          maxLins: 4,
                          textColor: AppColors.whiteColor,
                          fillcolor: const Color(0xFF313131),
                          borderColor: const Color(0xFF707070),
                          // validator: model.materialValidator,
                          onSaved: model.materialOnSave,
                        ),
                      ),
                      const SizedBox(height: 9),
                      NewProjectWorkType(
                        projectWorkTypeList: model.catigories,
                        selectedValue: model.selectedCatigory,
                        onChanged: model.onWorkTypeChanged,
                      ),
                      const SizedBox(height: 9),
                      NewProjectSubmitButton(
                        size: size,
                        onTap: model.submit,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      viewModelBuilder: () => NewProjectViewModel(),
      onModelReady: (model) {
        model.loadCategories();
        model.getLocale(context);
        if (serviceRequestModel != null) {
          model.setEdit(serviceRequestModel!);
        }
      },
    );
  }
}
