import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:line_icons/line_icons.dart';

class NewProjectDatePicker extends StatefulWidget {
  NewProjectDatePicker({
    Key? key,
    this.onSaved,
    this.validator,
    required this.onChanged,
    required this.pickedDate,
    required this.locale,
  }) : super(key: key);
  String pickedDate;
  String? Function(String?)? validator;
  final String locale;
  void Function(String?)? onSaved;
  dynamic Function(DateTime)? onChanged;

  @override
  _NewProjectDatePickerState createState() => _NewProjectDatePickerState();
}

class _NewProjectDatePickerState extends State<NewProjectDatePicker> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        DatePicker.showDatePicker(
          context,
          showTitleActions: false,
          // minTime: DateTime(2021),
          // maxTime: DateTime(2019, 6, 7),
          onChanged: (date) {
            // widget.onChanged?.call(date);
            widget.onChanged!(date);
            setState(() {
              widget.pickedDate = DateFormat("dd/MM/yyyy").format(date);

              // widget.pickedDate = date.toIso8601String();
            });
          },
          onConfirm: (date) {
            print('confirm $date');
          },

          currentTime: widget.pickedDate.isNotEmpty
              ? getCurrentString(widget.pickedDate)
              : DateTime.now(),
          locale: widget.locale == 'ar' ? LocaleType.ar : LocaleType.en,

          theme: const DatePickerTheme(
            backgroundColor: AppColors.blackColor,
            headerColor: AppColors.yellowColor,
            itemStyle: TextStyle(color: AppColors.whiteColor, fontSize: 18),
            cancelStyle: TextStyle(color: Colors.black45, fontSize: 18),
            doneStyle: TextStyle(color: Colors.black45, fontSize: 18),
          ),
        );
      },
      child: TextFormField(
        validator: widget.validator,
        onSaved: (val) {
          if (widget.onSaved != null) widget.onSaved!(widget.pickedDate);
        },
        controller: TextEditingController(
            text: widget.pickedDate.isNotEmpty ? widget.pickedDate : ''),
        enabled: false,
        onTap: () {},
        autofocus: false,
        style: const TextStyle(fontSize: 14.0, color: AppColors.blackColor),
        decoration: const InputDecoration(
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.yellowColor, width: 2.0),
            ),
            filled: true,
            fillColor: Colors.white,
            hintText: 'تاريخ بدء المشروع',
            hintStyle: TextStyle(fontSize: 14, color: Colors.black),
            prefixIcon: Icon(
              LineIcons.alternateCalendarAlt,
              color: Colors.black,
              size: 24,
            ),
            contentPadding: EdgeInsets.all(8.0),
            errorStyle: TextStyle(color: Colors.red)),
      ),
    );
  }

  DateTime getCurrentString(String stringDate) {
    stringDate = stringDate.replaceAll('م', 'PM');
    stringDate = stringDate.replaceAll('ص', 'AM');
    DateTime tempDate = DateFormat("dd/MM/yyyy").parse(stringDate);
    // final x = DateFormat("dd/MM/yyyy hh:mm").format(DateTime.now());
    // log(x);
    log(tempDate.month.toString());
    return tempDate;
    // return DateTime.parse(stringDate);
  }
}
