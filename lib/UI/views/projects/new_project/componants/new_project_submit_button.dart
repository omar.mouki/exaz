import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class NewProjectSubmitButton extends StatelessWidget {
  const NewProjectSubmitButton({
    Key? key,
    required this.size,
    required this.onTap,
  }) : super(key: key);

  final Size size;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.width / 5),
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
      decoration: BoxDecoration(
          color: AppColors.yellowColor,
          borderRadius: BorderRadius.circular(25)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            Icons.add_circle_outline_outlined,
            color: AppColors.whiteColor,
            size: 20,
          ),
          const SizedBox(
            width: 8,
          ),
          InkWell(
            onTap: onTap,
            child: const Text(
              'رفع المشروع',
              style: TextStyle(color: AppColors.whiteColor, fontSize: 14),
            ),
          ),
        ],
      ),
    );
  }
}
