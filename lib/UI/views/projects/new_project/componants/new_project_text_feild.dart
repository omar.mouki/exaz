import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NewProjectTextFeild extends StatelessWidget {
  NewProjectTextFeild({
    Key? key,
    required this.hintText,
    this.icon,
    this.fillcolor = Colors.white,
    this.borderColor = AppColors.yellowColor,
    this.maxLins,
    this.validator,
    this.keyboardType,
    this.onSaved,
    this.maxLength,
    this.textColor,
    this.initialValue,
  }) : super(key: key);
  final String hintText;
  final IconData? icon;
  final int? maxLins;
  int? maxLength;
  final Color? fillcolor;
  final Color borderColor;
  final String? Function(String?)? validator;
  void Function(String?)? onSaved;
  final TextInputType? keyboardType;
  final Color? textColor;
  final String? initialValue;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialValue,
      maxLength: maxLength,
      enabled: true,
      // autofocus: false,
      keyboardType: keyboardType,

      validator: validator,
      onSaved: onSaved,
      maxLines: maxLins ?? 1,
      style:
          TextStyle(fontSize: 14.0, color: textColor ?? AppColors.blackColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: fillcolor,
        counterText: "",
        hintText: hintText,
        hintStyle: const TextStyle(fontSize: 14, color: Colors.black),
        prefixIcon: icon != null
            ? Container(
                transform: Matrix4.translationValues(
                    0.0, maxLins != null ? -40.0 : 0, 0.0),
                child: Icon(
                  icon,
                  color: Colors.black,
                  size: 24,
                ),
              )
            : null,
        contentPadding: const EdgeInsets.all(8.0),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.yellowColor, width: 2),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: borderColor,
            width: 2.0,
          ),
        ),
      ),
    );
  }
}
