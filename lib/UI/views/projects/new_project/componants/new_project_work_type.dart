import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/core/data/models/service_request/service_request__category_model.dart';
import 'package:flutter/material.dart';

class NewProjectWorkType extends StatelessWidget {
  const NewProjectWorkType({
    Key? key,
    required this.projectWorkTypeList,
    required this.selectedValue,
    this.onChanged,
  }) : super(key: key);

  final List<ServiceRequestCategoryModel> projectWorkTypeList;
  final void Function(int?)? onChanged;
  final int selectedValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 9.0),
      child: Row(
        children: [
          Text(
            'نوع العمل',
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
                color: AppColors.blackColor,
                borderRadius: BorderRadius.circular(10)),

            // dropdown below..
            child: projectWorkTypeList.isEmpty
                ? CircularProgressIndicator(
                    color: AppColors.yellowColor,
                  )
                : Directionality(
                    textDirection: TextDirection.ltr,
                    child: DropdownButton<int>(
                      dropdownColor: Color(0xFF707070),
                      value: selectedValue,
                      onChanged: onChanged,
                      items: projectWorkTypeList
                          .map<DropdownMenuItem<int>>(
                              (value) => DropdownMenuItem<int>(
                                    value: value.id,
                                    child: Text(
                                      value.name ?? '',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ))
                          .toList(),

                      // add extra sugar..
                      icon: Icon(
                        Icons.arrow_drop_down,
                        color: AppColors.yellowColor,
                      ),
                      iconSize: 42,
                      underline: SizedBox(),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
