import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/default_app_bar.dart';
import 'package:exaaz/UI/shared/custom_widgets/list_error_widget.dart';
import 'package:exaaz/UI/shared/custom_widgets/list_no_data_widget.dart';
import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';
import 'package:exaaz/UI/views/projects/componants/project_list_view_item.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';

import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'package:stacked/stacked.dart';
import 'my_projects_list_view_model.dart';

class MyProjectsListView extends StatelessWidget {
  const MyProjectsListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ViewModelBuilder<MyProjectsListViewModel>.reactive(
      builder: (context, model, _) => Scaffold(
        backgroundColor: AppColors.blackColor,
        body: Column(
          children: [
            DefaultAppBar(
              action: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SvgIconButton(
                    color: AppColors.yellowColor,
                    svgPath: 'assets/icons/add.svg',
                    width: size.width / 18,
                    onPressed: model.navigateToAddNewProject,
                  ),
                ],
              ),
              backOnTab: () {},
              title: 'قائمة مشاريعي',
              showBack: false,
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: PagedListView<int, ServiceRequestModel>(
                pagingController: model.pagingController,
                reverse: false,
                builderDelegate: PagedChildBuilderDelegate<ServiceRequestModel>(
                  noItemsFoundIndicatorBuilder: (context) {
                    return const ListNoDataWidget();
                  },
                  firstPageErrorIndicatorBuilder: (context) => ListErrorWidget(
                    pagingController: model.pagingController,
                  ),
                  itemBuilder: (context, item, index) => ProjectListViewItem(
                    serviceRequestModel: item,
                    deleteItem: model.removeServiceRequest,
                  ),
                ),
              ),
              // child: ListView.builder(
              //   itemBuilder: (context, index) => ProjectListViewItem(),
              //   itemCount: 12,
              // ),
            ),
          ],
        ),
      ),
      viewModelBuilder: () => MyProjectsListViewModel(),
      onModelReady: (model) {
        model.listen();
      },
    );
  }
}
