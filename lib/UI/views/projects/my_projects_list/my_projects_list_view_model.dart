import 'dart:developer';

import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:exaaz/core/data/repository/service_request_repository.dart';
import 'package:exaaz/core/utils/loader_util.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class MyProjectsListViewModel extends BaseViewModel {
  static const _pageSize = 10;

  final _serviceRequestRepository = locator<ServiceRequestRepository>();

  final PagingController<int, ServiceRequestModel> pagingController =
      PagingController(firstPageKey: 0);
  void listen() {
    pagingController.addPageRequestListener((pageKey) {
      // api
      _fetchPage(pageKey);
    });
  }

  void navigateToAddNewProject() async {
    final result =
        await locator<NavigationService>().navigateTo(Routes.newProjectView);
    if (result != null) {
      pagingController.appendLastPage([result]);
      notifyListeners();
    }
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final commonResponse =
          await _serviceRequestRepository.getMyServiceRequests(
        pageIndex: pageKey,
        pageSize: _pageSize,
      );

      Iterable list = commonResponse.data;
      List<ServiceRequestModel> newList = [];

      list.forEach((element) {
        newList.add(ServiceRequestModel.fromJson(element));
      });
      // newList = [];
      final newItems = newList;
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  void removeServiceRequest(int id) async {
    setBusy(true);
    try {
      showLoader();
      final commonResponse =
          await _serviceRequestRepository.removeServiceRequest(id: id);
      // log(commonResponse.data.toString());

      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());
        setBusy(false);
        pagingController.itemList
            ?.removeWhere((element) => element.projectId == id);
        hideLoader();
        notifyListeners();
      } else {
        setBusy(false);
        hideLoader();

        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
        pagingController.refresh();
      }
    } catch (e) {
      setBusy(false);
      hideLoader();

      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }
}
