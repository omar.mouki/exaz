import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/service_request/service_request_model.dart';
import 'package:flutter/material.dart';
import 'package:ndialog/ndialog.dart';
import 'package:stacked_services/stacked_services.dart';

class ProjectListViewItem extends StatelessWidget {
  const ProjectListViewItem({
    Key? key,
    required this.serviceRequestModel,
    this.deleteItem,
  }) : super(key: key);

  final ServiceRequestModel serviceRequestModel;
  final Function(int)? deleteItem;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 100,
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
            colors: [
              Color(0xFF343434),
              Color(0xFF212020),
            ],
            begin: const FractionalOffset(1.0, 0.0),
            end: const FractionalOffset(0.0, 0.0),
            stops: const [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: InkWell(
        onTap: () {
          locator<NavigationService>().navigateTo(Routes.projectDetailsView,
              arguments: ProjectDetailsViewArguments(
                  id: serviceRequestModel.projectId!,
                  serviceRequestModel: serviceRequestModel));
          // log(serviceRequestModel.toJson());
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  serviceRequestModel.title,
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'NO:${serviceRequestModel.projectId}',
                  style: const TextStyle(color: Colors.white, fontSize: 16),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  serviceRequestModel.requestCategoryName.toString(),
                  style: const TextStyle(
                      color: AppColors.yellowColor, fontSize: 14),
                ),
                IconButton(
                  onPressed: () {
                    NDialog(
                      dialogStyle: DialogStyle(titleDivider: true),
                      title: Text('هل تريد بالتاكيد حذف المشروع؟'),
                      // content:
                      //     Text('camera or galary'),
                      actions: <Widget>[
                        FlatButton(
                            child: Text('موافق'),
                            onPressed: () {
                              locator<NavigationService>().back();
                              if (deleteItem != null) {
                                deleteItem!(serviceRequestModel.projectId!);
                              }
                            }),
                        FlatButton(
                            child: Text('الغاء الامر'),
                            onPressed: () {
                              locator<NavigationService>().back();
                            }),
                      ],
                    ).show(context);
                  },
                  icon: Icon(
                    Icons.delete,
                    size: 20,
                  ),
                  color: AppColors.yellowColor,
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  serviceRequestModel.startDate,
                  style: const TextStyle(
                      color: AppColors.yellowColor, fontSize: 14),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.remove_red_eye_sharp,
                      color: AppColors.whiteColor,
                      size: 14,
                    ),
                    SizedBox(width: 5),
                    Text(
                      '12',
                      style: const TextStyle(
                          color: AppColors.whiteColor, fontSize: 12),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      width: 16,
                      height: 16,
                      child: Checkbox(
                        checkColor: Colors.white,
                        fillColor: MaterialStateColor.resolveWith(
                          (states) {
                            if (states.contains(MaterialState.selected)) {
                              return AppColors
                                  .yellowColor; // the color when checkbox is selected;
                            }
                            return AppColors
                                .yellowColor; //the color when checkbox is unselected;
                          },
                        ),
                        activeColor: Colors.purple,
                        value: serviceRequestModel.requestType == 3 ||
                            serviceRequestModel.requestType == 2,
                        shape: CircleBorder(),
                        onChanged: (value) {},
                      ),
                    ),
                    SizedBox(width: 5),
                    Text(
                      'توريد',
                      style: const TextStyle(color: AppColors.whiteColor),
                    ),
                    SizedBox(width: 8),
                    SizedBox(
                      width: 16,
                      height: 16,
                      child: Checkbox(
                        checkColor: Colors.white,
                        fillColor: MaterialStateColor.resolveWith(
                          (states) {
                            if (states.contains(MaterialState.selected)) {
                              return AppColors
                                  .yellowColor; // the color when checkbox is selected;
                            }
                            return AppColors
                                .yellowColor; //the color when checkbox is unselected;
                          },
                        ),
                        activeColor: Colors.purple,
                        value: serviceRequestModel.requestType == 3 ||
                            serviceRequestModel.requestType == 1,
                        shape: CircleBorder(),
                        onChanged: (value) {},
                      ),
                    ),
                    SizedBox(width: 5),
                    Text(
                      'تنفيذ',
                      style: const TextStyle(color: AppColors.whiteColor),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
