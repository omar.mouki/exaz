import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../side_menu/side_menu_view.dart';
import 'home_view_model.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: AppColors.blackColor,
        extendBody: false,
        floatingActionButton: Container(
          width: 90.0,

          height: 90.0,
          decoration: const BoxDecoration(
            color: AppColors.yellowColor,
            shape: BoxShape.circle,
          ),
          child: const Center(
            child: Text(
              'logo',
              style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
          // child: FittedBox(
          //   child: Image.asset(
          //     'assets/new_order_icon.png',
          //     fit: BoxFit.fill,
          //   ),
          // ),
        ),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniCenterDocked,
        bottomNavigationBar: BottomAppBar(
          child: Material(
            color: Colors.black54,
            borderOnForeground: true,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                    child: SizedBox(
                  height: 80,
                  child: InkWell(
                    onTap: () {},
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.grid_view_rounded,
                          color: AppColors.yellowColor,
                        ),
                        Text(
                          'الرئيسي',
                          style: const TextStyle(color: AppColors.whiteColor),
                        )
                      ],
                    ),
                  ),
                )),
                Expanded(
                    child: SizedBox(
                  height: 80,
                  child: InkWell(
                    onTap: model.navigateToProjects,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          LineIcons.newspaper,
                          color: AppColors.yellowColor,
                        ),
                        Text(
                          'المشاريع',
                          style: const TextStyle(color: AppColors.whiteColor),
                        )
                      ],
                    ),
                  ),
                )),
                Expanded(
                  child: SizedBox(
                    height: 80,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        SizedBox(height: 24),
                        Text(''),
                      ],
                    ),
                  ),
                ),
                Expanded(
                    child: SizedBox(
                  height: 80,
                  child: InkWell(
                    onTap: () {},
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.search,
                          color: AppColors.yellowColor,
                        ),
                        Text(
                          'بحث',
                          style: const TextStyle(color: AppColors.whiteColor),
                        )
                      ],
                    ),
                  ),
                )),
                Expanded(
                    child: SizedBox(
                  height: 80,
                  child: InkWell(
                    onTap: () {
                      locator<NavigationService>()
                          .navigateTo(Routes.sideMenuView);
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.person,
                          color: AppColors.yellowColor,
                        ),
                        Text(
                          'حسابي',
                          style: const TextStyle(color: AppColors.whiteColor),
                        )
                      ],
                    ),
                  ),
                )),
              ],
            ),
          ),
          // notchedShape: CircularNotchedRectangle(),
          color: Colors.blueGrey,
        ),
        drawer: SideMenuView(),
        body: model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : ListView(
                children: [
                  Container(
                    height: 180,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            Colors.grey[800]!,
                            Colors.black54,
                          ],
                          begin: const FractionalOffset(1.0, 0.0),
                          end: const FractionalOffset(0.0, 0.0),
                          stops: const [0.0, 1.0],
                          tileMode: TileMode.clamp),
                    ),
                    child: const Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Text(
                          'مساحة اعلانية',
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        HomeTileWidget(
                          onTap: () {
                            locator<NavigationService>()
                                .navigateTo(Routes.workersView);
                          },
                          image:
                              'https://images.unsplash.com/photo-1560250097-0b93528c311a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=987&q=80',
                          title: 'افراد',
                        ),
                        const HomeTileWidget(
                          image:
                              'https://images.unsplash.com/photo-1618090584176-7132b9911657?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2128&q=80',
                          title: 'مقاولون',
                        ),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Divider(
                      color: Colors.white,
                      thickness: 1,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          'متاجر ومواد بناء',
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          'تسوق حسب القسم',
                          style: const TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GridView.builder(
                        itemCount: model.categoryList.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 200,
                                childAspectRatio: 0.88,
                                crossAxisSpacing: 20,
                                mainAxisSpacing: 20),
                        itemBuilder: (context, index) => HomeTileWidget(
                            title: model.categoryList[index].name,
                            image: model.categoryList[index].imageUrl,
                            onTap: () {
                              locator<NavigationService>().navigateTo(
                                  Routes.storeListView,
                                  arguments: StoreListViewArguments(
                                      category: model.categoryList[index]));
                            })),
                  ),
                ],
              ),
      ),
      onModelReady: (model) {
        model.getMyAccountInformation();
        model.loadCatigories();
        // model.getUserProfile();
      },
    );

    // }
    //   },
    // ));
  }
}

class HomeTileWidget extends StatelessWidget {
  final String image;
  final String title;
  final double? height;
  final double? width;
  final Function()? onTap;

  const HomeTileWidget({
    Key? key,
    required this.image,
    required this.title,
    this.onTap,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        // padding: EdgeInsets.all(16),
        height: height ?? size.width / 2,
        width: width ?? size.width / 2.5,

        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          image: DecorationImage(
            image: NetworkImage(image),
            fit: BoxFit.cover,
          ),
        ),
        child: Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Chip(
                label: Text(title),
                labelStyle: const TextStyle(fontWeight: FontWeight.bold),
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
            )),
      ),
    );
  }
}
