import 'dart:developer';

import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/category.dart';
import 'package:exaaz/core/data/models/profile_info.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../core/data/models/user_profile.dart';
import '../../../core/data/repository/account_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class HomeViewModel extends BaseViewModel {
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  final _accountRepository = locator<AccountRepository>();
  final _navigationService = locator<NavigationService>();
  final _storeService = locator<StoreRepository>();
  bool get isServiceProvider => sharedPreferencesRepository.getIsSP();
  List<Category> categoryList = [];
  void getUserProfile() async {
    try {
      setBusy(true);
      final commonResponse = await _accountRepository.getMyProfile();
      if (commonResponse.getStatus) {
        UserProfile user = UserProfile.fromJson(commonResponse.getData);
        sharedPreferencesRepository.saveUserInfo(user: user);

        if (user.requestedToBeServiceProvider != null) {
          if (user.requestedToBeServiceProvider) {
            if (user.requestApproved) {
              if (!user.isProfileComplete) {
                _navigationService.pushNamedAndRemoveUntil(
                    Routes.workerProfileView,
                    arguments: WorkerProfileViewArguments(
                        isUpdate: user.isProfileComplete));
              }
            }
          }
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void getMyAccountInformation() async {
    try {
      setBusy(true);
      final commonResponse = await _accountRepository.getMyAccountInformation();
      if (commonResponse.getStatus) {
        ProfileInfo profile = ProfileInfo.fromJson(commonResponse.getData);
        sharedPreferencesRepository.saveProfileInfo(profile: profile);
        setBusy(false);
        if (profile.isRequestedToBeServiceProvider) {
          getUserProfile();
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void loadCatigories() async {
    setBusy(true);
    try {
      final commonResponse = await _storeService.getCategories();
      // log(commonResponse.data.toString());

      if (commonResponse.getStatus) {
        log(commonResponse.data.toString());
        categoryList.clear();
        Iterable list = commonResponse.data;
        list.forEach((element) {
          categoryList.add(Category.fromJson(element));
        });
        notifyListeners();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      log(e.toString());
    }
  }

  void navigateToProjects() {
    if (!isServiceProvider) {
      CustomToasts.showMessage(
          message: 'يرجى التسجيل كمزود خدمة',
          messageType: MessageType.alertMessage);
    } else {
      _navigationService.navigateTo(Routes.serviceProviderProjectListView);
    }
  }
}
