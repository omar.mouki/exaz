import 'package:photo_view/photo_view.dart';

import '../../../../app/locator.dart';

import 'package:flutter/material.dart';

import 'package:stacked_services/stacked_services.dart';

import '../../shared/colors.dart';

class PhotoGalleryView extends StatefulWidget {
  final String imageUrl;

  const PhotoGalleryView({Key? key, required this.imageUrl}) : super(key: key);
  @override
  _PhotoGalleryViewState createState() => _PhotoGalleryViewState();
}

class _PhotoGalleryViewState extends State<PhotoGalleryView> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.9),
      body: Stack(
        children: [
          PhotoView(
            heroAttributes: PhotoViewHeroAttributes(
              tag: 'photo_gallery_hero',
            ),
            loadingBuilder: (context, event) => Center(
              child: Container(
                width: 20.0,
                height: 20.0,
                child: CircularProgressIndicator(
                  value: event == null
                      ? 0
                      : event.cumulativeBytesLoaded / event.expectedTotalBytes!,
                ),
              ),
            ),
            imageProvider: NetworkImage(widget.imageUrl),
          ),
          Align(
            alignment: AlignmentDirectional.topEnd,
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).padding.top + 16,
                horizontal: size.width / 18,
              ),
              child: Container(
                child: IconButton(
                    onPressed: () => locator<NavigationService>().back(),
                    icon: Icon(
                      Icons.close,
                      color: AppColors.yellowColor,
                      size: 30,
                    )),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
