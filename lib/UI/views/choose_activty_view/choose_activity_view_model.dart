import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';
import '../../../core/data/models/service_provider_type.dart';
import '../../../core/data/repository/change_request_repository.dart';
import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

//to rejester as a store the id is 2
class ChooseActivtyViewModel extends BaseViewModel {
  ChangeRequestRepository _changeRequestRepository =
      locator<ChangeRequestRepository>();

  List<ServiceProviderType> _types = [];
  List<ServiceProviderType> get types => _types;

  List<DropdownMenuItem<ServiceProviderType>> _items = [];
  List<DropdownMenuItem<ServiceProviderType>> get items => _items;

  late PickedFile _residenceImgage;
  PickedFile get residenceImgage => _residenceImgage;

  changeresidencePicture(ImageSource image) async {
    final picker = ImagePicker();

    var value = await picker.getImage(source: image);
    if (value != null) {
      _residenceImgage = value;
    }

    notifyListeners();
  }

  void getServiceType() async {
    try {
      setBusy(true);
      final commonResponse =
          await _changeRequestRepository.getServiceProviderTypes();
      if (commonResponse.getStatus) {
        setBusy(false);
        Iterable list = commonResponse.getData;
        _types =
            list.map((model) => ServiceProviderType.fromJson(model)).toList();

        for (int i = 0; i < _types.length; i++) {
          _items.add(DropdownMenuItem(
            value: _types[i],
            child: Text(_types[i].name),
          ));
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
