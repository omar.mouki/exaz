import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../widgets/activity_item.dart';
import 'choose_activity_view_model.dart';

class ChooseActivityView extends StatelessWidget {
  const ChooseActivityView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
        body: ViewModelBuilder<ChooseActivtyViewModel>.reactive(
            viewModelBuilder: () => ChooseActivtyViewModel(),
            onModelReady: (model) {
              model.getServiceType();
            },
            builder: (context, model, child) {
              return model.isBusy
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Image.asset(
                          'assets/pngs/choose_service_bg.png',
                          fit: BoxFit.fill,
                          width: size.width,
                          height: size.height,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: size.height / 3),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                tr('select_activity'),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: size.shortestSide / 24),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Expanded(
                                child: GridView.count(
                                  crossAxisCount: 3,
                                  children: List<Widget>.generate(
                                      model.types.length, (index) {
                                    return GridTile(
                                      child: ActivityItem(
                                        onTab: () {
                                          locator<NavigationService>()
                                              .navigateTo(Routes.registerSpView,
                                                  arguments:
                                                      RegisterSpViewArguments(
                                                          typeId: model
                                                              .types[index]
                                                              .id));
                                        },
                                        title: model.types[index].name,
                                        svgPath: model.types[index].iconUrl,
                                      ),
                                    );
                                  }),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    );
            }));
  }
}
