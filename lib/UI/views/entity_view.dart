import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import '../shared/custom_widgets/custom_appbar.dart';
import '../shared/custom_widgets/custom_image_network.dart';

import '../widgets/bio_tab.dart';
import '../widgets/info_tab.dart';
import '../widgets/products_tab.dart';

class EntityView extends StatefulWidget {
  const EntityView({Key? key}) : super(key: key);

  @override
  _EntityViewState createState() => _EntityViewState();
}

class _EntityViewState extends State<EntityView>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int _tabControllerIndex = 0;
  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this)
      ..addListener(() {
        setState(() {
          _tabControllerIndex = _tabController.index;
        });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.blackColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const CustomAppBar(),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: size.width / 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(
                      height: 16,
                    ),
                    AspectRatio(
                      aspectRatio: 1.7,
                      child: CircularImageNetwork(
                        imagePath: '',
                        imageSize: null,
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      tr('institution_name'),
                      style: TextStyle(
                          color: AppColors.whiteColor,
                          fontWeight: FontWeight.w500,
                          fontSize: size.shortestSide / 24),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          tr('activity_type'),
                          style: TextStyle(
                              color: AppColors.yellowColor,
                              fontWeight: FontWeight.w500,
                              fontSize: size.shortestSide / 30),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          tr('city'),
                          style: TextStyle(
                              color: AppColors.grey200Color,
                              fontWeight: FontWeight.w500,
                              fontSize: size.shortestSide / 30),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          tr('membership_number'),
                          style: TextStyle(
                              color: AppColors.whiteColor,
                              fontWeight: FontWeight.w500,
                              fontSize: size.shortestSide / 30),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          '12',
                          style: TextStyle(
                              color: AppColors.whiteColor,
                              fontWeight: FontWeight.w500,
                              fontSize: size.shortestSide / 30),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            _tabController.animateTo(0);
                          },
                          child: Text(
                            tr('bio'),
                            style: TextStyle(
                                color: _tabControllerIndex == 0
                                    ? AppColors.yellowColor
                                    : AppColors.grey200Color,
                                fontWeight: FontWeight.w400,
                                fontSize: size.shortestSide / 26),
                          ),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        InkWell(
                          onTap: () {
                            _tabController.animateTo(1);
                          },
                          child: Text(
                            tr('our_products'),
                            style: TextStyle(
                                color: _tabControllerIndex == 1
                                    ? AppColors.yellowColor
                                    : AppColors.grey200Color,
                                fontWeight: FontWeight.w400,
                                fontSize: size.shortestSide / 26),
                          ),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        InkWell(
                          onTap: () {
                            _tabController.animateTo(2);
                          },
                          child: Text(
                            tr('contacts_info'),
                            style: TextStyle(
                                color: _tabControllerIndex == 2
                                    ? AppColors.yellowColor
                                    : AppColors.grey200Color,
                                fontWeight: FontWeight.w400,
                                fontSize: size.shortestSide / 26),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: size.height / 2,
                      child: TabBarView(
                          controller: _tabController,
                          children: [BioTab(), ProductsTab(), InfoTab()]),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
