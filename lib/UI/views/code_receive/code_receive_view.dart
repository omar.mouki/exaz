import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:stacked/stacked.dart';

import 'code_receive_view_model.dart';

class CodeReceiveView extends StatefulWidget {
  final String phoneNumber;

  const CodeReceiveView({
    Key? key,
    required this.phoneNumber,
  }) : super(key: key);

  @override
  _CodeReceiveBottomWidgetState createState() =>
      _CodeReceiveBottomWidgetState();
}

class _CodeReceiveBottomWidgetState extends State<CodeReceiveView> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: ViewModelBuilder<CodeReceiveModel>.reactive(
        viewModelBuilder: () => CodeReceiveModel(),
        builder: (context, model, child) => Stack(
          children: <Widget>[
            Image.asset(
              'assets/pngs/choose_service_bg.png',
              fit: BoxFit.fill,
              width: size.width,
              height: size.height,
            ),
            Container(
              padding: EdgeInsets.only(
                  top: size.shortestSide / 16,
                  left: size.shortestSide / 16,
                  right: size.shortestSide / 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  PinCodeTextField(
                    controller: _controller,
                    textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: size.shortestSide / 20),
                    length: 4,
                    onCompleted: (code) {
                      model.verifyCode(
                          phoneNumber: widget.phoneNumber, code: code);
                    },
                    // activeFillColor: Colors.red,
                    backgroundColor: Colors.transparent,
                    // textInputType: TextInputType.phone,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    // activeColor: AppColors.yellowColor,
                    // selectedColor: AppColors.grey200Color,
                    // inactiveColor: AppColors.grey300Color,
                    animationType: AnimationType.fade,
                    // shape: PinCodeFieldShape.underline,
                    animationDuration: Duration(milliseconds: 300),
                    // borderRadius: BorderRadius.circular(5),
                    // fieldHeight: size.longestSide / 12,
                    // fieldWidth: size.shortestSide / 8,
                    onChanged: (value) {}, appContext: context,
                  ),
                  SizedBox(
                    height: size.height / 20,
                  ),
                  InkWell(
                    onTap: () {},
                    child: Text(
                      tr('resend_code'),
                      style: TextStyle(
                          color: AppColors.blackColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: size.height / 20,
                  ),
                  // FlatButton(
                  //   color: AppColors.blackColor,
                  //   shape: RoundedRectangleBorder(
                  //     borderRadius: BorderRadius.circular(18.0),
                  //   ),
                  //   onPressed: () {},
                  //   child: SizedBox(
                  //     width: size.width / 1.2,
                  //     child: Center(
                  //       child: Text(tr( 'sign_in'),
                  //           style: TextStyle(
                  //               color: AppColors.whiteColor,
                  //               fontSize: size.shortestSide / 21,
                  //               fontWeight: FontWeight.w500)),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
