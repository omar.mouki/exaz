import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import '../../../core/data/repository/auth_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class CodeReceiveModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  NavigationService _navigationService = locator<NavigationService>();

  void verifyCode({required String phoneNumber, required String code}) async {
    try {
      setBusy(true);
      final commonResponse = await _authenticationRepository.codeVerification(
        verificationCode: code,
        phoneNumber: phoneNumber,
      );
      if (commonResponse.getStatus) {
        setBusy(false);
        CustomToasts.showMessage(
            message: tr('register_succ'),
            messageType: MessageType.successMessage);
        _navigationService.pushNamedAndRemoveUntil(Routes.signInView);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
