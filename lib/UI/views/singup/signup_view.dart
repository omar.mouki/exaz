import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';

import '../../../core/data/models/city.dart';

import '../../shared/custom_widgets/custom_lodaer.dart';
import '../../shared/custom_widgets/custom_toasts.dart';
import '../../shared/custom_widgets/svg_icon_button.dart';
import '../../shared/custom_widgets/text_field_widget.dart';
import '../../../core/utils/string_utils.dart';

import 'singup_view_model.dart';

class SignUpView extends StatefulWidget {
  SignUpView({Key? key}) : super(key: key);

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  bool _isApproved = false;

  TextEditingController _fullNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phonecontroller = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  FocusNode _nameFocusNode = FocusNode();
  FocusNode _numberFocusNode = FocusNode();
  FocusNode _emailFocusNode = FocusNode();
  FocusNode _passwordFocusNode = FocusNode();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  City? _city;
  int cityId = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: ViewModelBuilder<SingupViewModel>.reactive(
        viewModelBuilder: () => SingupViewModel(),
        onModelReady: (model) {
          model.getCities();
        },
        builder: (context, model, child) => model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/pngs/choose_service_bg.png',
                    fit: BoxFit.cover,
                    width: size.width,
                    height: size.height,
                  ),
                  SingleChildScrollView(
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width / 20),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: size.height / 5,
                            ),
                            Center(
                              child: Text(
                                tr('new_register'),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: size.width / 22,
                                  color: AppColors.blackColor,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: size.height / 40,
                            ),
                            TextFieldWidget(
                              textEditingController: _fullNameController,
                              title: tr('full_name'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              textColor: AppColors.blackColor,
                              // inputFormatters: [
                              //   FilteringTextInputFormatter.allow(
                              //       RegExp("[ A-Za-z]"))
                              // ],
                              onValidate: (text) =>
                                  text != null && text.isNotEmpty
                                      ? null
                                      : tr('required'),
                              fieldFocusNode: _nameFocusNode,
                              nextFocusNode: _emailFocusNode,
                            ),
                            TextFieldWidget(
                              textEditingController: _emailController,
                              title: tr('email'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              textColor: AppColors.blackColor,
                              keyboardType: TextInputType.emailAddress,
                              onValidate: (text) =>
                                  text != null && StringUtil.isValidEmail(text)
                                      ? null
                                      : tr('required'),
                              fieldFocusNode: _emailFocusNode,
                              nextFocusNode: _numberFocusNode,
                            ),
                            _buildCityWidget(context, model),
                            TextFieldWidget(
                              textEditingController: _phonecontroller,
                              title: tr('phone_number'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              textColor: AppColors.blackColor,
                              keyboardType: TextInputType.phone,
                              maxLength: 10,
                              onValidate: (text) => text != null &&
                                      text.length > 9
                                  ? null
                                  : tr('ReportProblemController_invalidePhone'),
                              fieldFocusNode: _numberFocusNode,
                              nextFocusNode: _passwordFocusNode,
                            ),
                            TextFieldWidget(
                              textEditingController: _passwordController,
                              title: tr('password'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              textColor: AppColors.blackColor,
                              keyboardType: TextInputType.visiblePassword,
                              onValidate: (text) =>
                                  text != null && text.length > 6
                                      ? null
                                      : tr('invalidPassword'),
                              fieldFocusNode: _passwordFocusNode,
                            ),
                            Row(
                              children: <Widget>[
                                Checkbox(
                                  value: _isApproved,
                                  onChanged: (bool) {
                                    setState(() {
                                      _isApproved = !_isApproved;
                                    });
                                  },
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  activeColor: AppColors.yellowColor,
                                  visualDensity: VisualDensity.compact,
                                ),
                                Text(
                                  tr('agree_terms_conditions'),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: size.shortestSide / 36),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: size.height / 12,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  color: AppColors.blackColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                  ),
                                  onPressed: () {
                                    if (_city != null) {
                                      if (_formKey.currentState!.validate()) {
                                        if (_isApproved) {
                                          model.isPhoneNumberExist(
                                            phoneNumber: _phonecontroller.text,
                                            cityId: _city!.id,
                                            email: _emailController.text,
                                            fullName: _fullNameController.text,
                                            password: _passwordController.text,
                                          );

                                          _formKey.currentState!.reset();
                                        } else {
                                          CustomToasts.showMessage(
                                              message: tr('check_terms'),
                                              messageType:
                                                  MessageType.alertMessage);
                                        }
                                      }
                                    } else {
                                      CustomToasts.showMessage(
                                          message: tr('please_select_city'),
                                          messageType:
                                              MessageType.alertMessage);
                                    }
                                  },
                                  child: SizedBox(
                                    width: size.width / 1.2,
                                    child: Center(
                                      child: Text(tr('sign'),
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: size.shortestSide / 21,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: size.height / 40,
                                ),
                                Text(
                                  tr('or_register_by'),
                                  style: TextStyle(
                                    fontSize: size.width / 28,
                                    color: AppColors.grey300Color,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SvgIconButton(
                                      onPressed: () async {
                                        model.singinWithFacebook();
                                      },
                                      svgPath: 'assets/icons/facebook_logo.svg',
                                      width: size.width / 12,
                                    ),
                                    SvgIconButton(
                                      onPressed: () async {
                                        model.singinWithGoogle();
                                      },
                                      svgPath: 'assets/icons/google_logo.svg',
                                      width: size.width / 12,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: size.height / 8,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }

  Widget _buildCityWidget(BuildContext context, SingupViewModel model) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: DropdownButton<City>(
        isExpanded: true,
        value: _city,
        onChanged: (newVal) {
          setState(() {
            _city = newVal;
          });
        },
        items: model.items,
        hint: Text(tr('please_select_city')),
      ),
    );
  }
}
