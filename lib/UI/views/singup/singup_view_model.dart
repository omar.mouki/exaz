import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/repository/address_repository.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../core/data/models/city.dart';
import '../../../core/data/models/token_info.dart';

import '../../../core/data/repository/auth_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../../core/utils/social_media_util.dart';
import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class SingupViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  AddressRepository addressRepository = locator<AddressRepository>();
  NavigationService _navigationService = locator<NavigationService>();

  List<City> _cityList = [];
  List<City> get cityList => _cityList;

  List<DropdownMenuItem<City>> _items = [];
  List<DropdownMenuItem<City>> get items => _items;

  Future<void> singinWithFacebook() async {
    // String token = await SocialMediaUtil().signInWithFacebook();
    // if (token != null) {
    //   print(token);
    //   tokenByFacebookTokenRequest(token: token);
    // } else {
    //   CustomToasts.showMessage(
    //       message: 'Facebok token is null',
    //       messageType: MessageType.errorMessage);
    // }
  }

  Future<void> singinWithGoogle() async {
    String? token = await SocialMediaUtil().signInWithGoogle();
    if (token != null) {
      print(token);
      tokenByGoogleTokenRequest(token: token);
    } else {
      CustomToasts.showMessage(
          message: 'Google token is null',
          messageType: MessageType.errorMessage);
    }
  }

  void getCities() async {
    setBusy(true);
    try {
      final commonResponse = await addressRepository.getCities();
      if (commonResponse.getStatus) {
        setBusy(false);

        Iterable list = commonResponse.getData;
        _cityList = list.map((model) => City.fromJson(model)).toList();

        for (int i = 0; i < _cityList.length; i++) {
          _items.add(DropdownMenuItem(
            value: _cityList[i],
            child: Text(_cityList[i].name),
          ));
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void isPhoneNumberExist(
      {required String phoneNumber,
      required String email,
      required String fullName,
      required String password,
      required int cityId}) async {
    try {
      final commonResponse = await _authenticationRepository.isPhoneNumberExist(
        phoneNumber: phoneNumber,
      );
      if (commonResponse.getStatus) {
        if (commonResponse.data) {
          CustomToasts.showMessage(
              message: tr('phone_number_exist'),
              messageType: MessageType.alertMessage);
        } else {
          singup(
              cityId: cityId,
              email: email,
              fullName: fullName,
              password: password,
              phoneNumber: phoneNumber);
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void singup(
      {required String phoneNumber,
      required String email,
      required String fullName,
      required String password,
      required int cityId}) async {
    try {
      final commonResponse = await _authenticationRepository.singup(
          phoneNumber: phoneNumber,
          email: email,
          fullName: fullName,
          password: password,
          cityId: cityId);
      if (commonResponse.getStatus) {
        _navigationService.navigateTo(Routes.codeReceiveView,
            arguments: CodeReceiveViewArguments(phoneNumber: phoneNumber));
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void tokenByFacebookTokenRequest({required String token}) async {
    try {
      setBusy(true);
      final commonResponse =
          await _authenticationRepository.tokenByFacebookToken(token: token);
      if (commonResponse.getStatus) {
        sharedPreferencesRepository.saveTokenInfo(
            tokenInfo: TokenInfo.fromJson(commonResponse.getData));
        sharedPreferencesRepository.setLoggedIn(isLoggedIn: true);
        _navigationService.pushNamedAndRemoveUntil(Routes.landingView);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void tokenByGoogleTokenRequest({required String token}) async {
    try {
      setBusy(true);
      final commonResponse =
          await _authenticationRepository.tokenByGoogleToken(token: token);
      if (commonResponse.getStatus) {
        sharedPreferencesRepository.saveTokenInfo(
            tokenInfo: TokenInfo.fromJson(commonResponse.getData));
        sharedPreferencesRepository.setLoggedIn(isLoggedIn: true);
        _navigationService.pushNamedAndRemoveUntil(Routes.landingView);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
