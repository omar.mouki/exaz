import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../app/locator.dart';
import '../shared/custom_widgets/svg_icon_button.dart';
import '../shared/custom_widgets/text_field_widget.dart';

class LandingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.blackColor,
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          SizedBox(
            height: size.height / 3.56,
            child: Container(
              width: size.width,
              color: AppColors.darkGreyColor,
            ),
          ),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: size.height / 4.5,
                ),
                SizedBox(
                  height: size.shortestSide / 4,
                  width: size.shortestSide / 4,
                  child: ClipOval(
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Container(
                          color: AppColors.whiteColor,
                        ),
                        Container(
                          height: size.shortestSide / 8.1,
                          width: size.shortestSide / 4,
                          color: AppColors.grey100Color,
                          child: SvgIconButton(
                            svgPath: 'assets/icons/add_photo.svg',
                            width: size.shortestSide / 21,
                            color: const Color.fromRGBO(45, 62, 80, 1),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(tr('name'),
                    style: TextStyle(
                        color: AppColors.yellowColor,
                        fontSize: size.shortestSide / 22,
                        fontWeight: FontWeight.bold)),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 24,
                      ),
                      Text(tr('user_info'),
                          style: TextStyle(
                              color: AppColors.yellowColor,
                              fontSize: size.shortestSide / 22,
                              fontWeight: FontWeight.w300)),
                      const SizedBox(
                        height: 8,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.horizontal(
                              end: Radius.circular(size.width / 40)),
                          gradient: const LinearGradient(
                            colors: [
                              Color.fromRGBO(33, 32, 32, 1),
                              Color.fromRGBO(52, 52, 52, 1),
                            ],
                            end: AlignmentDirectional.centerStart,
                            begin: AlignmentDirectional.centerEnd,
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                SvgIconButton(
                                  svgPath: 'assets/icons/info_phone.svg',
                                  width: size.shortestSide / 16,
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                    child: TextFieldWidget(
                                  title: '',
                                  isDense: true,
                                ))
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                SvgIconButton(
                                  svgPath: 'assets/icons/info_whatsapp.svg',
                                  width: size.shortestSide / 16,
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                    child: TextFieldWidget(
                                  title: '',
                                  isDense: true,
                                ))
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                SvgIconButton(
                                  svgPath: 'assets/icons/info_messages.svg',
                                  width: size.shortestSide / 16,
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                    child: TextFieldWidget(
                                  title: '',
                                  isDense: true,
                                ))
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                FlatButton(
                  color: AppColors.yellowColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  onPressed: () {
                    locator<NavigationService>()
                        .navigateTo(Routes.registerSpView);
                  },
                  child: Text(tr('join_us_as_sp'),
                      style: TextStyle(
                          color: AppColors.whiteColor,
                          fontSize: size.shortestSide / 21,
                          fontWeight: FontWeight.w500)),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
