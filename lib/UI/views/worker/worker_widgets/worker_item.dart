import 'package:cached_network_image/cached_network_image.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/core/utils/utils.dart';
import 'package:flutter/material.dart';

import '../../../shared/custom_widgets/svg_icon_button.dart';

class WorkerItem extends StatefulWidget {
  final String work;
  final String name;
  final String city;
  final String imgUrl;
  final String nationalty;
  final int ratting;
  final bool isWhiteBall;
  final Function shareOnTab;
  final Function(bool) favoriteOnTab;
  final bool isFavorite;

  const WorkerItem(
      {Key? key,
      required this.work,
      required this.imgUrl,
      required this.name,
      required this.city,
      required this.nationalty,
      required this.ratting,
      this.isWhiteBall = false,
      required this.shareOnTab,
      required this.favoriteOnTab,
      this.isFavorite = false})
      : super(key: key);

  @override
  _WorkerItemState createState() => _WorkerItemState();
}

class _WorkerItemState extends State<WorkerItem> {
  late bool isFavorite;
  @override
  void initState() {
    isFavorite = widget.isFavorite;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: AspectRatio(
        aspectRatio: 3,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              margin:
                  EdgeInsetsDirectional.only(start: size.width / 5, end: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadiusDirectional.horizontal(
                      end: Radius.circular(size.width / 40)),
                  gradient: const LinearGradient(
                    colors: [
                      Color.fromRGBO(33, 32, 32, 1),
                      Color.fromRGBO(52, 52, 52, 1),
                    ],
                    end: AlignmentDirectional.centerStart,
                    begin: AlignmentDirectional.centerEnd,
                  )),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AspectRatio(
                    aspectRatio: 1,
                    child: ClipRRect(
                      borderRadius:
                          BorderRadius.circular(size.shortestSide / 40),
                      child: CachedNetworkImage(
                        imageUrl: widget.imgUrl,
                        fit: BoxFit.cover,
                        placeholder: (context, s) =>
                            const Center(child: CircularProgressIndicator()),
                        errorWidget: (context, s, o) => Container(
                          color: AppColors.whiteColor,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 4),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        widget.name,
                                        style: TextStyle(
                                            color: AppColors.whiteColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: size.shortestSide / 24),
                                      ),
                                      if (widget.isWhiteBall)
                                        Container(
                                          width: 16,
                                          height: 16,
                                          decoration: const BoxDecoration(
                                              color: AppColors.whiteColor,
                                              shape: BoxShape.circle),
                                        ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Text(
                                    widget.city,
                                    style: TextStyle(
                                        color: AppColors.grey300Color,
                                        fontWeight: FontWeight.w500,
                                        fontSize: size.shortestSide / 27),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        widget.work,
                                        style: TextStyle(
                                            color: AppColors.yellowColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: size.shortestSide / 27),
                                      ),
                                      Text(
                                        widget.nationalty,
                                        style: TextStyle(
                                            color: AppColors.yellowColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: size.shortestSide / 27),
                                      ),
                                      const SizedBox(),
                                    ],
                                  ),
                                ),
                                const SizedBox(),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.yellowColor,
                                    borderRadius: BorderRadius.circular(2),
                                  ),
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 4),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        color: AppColors.whiteColor,
                                        size: size.width / 24,
                                      ),
                                      const SizedBox(
                                        width: 4,
                                      ),
                                      Text(
                                        widget.ratting.toString(),
                                        style: TextStyle(
                                          color: AppColors.whiteColor,
                                          fontSize: size.width / 28,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SvgIconButton(
                                  svgPath: 'assets/icons/share.svg',
                                  width: size.width / 21,
                                  color: AppColors.whiteColor,
                                  onPressed: widget.shareOnTab,
                                ),
                                if (Utils.isLoogedIn)
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        isFavorite = !isFavorite;
                                      });
                                      widget.favoriteOnTab(isFavorite);
                                    },
                                    child: Icon(
                                      isFavorite
                                          ? Icons.favorite
                                          : Icons.favorite_border,
                                      color: AppColors.whiteColor,
                                      size: size.width / 21,
                                    ),
                                  ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
