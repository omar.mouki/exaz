import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../../app/locator.dart';
import '../../../../core/data/models/worker_fee_dto.dart';
import '../../../../core/data/models/worker_professional.dart';
import '../../../../core/data/models/worker_profile_data.dart';
import '../../../shared/custom_widgets/svg_icon_button.dart';
import '../../../shared/custom_widgets/text_field_widget.dart';

import 'profession_dropdown.dart';
import 'service_item.dart';

class WorkerServicesTab extends StatefulWidget {
  final List<WorkerProfession> workerProfessions;

  const WorkerServicesTab({
    Key? key,
    required this.workerProfessions,
  }) : super(key: key);
  @override
  _WorkerServicesTabState createState() => _WorkerServicesTabState();
}

class _WorkerServicesTabState extends State<WorkerServicesTab> {
  WorkerProfileData _workerProfileData = locator<WorkerProfileData>();
  TextEditingController _workerProfessionsController = TextEditingController();

  int houeFeeTotal = 0;
  int dayFeeTotal = 0;
  int monthFeeTotal = 0;

  @override
  void initState() {
    // _workerProfileData.houeFee = WorkerFeeDto();
    // _workerProfileData.dayFee = WorkerFeeDto();
    // _workerProfileData.monthFee = WorkerFeeDto();

    // _workerProfileData.houeFee.feeList = List();
    // _workerProfileData.dayFee.feeList = List();
    // _workerProfileData.monthFee.feeList = List();

    _workerProfileData.houeFee?.feeIncludedTypes = 0;
    _workerProfileData.houeFee?.feeType = 1;

    _workerProfileData.dayFee?.feeIncludedTypes = 0;
    _workerProfileData.dayFee?.feeType = 2;

    _workerProfileData.monthFee?.feeIncludedTypes = 0;
    _workerProfileData.monthFee?.feeType = 3;

    for (int i = 0; i < _workerProfileData.houeFee!.feeList.length; i++) {
      houeFeeTotal += _workerProfileData.houeFee!.feeList[i].value;
    }

    for (int i = 0; i < _workerProfileData.dayFee!.feeList.length; i++) {
      dayFeeTotal += _workerProfileData.dayFee!.feeList[i].value;
    }
    for (int i = 0; i < _workerProfileData.monthFee!.feeList.length; i++) {
      monthFeeTotal += _workerProfileData.monthFee!.feeList[i].value;
    }

    if (_workerProfileData.professionName != null) {
      _workerProfessionsController.text != _workerProfileData.professionName;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ListView(
      shrinkWrap: true,
      padding:
          EdgeInsets.only(bottom: 60 + MediaQuery.of(context).padding.bottom),
      children: [
        Row(
          children: <Widget>[
            Text(
              tr('service_type'),
              style: TextStyle(
                  color: AppColors.grey200Color,
                  fontSize: size.shortestSide / 24),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
                child: InkWell(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return ProfessionDropDown(
                        professionList: widget.workerProfessions,
                        onChanged: (professionId, professionName) {
                          locator<NavigationService>().back();
                          setState(() {
                            _workerProfessionsController.text = professionName;
                            _workerProfileData.professionId = professionId;
                            _workerProfileData.professionName = professionName;
                          });
                        },
                      );
                    });
              },
              child: IgnorePointer(
                child: TextFieldWidget(
                  textEditingController: _workerProfessionsController,
                  isReadOnly: true,
                  suffixWidget: RotatedBox(
                    quarterTurns: 3,
                    child: SvgIconButton(
                        onPressed: () {},
                        svgPath: 'assets/icons/arrow_back.svg',
                        color: AppColors.grey100Color,
                        width: size.width / 28),
                  ),
                  title: '',
                  isDense: true,
                ),
              ),
            ))
          ],
        ),
        ServiceItem(
          value: _workerProfileData.houeFee!.value,
          title: tr('hour_fee'),
          valueCallback: (value) {
            _workerProfileData.houeFee!.value = double.parse(value);
          },
          checkItemsCallback: (list) {
            for (var item in list) {
              if (item.value) {
                _workerProfileData.houeFee!.addToList(FeeIncloudType(
                    id: int.parse(item.id!), value: int.parse(item.id!)));
                _workerProfileData.houeFeeMap![item.name!] = true;
              } else {
                _workerProfileData.houeFee!.removeFromList(int.parse(item.id!));
                _workerProfileData.houeFeeMap![item.name!] = false;
              }
            }
          },
          checkItems: [
            CheckItem(
              value:
                  _workerProfileData.houeFeeMap![tr('include_food')] ?? false,
              id: '1',
              name: tr('include_food'),
            ),
            CheckItem(
              value: _workerProfileData
                      .houeFeeMap![tr('includes_transportation')] ??
                  false,
              id: '2',
              name: tr('includes_transportation'),
            ),
          ],
        ),
        ServiceItem(
          value: _workerProfileData.dayFee!.value,
          title: tr('day_fee'),
          valueCallback: (value) {
            _workerProfileData.dayFee!.value = double.parse(value);
          },
          checkItemsCallback: (list) {
            for (var item in list) {
              if (item.value) {
                _workerProfileData.dayFee!.addToList(FeeIncloudType(
                    id: int.parse(item.id!), value: int.parse(item.id!)));

                _workerProfileData.dayFeeMap![item.name!] = true;
              } else {
                _workerProfileData.dayFee!.removeFromList(int.parse(item.id!));
                _workerProfileData.dayFeeMap![item.name!] = false;
              }
            }
          },
          checkItems: [
            CheckItem(
              value: _workerProfileData.dayFeeMap![tr('include_food')] ?? false,
              id: '1',
              name: tr('include_food'),
            ),
            CheckItem(
              value: _workerProfileData
                      .dayFeeMap![tr('includes_transportation')] ??
                  false,
              id: '2',
              name: tr('includes_transportation'),
            ),
          ],
        ),
        ServiceItem(
          value: _workerProfileData.monthFee!.value,
          title: tr('month_fee'),
          valueCallback: (value) {
            _workerProfileData.monthFee!.value = double.parse(value);
          },
          checkItemsCallback: (list) {
            for (var item in list) {
              if (item.value) {
                _workerProfileData.monthFee!.addToList(FeeIncloudType(
                    id: int.parse(item.id!), value: int.parse(item.id!)));

                _workerProfileData.monthFeeMap![item.name!] = true;
              } else {
                _workerProfileData.monthFee!
                    .removeFromList(int.parse(item.id!));

                _workerProfileData.monthFeeMap![item.name!] = false;
              }
            }
          },
          checkItems: [
            CheckItem(
              value:
                  _workerProfileData.monthFeeMap![tr('include_food')] ?? false,
              id: '1',
              name: tr('include_food'),
            ),
            CheckItem(
              value: _workerProfileData
                      .monthFeeMap![tr('includes_transportation')] ??
                  false,
              id: '2',
              name: tr('includes_transportation'),
            ),
            CheckItem(
              value: _workerProfileData.monthFeeMap![tr('includes_housing')] ??
                  false,
              id: '4',
              name: tr('includes_housing'),
            )
          ],
        ),
      ],
    );
  }
}
