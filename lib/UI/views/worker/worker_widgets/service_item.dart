import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/text_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ServiceItem extends StatefulWidget {
  final String title;
  final double? value;
  final List<CheckItem> checkItems;
  final Function(List<CheckItem>) checkItemsCallback;
  final Function(String) valueCallback;
  const ServiceItem(
      {Key? key,
      required this.title,
      required this.checkItems,
      required this.checkItemsCallback,
      required this.valueCallback,
      this.value})
      : super(key: key);

  @override
  _ServiceItemState createState() => _ServiceItemState();
}

class _ServiceItemState extends State<ServiceItem> {
  late List<CheckItem> _checkItems;
  TextEditingController _valueController = TextEditingController();

  @override
  void initState() {
    _checkItems = widget.checkItems;
    if (widget.value != null) {
      _valueController.text = widget.value.toString();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          children: <Widget>[
            Text(
              widget.title,
              style: TextStyle(
                  color: AppColors.grey200Color,
                  fontSize: size.shortestSide / 24),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
                child: TextFieldWidget(
                    textEditingController: _valueController,
                    onChange: (string) {
                      widget.valueCallback(string);
                    },
                    isReadOnly: false,
                    title: '',
                    isDense: false,
                    keyboardType: TextInputType.phone))
          ],
        ),
        SizedBox(
          width: size.width,
          height: 45,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: widget.checkItems.length,
            itemBuilder: (context, index) => Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Theme(
                  data: ThemeData(
                    unselectedWidgetColor: AppColors.yellowColor,
                  ),
                  child: Checkbox(
                    value: _checkItems[index].value,
                    onChanged: (bool) {
                      setState(() {
                        _checkItems[index].value = !_checkItems[index].value;
                      });
                      widget.checkItemsCallback(_checkItems);
                      _checkItems[index].onChecked!(
                          bool!, _checkItems[index].name!);
                    },
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    activeColor: AppColors.yellowColor,
                    visualDensity: VisualDensity.compact,
                  ),
                ),
                Text(
                  widget.checkItems[index].name!,
                  style: TextStyle(
                      color: AppColors.grey200Color,
                      fontSize: size.shortestSide / 30),
                ),
                const SizedBox(
                  width: 8,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class CheckItem {
  final String? id;
  final String? name;
  bool value;
  final Function(bool, String)? onChecked;

  CheckItem({
    this.id,
    this.name,
    this.value = false,
    this.onChecked,
  });
}
