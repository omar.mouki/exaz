import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../shared/custom_widgets/custom_image_network.dart';
import '../../../shared/custom_widgets/ios_bottom_sheet.dart';

import '../../register_sp/register_sp_view_model.dart';

class WorkerResidenceTab extends StatefulWidget {
  final RegisterSpViewModel model;
  const WorkerResidenceTab({Key? key, required this.model}) : super(key: key);

  @override
  _WorkerResidenceTabState createState() => _WorkerResidenceTabState();
}

class _WorkerResidenceTabState extends State<WorkerResidenceTab> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Text(
          'إضافة صورة الإقامة',
          style: TextStyle(
              color: AppColors.blackColor,
              fontWeight: FontWeight.w500,
              fontSize: size.shortestSide / 24),
        ),
        const SizedBox(
          height: 8,
        ),
        AspectRatio(
          aspectRatio: 1.5,
          child: CircularImageNetwork(
            imgFit: BoxFit.fill,
            imageOnTab: () {
              IosBottomSheet.show(context: context, bottomSheetWidgets: [
                BottomSheetIosWidget(
                  title: tr('camera'),
                  textColor: AppColors.blueColor,
                  onPressed: () {
                    widget.model.changeresidencePicture(ImageSource.camera);
                  },
                ),
                BottomSheetIosWidget(
                  title: tr('gallery'),
                  textColor: AppColors.blueColor,
                  onPressed: () {
                    widget.model.changeresidencePicture(ImageSource.gallery);
                  },
                ),
              ]);
            },
            imageFile: widget.model.residenceImgage,
            isSquare: true,
            imagePath: '',
            // imageErrorBackroundColor: AppColors.grey100Color,
            // imageErrorColor: AppColors.grey200Color,
            placeHolderSize: size.width / 12,
            placeHolderSvgPath: 'assets/icons/photo_place_holder.svg',
          ),
        ),
      ],
    );
  }
}
