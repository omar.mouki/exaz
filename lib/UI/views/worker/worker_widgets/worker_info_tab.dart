import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';
import 'package:exaaz/UI/shared/custom_widgets/text_field_widget.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/worker_profile_data.dart';
import 'package:flutter/material.dart';

class WorkerInfoTab extends StatefulWidget {
  final bool isUpdate;
  const WorkerInfoTab({Key? key, required this.isUpdate}) : super(key: key);

  @override
  _WorkerInfoTabState createState() => _WorkerInfoTabState();
}

class _WorkerInfoTabState extends State<WorkerInfoTab> {
  WorkerProfileData _workerProfileData = locator<WorkerProfileData>();

  TextEditingController _phoneController = TextEditingController();
  TextEditingController _whatsAppController = TextEditingController();

  @override
  void initState() {
    _phoneController.text = _workerProfileData.phoneNumber ?? '';
    _whatsAppController.text = _workerProfileData.whatsappNumber ?? '';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            SvgIconButton(
              svgPath: 'assets/icons/info_phone.svg',
              width: size.shortestSide / 16,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
                child: TextFieldWidget(
              maxLength: 10,
              keyboardType: TextInputType.phone,
              textEditingController: _phoneController,
              onChange: (text) {
                _workerProfileData.phoneNumber = text;
              },
              title: '',
              isDense: true,
            ))
          ],
        ),
        Row(
          children: <Widget>[
            SvgIconButton(
              svgPath: 'assets/icons/info_whatsapp.svg',
              width: size.shortestSide / 16,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
                child: TextFieldWidget(
              maxLength: 10,
              keyboardType: TextInputType.phone,
              textEditingController: _whatsAppController,
              onChange: (text) {
                _workerProfileData.whatsappNumber = text;
              },
              title: '',
              isDense: true,
            ))
          ],
        ),
      ],
    );
  }
}
