import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/core/data/models/worker_professional.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfessionDropDown extends StatelessWidget {
  final List<WorkerProfession> professionList;
  final Function(int, String) onChanged;

  const ProfessionDropDown(
      {required this.professionList, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.shortestSide / 10, vertical: size.shortestSide / 10),
      child: Column(
        children: [
          Text('الرجاء اختيار نوع الخدمة',
              style: TextStyle(
                  color: AppColors.blue150Color,
                  fontSize: size.shortestSide / 20,
                  fontWeight: FontWeight.bold)),
          const SizedBox(
            height: 25,
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: professionList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  onChanged(professionList[index].professionId,
                      professionList[index].professionName!);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(professionList[index].professionName!,
                        style: TextStyle(
                            color: AppColors.blackColor,
                            fontSize: size.shortestSide / 26,
                            fontWeight: FontWeight.bold)),
                    const Divider(color: Colors.black),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
