import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../core/data/models/city.dart';

class NationalitiesDropDown extends StatelessWidget {
  final List<City> nationalityList;
  final Function(int, String) onChanged;

  const NationalitiesDropDown(
      {required this.nationalityList, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.shortestSide / 10, vertical: size.shortestSide / 10),
      child: Column(
        children: [
          Text('الرجاء اختيار الجنسية',
              style: TextStyle(
                  color: AppColors.blue150Color,
                  fontSize: size.shortestSide / 20,
                  fontWeight: FontWeight.bold)),
          const SizedBox(
            height: 25,
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: nationalityList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  onChanged(
                      nationalityList[index].id, nationalityList[index].name);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(nationalityList[index].name,
                        style: TextStyle(
                            color: AppColors.blackColor,
                            fontSize: size.shortestSide / 26,
                            fontWeight: FontWeight.bold)),
                    const Divider(color: Colors.black),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
