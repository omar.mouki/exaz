import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/UI/views/worker/worker_widgets/worker_item.dart';
import 'package:exaaz/UI/views/worker/workers_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';

import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';

import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../shared/custom_widgets/share_appbar.dart';

class WorkersView extends StatefulWidget {
  @override
  _WorkersViewState createState() => _WorkersViewState();
}

class _WorkersViewState extends State<WorkersView> {
  NavigationService _navigationService = locator<NavigationService>();
  int _categoryIndex = 0;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.blackColor,
      body: ViewModelBuilder<WorkersViewModel>.reactive(
        viewModelBuilder: () => WorkersViewModel(),
        onModelReady: (model) {
          model.getWorkerProfession();
        },
        builder: (context, model, child) => model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : Column(
                children: <Widget>[
                  ShareAppBar(
                    backOnTab: () {
                      _navigationService.back();
                    },
                    title: tr('individuals_work'),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  SizedBox(
                    height: 40,
                    width: size.width,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: model.workerProfessions.length,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      itemBuilder: (context, index) => InkWell(
                        onTap: () {
                          setState(() {
                            _categoryIndex = index;
                          });
                          model.runGetWorkersList(model
                              .workerProfessions[_categoryIndex].professionId);
                        },
                        child: Container(
                          margin: const EdgeInsetsDirectional.only(end: 8),
                          padding: const EdgeInsets.symmetric(horizontal: 40),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: _categoryIndex == index
                                  ? AppColors.yellowColor
                                  : AppColors.whiteColor,
                            ),
                            borderRadius:
                                BorderRadius.circular(size.width / 40),
                          ),
                          child: Text(
                            model.workerProfessions[index].professionName ?? '',
                            style: TextStyle(
                                fontSize: size.width / 22,
                                color: AppColors.whiteColor),
                          ),
                        ),
                      ),
                    ),
                  ),
                  model.busy(model.workerList)
                      ? Center(child: CustomLoader())
                      : Expanded(
                          child: ListView.builder(
                          itemBuilder: (context, index) => InkWell(
                            onTap: () {
                              locator<NavigationService>().navigateTo(
                                  Routes.workerDetailsView,
                                  arguments: WorkerDetailsViewArguments(
                                      worker: model.workerList[index]));
                            },
                            child: WorkerItem(
                              isFavorite:
                                  model.workerList[index].isFavorite ?? false,
                              favoriteOnTab: (bool) {
                                if (bool) {
                                  model.addToFavorite(
                                      serviceProviderId:
                                          model.workerList[index].workerId);
                                } else {
                                  model.removeFromFavorite(
                                      serviceProviderId:
                                          model.workerList[index].workerId);
                                }
                              },
                              shareOnTab: () {
                                if (model.workerList[index].shareProfileLink !=
                                    null) {
                                  FlutterShare.share(
                                      title: '',
                                      text: '',
                                      linkUrl: model
                                          .workerList[index].shareProfileLink,
                                      chooserTitle: '');
                                } else {
                                  CustomToasts.showMessage(
                                      message:
                                          'shareProfileLink is return null from api',
                                      messageType: MessageType.errorMessage);
                                }
                              },
                              name: model.workerList[index].userFullName ??
                                  'لا يوجد محتوى',
                              city: model.workerList[index].worksInCityName ??
                                  'لا يوجد محتوى',
                              isWhiteBall: true,
                              nationalty: model.workerList[index].nationality ??
                                  'لا يوجد محتوى',
                              ratting: model.workerList[index].rating ?? 0,
                              work: model.workerList[index].professionName ??
                                  'لا يوجد محتوى',
                              imgUrl: model.workerList[index].pictureUrl ?? '',
                            ),
                          ),
                          itemCount: model.workerList.length,
                        )),
                ],
              ),
      ),
    );
  }
}
