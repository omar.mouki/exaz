import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_appbar.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_image_network.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_lodaer.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/UI/shared/custom_widgets/ios_bottom_sheet.dart';
import 'package:exaaz/UI/views/worker/worker_widgets/nationalities_dropdown.dart';
import 'package:exaaz/UI/views/worker/worker_widgets/worker_info_tab.dart';
import 'package:exaaz/UI/views/worker/worker_widgets/worker_services_tab.dart';
import 'package:exaaz/UI/views/worker/workers_view_model.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/core/data/models/profile_info.dart';
import 'package:exaaz/core/data/models/worker_fee_dto.dart';
import 'package:exaaz/core/data/models/worker_profile_data.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class WorkerProfileView extends StatefulWidget {
  final bool isUpdate;
  WorkerProfileView({Key? key, required this.isUpdate}) : super(key: key);

  @override
  _WorkerProfileViewState createState() => _WorkerProfileViewState();
}

class _WorkerProfileViewState extends State<WorkerProfileView>
    with SingleTickerProviderStateMixin {
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  late TabController _tabController;
  int _tabControllerIndex = 0;
  late ProfileInfo profile;

  WorkerProfileData _workerProfileData = locator<WorkerProfileData>();
  @override
  void initState() {
    profile = sharedPreferencesRepository.getProfileInfo()!;
    _tabController = TabController(length: 2, vsync: this)
      ..addListener(() {
        setState(() {
          _tabControllerIndex = _tabController.index;
        });
      });

    _workerProfileData.addDefaultDataToMap();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: AppColors.blackColor,
        body: ViewModelBuilder<WorkersViewModel>.reactive(
          viewModelBuilder: () => WorkersViewModel(),
          onModelReady: (model) async {
            if (widget.isUpdate) {
              await model.getMyDetails();

              final worker = model.worker;

              _workerProfileData.phoneNumber = worker!.userName;
              _workerProfileData.whatsappNumber = worker.userName;
              _workerProfileData.professionName = worker.professionName;

              for (int i = 0; i < worker.fees.length; i++) {
                for (int j = 0;
                    j < worker.fees[i].feeIncludedFeatures.length;
                    j++) {
                  if (worker.fees[i].feeType == 1) {
                    _workerProfileData.houeFee!.value = worker.fees[i].value;
                    if (worker.fees[i].feeIncludedFeatures[j].id == 1) {
                      _workerProfileData.houeFeeMap![tr('include_food')] = true;
                    } else if (worker.fees[i].feeIncludedFeatures[j].id == 2) {
                      _workerProfileData
                          .houeFeeMap![tr('includes_transportation')] = true;
                    }
                  } else if (worker.fees[i].feeType == 2) {
                    _workerProfileData.dayFee!.value = worker.fees[i].value;
                    if (worker.fees[i].feeIncludedFeatures[j].id == 1) {
                      _workerProfileData.dayFeeMap![tr('include_food')] = true;
                    } else if (worker.fees[i].feeIncludedFeatures[j].id == 2) {
                      _workerProfileData
                          .dayFeeMap![tr('includes_transportation')] = true;
                    }
                  } else if (worker.fees[i].feeType == 3) {
                    _workerProfileData.monthFee!.value = worker.fees[i].value;
                    if (worker.fees[i].feeIncludedFeatures[j].id == 1) {
                      _workerProfileData.monthFeeMap![tr('include_food')] =
                          true;
                    } else if (worker.fees[i].feeIncludedFeatures[j].id == 2) {
                      _workerProfileData
                          .monthFeeMap![tr('includes_transportation')] = true;
                    } else if (worker.fees[i].feeIncludedFeatures[j].id == 4) {
                      _workerProfileData.monthFeeMap![tr('includes_housing')] =
                          true;
                    }
                  }
                }
              }
            }
            await model.getNationalities();
            await model.getCities(profile);

            if (widget.isUpdate) {
              for (int i = 0; i < model.workerProfessions.length; i++) {
                if (model.workerProfessions[i].professionName ==
                    model.worker!.professionName) {
                  _workerProfileData.professionId =
                      model.workerProfessions[i].professionId;
                }
              }

              for (int i = 0; i < model.cityList.length; i++) {
                if (model.cityList[i].name == model.worker!.worksInCityName) {
                  model.cityIdSet = model.cityList[i].id;
                }
              }
            }
          },
          builder: (context, model, child) => model.isBusy
              ? Center(
                  child: CustomLoader(),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomAppBar(
                      backOnTab: () {
                        _workerProfileData.clearDate();
                        model.navigationService.back();
                      },
                      saveOnTab: () {
                        int houeFeeTotal = 0;
                        int dayFeeTotal = 0;
                        int monthFeeTotal = 0;

                        for (int i = 0;
                            i < _workerProfileData.houeFee!.feeList.length;
                            i++) {
                          houeFeeTotal +=
                              _workerProfileData.houeFee!.feeList[i].value;
                        }

                        for (int i = 0;
                            i < _workerProfileData.dayFee!.feeList.length;
                            i++) {
                          dayFeeTotal +=
                              _workerProfileData.dayFee!.feeList[i].value;
                        }
                        for (int i = 0;
                            i < _workerProfileData.monthFee!.feeList.length;
                            i++) {
                          monthFeeTotal +=
                              _workerProfileData.monthFee!.feeList[i].value;
                        }

                        _workerProfileData.houeFee!.feeIncludedTypes =
                            houeFeeTotal;
                        _workerProfileData.dayFee!.feeIncludedTypes =
                            dayFeeTotal;
                        _workerProfileData.monthFee!.feeIncludedTypes =
                            monthFeeTotal;

                        _workerProfileData.houeFee!.feeType = 1;
                        _workerProfileData.dayFee!.feeType = 2;
                        _workerProfileData.monthFee!.feeType = 3;

                        _workerProfileData.houeFee!.workerFeeId = 0;
                        _workerProfileData.dayFee!.workerFeeId =
                            widget.isUpdate ? 1 : 0;
                        _workerProfileData.monthFee!.workerFeeId =
                            widget.isUpdate ? 2 : 0;

                        List<WorkerFeeDto> workFees = [];
                        workFees.add(_workerProfileData.houeFee!);
                        workFees.add(_workerProfileData.dayFee!);
                        workFees.add(_workerProfileData.monthFee!);

                        if (!widget.isUpdate && model.avatarImgage == null) {
                          CustomToasts.showMessage(
                              message: 'الرجاء اضافة الصورة الشخصية',
                              messageType: MessageType.alertMessage);
                        } else if (_workerProfileData.professionId == null) {
                          CustomToasts.showMessage(
                              message: 'الرجاء اختيار نوع الخدمة',
                              messageType: MessageType.alertMessage);
                        } else if (_workerProfileData.houeFee!.value == null) {
                          CustomToasts.showMessage(
                              message: 'الرجاء إدخال الأجر بالساعة',
                              messageType: MessageType.alertMessage);
                        } else if (_workerProfileData.dayFee!.value == null) {
                          CustomToasts.showMessage(
                              message: 'الرجاء إدخال الأجر اليومي',
                              messageType: MessageType.alertMessage);
                        } else if (_workerProfileData.monthFee!.value == null) {
                          CustomToasts.showMessage(
                              message: 'الرجاء إدخال الأجر الشهري',
                              messageType: MessageType.alertMessage);
                        } else {
                          model.createUpdateWorker(
                              description: '',
                              workerFees: jsonEncode(workFees),
                              nationalityId: model.natId.toString(),
                              professionId:
                                  _workerProfileData.professionId.toString());
                        }
                      },
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: size.width / 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 16,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: AspectRatio(
                                      aspectRatio: 1,
                                      child: CircularImageNetwork(
                                        imgFit: BoxFit.fill,
                                        imageOnTab: () {
                                          IosBottomSheet.show(
                                              context: context,
                                              bottomSheetWidgets: [
                                                BottomSheetIosWidget(
                                                  title: tr('camera'),
                                                  textColor:
                                                      AppColors.blueColor,
                                                  onPressed: () {
                                                    model.changeAvatarPicture(
                                                        ImageSource.camera);
                                                  },
                                                ),
                                                BottomSheetIosWidget(
                                                  title: tr('gallery'),
                                                  textColor:
                                                      AppColors.blueColor,
                                                  onPressed: () {
                                                    model.changeAvatarPicture(
                                                        ImageSource.gallery);
                                                  },
                                                ),
                                              ]);
                                        },
                                        imageFile: model.avatarImgage,
                                        isSquare: true,
                                        imagePath: widget.isUpdate
                                            ? model.worker!.pictureUrl
                                            : '',
                                        // imageErrorBackroundColor: AppColors.grey100Color,
                                        // imageErrorColor: AppColors.grey200Color,
                                        placeHolderSize: size.width / 12,
                                        placeHolderSvgPath:
                                            'assets/icons/photo_place_holder.svg',
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 4),
                                          child: Text(
                                            widget.isUpdate
                                                ? tr('membership_number')
                                                : '',
                                            style: TextStyle(
                                                color: AppColors.whiteColor,
                                                fontWeight: FontWeight.w500,
                                                fontSize:
                                                    size.shortestSide / 30),
                                          ),
                                        ),
                                        Text(
                                          widget.isUpdate
                                              ? model.worker!.workerId
                                                  .toString()
                                              : '',
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontWeight: FontWeight.w500,
                                              fontSize: size.shortestSide / 30),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 12,
                              ),
                              Text(
                                profile.fullName!,
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: size.shortestSide / 24),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                profile.city!,
                                style: TextStyle(
                                    color: AppColors.grey200Color,
                                    fontWeight: FontWeight.w500,
                                    fontSize: size.shortestSide / 30),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return NationalitiesDropDown(
                                          nationalityList: model.natList,
                                          onChanged: (natId, natName) {
                                            locator<NavigationService>().back();
                                            setState(() {
                                              model.natIdSet = natId;
                                              model.selectedNatSet = natName;
                                            });
                                          },
                                        );
                                      });
                                },
                                child: Text(
                                  widget.isUpdate
                                      ? model.worker!.nationality ?? ""
                                      : model.selectedNat ?? tr('nationalty'),
                                  style: TextStyle(
                                      color: AppColors.grey200Color,
                                      fontWeight: FontWeight.w500,
                                      fontSize: size.shortestSide / 30),
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Row(
                                children: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      _tabController.animateTo(0);
                                    },
                                    child: Text(
                                      tr('my_services'),
                                      style: TextStyle(
                                          color: _tabControllerIndex == 0
                                              ? AppColors.yellowColor
                                              : AppColors.grey200Color,
                                          fontWeight: FontWeight.w400,
                                          fontSize: size.shortestSide / 26),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      _tabController.animateTo(1);
                                    },
                                    child: Text(
                                      tr('contacts_info'),
                                      style: TextStyle(
                                          color: _tabControllerIndex == 1
                                              ? AppColors.yellowColor
                                              : AppColors.grey200Color,
                                          fontWeight: FontWeight.w400,
                                          fontSize: size.shortestSide / 26),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                height: size.height / 2,
                                child: TabBarView(
                                    controller: _tabController,
                                    children: [
                                      WorkerServicesTab(
                                        workerProfessions:
                                            model.workerProfessions,
                                      ),
                                      WorkerInfoTab(isUpdate: widget.isUpdate),
                                    ]),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
        ));
  }
}
