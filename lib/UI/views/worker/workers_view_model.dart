import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/city.dart';
import 'package:exaaz/core/data/models/worker.dart';
import 'package:exaaz/core/data/models/worker_professional.dart';
import 'package:exaaz/core/data/models/worker_profile_data.dart';
import 'package:exaaz/core/data/repository/address_repository.dart';
import 'package:exaaz/core/data/repository/service_provider_repository.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';
import 'package:exaaz/core/data/repository/worker_repository.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart' show NavigationService;

import '../../../core/data/models/profile_info.dart';

class WorkersViewModel extends BaseViewModel {
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  WorkersRepository workersRepository = locator<WorkersRepository>();
  AddressRepository addressRepository = locator<AddressRepository>();
  NavigationService navigationService = locator<NavigationService>();
  ServiceProviderRepository serviceProviderRepository =
      locator<ServiceProviderRepository>();

  List<City> _cityList = [];
  List<City> get cityList => _cityList;

  int _cityId = 0;
  int get cityId => _cityId;
  set cityIdSet(int value) {
    _cityId = value;
  }

  String? _selectedNat;
  String? get selectedNat => _selectedNat;
  set selectedNatSet(String value) {
    _selectedNat = value;
  }

  int _natId = 0;
  int get natId => _natId;
  set natIdSet(int value) {
    _natId = value;
  }

  WorkerModel? _worker;
  WorkerModel? get worker => _worker;
  set setWorker(WorkerModel value) {
    _worker = value;
  }

  List<WorkerProfession> _workerProfessions = [];
  List<WorkerProfession> get workerProfessions => _workerProfessions;

  List<City> _natList = [];
  List<City> get natList => _natList;

  List<WorkerModel> _workerList = [];
  List<WorkerModel> get workerList => _workerList;

  int _selectedWorkerProfessionId = -1;
  int get selectedWorkerProfessionId => _selectedWorkerProfessionId;

  set setSelectedWorkerProfessionId(int value) {
    _selectedWorkerProfessionId = value;
  }

  PickedFile? _avatarImgage;
  PickedFile? get avatarImgage => _avatarImgage;

  changeAvatarPicture(ImageSource image) async {
    final picker = ImagePicker();

    var value = await picker.getImage(source: image);
    if (value != null) {
      _avatarImgage = value;
    }

    notifyListeners();
  }

  Future<void> getWorkerProfession() async {
    setBusy(true);
    try {
      final commonResponse = await workersRepository.getWorkerProfessions();
      if (commonResponse.getStatus) {
        setBusy(false);

        Iterable list = commonResponse.getData;
        _workerProfessions =
            list.map((model) => WorkerProfession.fromJson(model)).toList();

        if (_workerProfessions.length > 0) {
          setSelectedWorkerProfessionId = _workerProfessions[0].professionId;
        }
        runGetWorkersList(selectedWorkerProfessionId);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  Future<void> getNationalities() async {
    setBusy(true);
    try {
      final commonResponse = await addressRepository.getNationalities();
      if (commonResponse.getStatus) {
        Iterable list = commonResponse.getData;
        _natList = list.map((model) => City.fromJson(model)).toList();
        if (worker != null) {
          for (int i = 0; i < _natList.length; i++) {
            if (_natList[i].name == worker!.nationality) {
              natIdSet = _natList[i].id;
            }
          }
        }
        getWorkerProfession();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void runGetWorkersList(int professionId) {
    runBusyFuture(getWorkersRequest(professionId: professionId),
        busyObject: workerList);
  }

  Future<void> getWorkersRequest({required int professionId}) async {
    try {
      final commonResponse = await workersRepository.getWorkers(
          pageIndex: 0, professionId: professionId);
      if (commonResponse.getStatus) {
        Iterable list = commonResponse.getData;
        _workerList = list.map((model) => WorkerModel.fromJson(model)).toList();
      } else {
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  Future<void> getCities(ProfileInfo info) async {
    try {
      final commonResponse = await addressRepository.getCities();
      if (commonResponse.getStatus) {
        setBusy(false);

        Iterable list = commonResponse.getData;
        _cityList = list.map((model) => City.fromJson(model)).toList();

        for (int i = 0; i < _cityList.length; i++) {
          if (_cityList[i].name == info.city) {
            cityIdSet = _cityList[i].id;
          }
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  Future<void> createUpdateWorker({
    required String description,
    required String workerFees,
    required String nationalityId,
    required String professionId,
  }) async {
    try {
      setBusy(true);
      final commonResponse = await workersRepository.createUpdateWorker(
          cityId: _cityId.toString(),
          description: description,
          nationalityId: nationalityId,
          picture: _avatarImgage?.path ?? '',
          professionId: professionId,
          // residencePicture: _residenceImgage.path ?? '',
          workerFees: workerFees);
      if (commonResponse.getStatus) {
        //   _workerProfileData.clearDate();
        CustomToasts.showMessage(
            message: 'تم حفظ التغيرات بنجاح',
            messageType: MessageType.successMessage);
        locator<WorkerProfileData>().clearDate();

        navigationService.replaceWith(Routes.homeView);
        setBusy(false);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message:
                'Api Error: ' + commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  Future<void> addToFavorite({
    required int serviceProviderId,
  }) async {
    try {
      final commonResponse = await serviceProviderRepository.addToFavorite(
        serviceProviderId: serviceProviderId,
        serviceProviderType: 0,
      );
      if (commonResponse.getStatus) {
        CustomToasts.showMessage(
            message: 'تمت الإضافة الى المفضلة بنجاح',
            messageType: MessageType.successMessage);
      } else {
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.errorMessage);
      print(e);
    }
  }

  Future<void> removeFromFavorite({
    required int serviceProviderId,
  }) async {
    try {
      final commonResponse = await serviceProviderRepository.removeFromFavorite(
        serviceProviderId: serviceProviderId,
        serviceProviderType: 0,
      );
      if (commonResponse.getStatus) {
        CustomToasts.showMessage(
            message: 'تمت الإزالة من المفضلة بنجاح',
            messageType: MessageType.successMessage);
      } else {
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.errorMessage);
      print(e);
    }
  }

  Future<void> getMyDetails() async {
    setBusy(true);
    try {
      final commonResponse = await workersRepository.getMyDetails();
      if (commonResponse.getStatus) {
        setWorker = WorkerModel.fromJson(commonResponse.data);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
