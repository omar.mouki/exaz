import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/models/profile_info.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';

import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_image_network.dart';

import 'side_menu_widgets/side_menu_item_card.dart';

class SideMenuView extends StatefulWidget {
  @override
  _SideMenuViewState createState() => _SideMenuViewState();
}

class _SideMenuViewState extends State<SideMenuView> {
  ProfileInfo profile =
      locator<SharedPreferencesRepository>().getProfileInfo()!;
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  final _navigationService = locator<NavigationService>();

  String date = "";

  @override
  void initState() {
    profile = sharedPreferencesRepository.getProfileInfo()!;
    if (profile.accountCreatedAt != null) {
      var arr = profile.accountCreatedAt!.split('T');
      date = arr[0];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            'assets/pngs/choose_service_bg.png',
            fit: BoxFit.fill,
            width: size.width,
            height: size.height,
          ),
          ListView(
              padding: EdgeInsets.only(
                top: size.height / 7,
              ),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: size.width / 22,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: AspectRatio(
                              aspectRatio: 0.99,
                              child: CircularImageNetwork(
                                  isSquare: true,
                                  imagePath: profile.picture ?? '',
                                  // imageErrorBackroundColor:
                                  //     AppColors.grey100Color,
                                  // imageErrorColor: AppColors.grey200Color,
                                  placeHolderSize: size.width / 12,
                                  placeHolderSvgPath:
                                      'assets/icons/navbar_profile.svg'),
                            ),
                          ),
                          const SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                FlatButton(
                                  minWidth: size.width / 2.2,
                                  color: AppColors.blackColor,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: size.width / 24),
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(size.width / 20),
                                  ),
                                  onPressed: () {
                                    locator<NavigationService>()
                                        .navigateTo(Routes.editProfileView);
                                  },
                                  child: Text(tr('edit_profile'),
                                      style: TextStyle(
                                          color: AppColors.whiteColor,
                                          fontSize: size.shortestSide / 26,
                                          fontWeight: FontWeight.bold)),
                                ),
                                if (!profile.isRequestedToBeServiceProvider &&
                                    !profile.isServiceProvider)
                                  FlatButton(
                                    minWidth: size.width / 2.2,
                                    color: AppColors.blackColor,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: size.width / 24),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          size.width / 20),
                                    ),
                                    onPressed: () {
                                      locator<NavigationService>().navigateTo(
                                          Routes.chooseActivityView);
                                    },
                                    child: Text(tr('your_sp_profile'),
                                        style: TextStyle(
                                            color: AppColors.whiteColor,
                                            fontSize: size.shortestSide / 26,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                if (profile.isServiceProvider)
                                  FlatButton(
                                    minWidth: size.width / 2.2,
                                    color: AppColors.blackColor,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: size.width / 24),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          size.width / 20),
                                    ),
                                    onPressed: () {
                                      locator<NavigationService>().navigateTo(
                                          Routes.workerProfileView,
                                          arguments: WorkerProfileViewArguments(
                                              isUpdate: true));
                                    },
                                    child: Text('تعديل حسابي كمزود خدمة',
                                        style: TextStyle(
                                            color: AppColors.whiteColor,
                                            fontSize: size.shortestSide / 26,
                                            fontWeight: FontWeight.bold)),
                                  ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(profile.fullName ?? '',
                          style: TextStyle(
                              color: AppColors.blackColor,
                              fontSize: size.shortestSide / 26,
                              fontWeight: FontWeight.bold)),
                      const SizedBox(
                        height: 8,
                      ),
                      Text('',
                          style: TextStyle(
                              color: AppColors.grey,
                              fontSize: size.shortestSide / 30,
                              fontWeight: FontWeight.normal)),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(' عضو منذ ' + date,
                          style: TextStyle(
                              color: AppColors.grey500Color,
                              fontSize: size.shortestSide / 30,
                              fontWeight: FontWeight.normal)),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 24,
                ),
                SideMenuItemCard(
                  title: tr('favorites'),
                ),
                SideMenuItemCard(
                  title: tr('my_projects'),
                  onTab: () => locator<NavigationService>()
                      .navigateTo(Routes.myProjectsListView),
                ),
                SideMenuItemCard(
                  title: tr('share_app'),
                ),
                SideMenuItemCard(
                  title: tr('call_us'),
                ),
                SideMenuItemCard(
                  title: tr('privacy_use_policy'),
                ),
                SideMenuItemCard(
                  title: tr('rate_app'),
                ),
                SideMenuItemCard(
                  title: tr('choose_app_language'),
                ),
                SideMenuItemCard(
                  onTab: () {
                    sharedPreferencesRepository.logout();
                    _navigationService.clearStackAndShow(Routes.splashView);
                  },
                  title: tr('logout'),
                ),
              ]),
        ],
      ),
    );
  }
}
