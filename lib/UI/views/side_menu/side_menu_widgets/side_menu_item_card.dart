import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SideMenuItemCard extends StatelessWidget {
  final String title;
  final Function()? onTab;

  const SideMenuItemCard({Key? key, required this.title, this.onTab})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTab,
      child: Container(
        margin: const EdgeInsets.only(bottom: 12),
        padding:
            EdgeInsets.symmetric(horizontal: size.width / 22, vertical: 20),
        decoration: BoxDecoration(
          color: AppColors.whiteColor,
          boxShadow: [
            BoxShadow(
              color: AppColors.blackColor.withOpacity(0.2),
              offset: const Offset(0, 7),
              blurRadius: 4,
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                  color: AppColors.blackColor,
                  fontSize: size.width / 24,
                  fontWeight: FontWeight.bold),
            ),
            SvgPicture.asset('assets/icons/arrow_back.svg',
                color: AppColors.blackColor, width: size.width / 50),
          ],
        ),
      ),
    );
  }
}
