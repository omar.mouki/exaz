import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

import 'package:flutter_swiper_tv/flutter_swiper.dart';

import '../shared/custom_widgets/custom_image_network.dart';
import '../shared/custom_widgets/share_appbar.dart';

class ProductView extends StatefulWidget {
  const ProductView({Key? key}) : super(key: key);

  @override
  _ProductViewState createState() => _ProductViewState();
}

class _ProductViewState extends State<ProductView> {
  late SwiperController _controller;

  @override
  void initState() {
    super.initState();
    _controller = SwiperController();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.blackColor,
      body: Column(
        children: <Widget>[
          ShareAppBar(
            backOnTab: () {},
            title: 'سيراميك بيج',
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: size.width / 32),
                child: Swiper(
                  loop: false,
                  itemCount: 3,
                  viewportFraction: 1,
                  itemBuilder: (context, index) {
                    return CircularImageNetwork(
                      imagePath: 'https://picsum.photos/100',
                      imageSize: size.shortestSide * 0.25,
                    );
                  },
                  outer: true,
                  pagination: SwiperPagination(
                      margin: EdgeInsets.symmetric(
                          vertical: size.longestSide / 128),
                      builder: RectSwiperPaginationBuilder(
                          size: Size(14, 14),
                          activeSize: Size(14, 14),
                          color: AppColors.darkGreyColor,
                          activeColor: AppColors.yellowColor)),
                  controller: _controller,
                ),
              )),
          SizedBox(
            height: 10,
          ),
          Expanded(
              flex: 2,
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: size.width / 32),
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        tr('product_code'),
                        style: TextStyle(
                            color: AppColors.yellowColor,
                            fontWeight: FontWeight.w500,
                            fontSize: size.shortestSide / 24),
                      ),
                      SizedBox(
                        width: size.width / 8,
                      ),
                      Text(
                        'I3001',
                        style: TextStyle(
                            color: AppColors.grey200Color,
                            fontWeight: FontWeight.w500,
                            fontSize: size.shortestSide / 24),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Text(
                    tr('product_bio'),
                    style: TextStyle(
                        color: AppColors.yellowColor,
                        fontWeight: FontWeight.w500,
                        fontSize: size.shortestSide / 24),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    'مؤسسة قصر اليمان لبيع وتوزيع منتجات الرخام والسيراميك وغيرها من النتجات محلية الصنع والمستورد',
                    style: TextStyle(
                        color: AppColors.whiteColor,
                        fontWeight: FontWeight.w400,
                        fontSize: size.shortestSide / 24),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        tr('product_details'),
                        style: TextStyle(
                            color: AppColors.yellowColor,
                            fontWeight: FontWeight.w500,
                            fontSize: size.shortestSide / 24),
                      ),
                      SizedBox(
                        width: size.width / 8,
                      ),
                      Text(
                        'I3001',
                        style: TextStyle(
                            color: AppColors.grey200Color,
                            fontWeight: FontWeight.w500,
                            fontSize: size.shortestSide / 24),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        tr('price'),
                        style: TextStyle(
                            color: AppColors.yellowColor,
                            fontWeight: FontWeight.w500,
                            fontSize: size.shortestSide / 24),
                      ),
                      SizedBox(
                        width: size.width / 8,
                      ),
                      Text(
                        'I3001',
                        style: TextStyle(
                            color: AppColors.grey200Color,
                            fontWeight: FontWeight.w500,
                            fontSize: size.shortestSide / 24),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
