import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/UI/shared/custom_widgets/text_field_widget.dart';
import 'package:exaaz/UI/views/register_sp/register_sp_view_model.dart';
import 'package:exaaz/UI/views/worker/worker_widgets/worker_residence_tab.dart';
import 'package:flutter/material.dart';

class RegesterSpWorkerWidget extends StatefulWidget {
  final Size size;
  final RegisterSpViewModel model;

  RegesterSpWorkerWidget({Key? key, required this.size, required this.model})
      : super(key: key);

  @override
  _RegesterSpWorkerWidgetState createState() => _RegesterSpWorkerWidgetState();
}

class _RegesterSpWorkerWidgetState extends State<RegesterSpWorkerWidget> {
  bool _isApproved = false;

  TextEditingController _nameController = TextEditingController();

  TextEditingController _numberController = TextEditingController();

  FocusNode _nameFocusNode = FocusNode();

  FocusNode _numberFocusNode = FocusNode();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
// from api
  final typeId = 1;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SizedBox(
        width: widget.size.width,
        height: widget.size.height,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: widget.size.height / 16,
            horizontal: widget.size.width / 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    tr('sign_in_as_sp'),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: widget.size.shortestSide / 24),
                  ),
                  Builder(builder: (context) {
                    return IconButton(
                        padding: EdgeInsets.zero,
                        alignment: AlignmentDirectional.centerEnd,
                        icon: const Icon(Icons.menu),
                        onPressed: () => Scaffold.of(context).openDrawer());
                  }),
                ],
              ),
              const SizedBox(
                height: 12,
              ),
              Expanded(
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFieldWidget(
                        textColor: AppColors.blackColor,
                        textEditingController: _nameController,
                        fieldFocusNode: _nameFocusNode,
                        underlineColor: AppColors.grey300Color,
                        underlineWidth: 2,
                        title: tr('institution_name_individual'),
                        isDense: true,
                        onValidate: (text) => text != null && text.isNotEmpty
                            ? null
                            : tr('required'),
                      ),
                      TextFieldWidget(
                        textColor: AppColors.blackColor,
                        textEditingController: _numberController,
                        fieldFocusNode: _numberFocusNode,
                        underlineColor: AppColors.grey300Color,
                        underlineWidth: 2,
                        title: tr('commercial_registration_number_residence'),
                        isDense: true,
                        onValidate: (text) => text != null && text.isNotEmpty
                            ? null
                            : tr('required'),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      WorkerResidenceTab(
                        model: widget.model,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: <Widget>[
                          Checkbox(
                            value: _isApproved,
                            onChanged: (bool) {
                              setState(() {
                                _isApproved = !_isApproved;
                              });
                            },
                            activeColor: AppColors.yellowColor,
                            visualDensity: VisualDensity.compact,
                          ),
                          Text(
                            tr('agree_terms_conditions'),
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: widget.size.shortestSide / 40),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: FlatButton(
                          color: AppColors.blackColor,
                          padding: EdgeInsets.symmetric(
                              horizontal: widget.size.width / 5),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              if (_isApproved) {
                                if (widget.model.residenceImgage == null) {
                                  CustomToasts.showMessage(
                                      message: 'الرجاء اضافة صورة الإقامة',
                                      messageType: MessageType.alertMessage);
                                } else {
                                  widget.model.registerAsSp(
                                      companyTitle: _nameController.text,
                                      fullName: _nameController.text,
                                      commercialRegistrationNumber:
                                          _numberController.text,
                                      residenceNumber: _numberController.text,
                                      requestedServiceType: typeId);

                                  _formKey.currentState!.reset();
                                }
                              } else {
                                CustomToasts.showMessage(
                                    message: tr('check_terms'),
                                    messageType: MessageType.alertMessage);
                              }
                            }
                          },
                          child: Text(tr('make_request'),
                              style: TextStyle(
                                  color: AppColors.whiteColor,
                                  fontSize: widget.size.shortestSide / 26,
                                  fontWeight: FontWeight.w400)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
