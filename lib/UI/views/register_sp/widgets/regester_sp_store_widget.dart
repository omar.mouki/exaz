import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/UI/shared/custom_widgets/text_field_widget.dart';
import 'package:exaaz/UI/views/register_sp/register_sp_view_model.dart';
import 'package:exaaz/core/data/models/category.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class RegesterSpStoreWidget extends StatefulWidget {
  final Size size;
  final RegisterSpViewModel model;
  RegesterSpStoreWidget({Key? key, required this.size, required this.model})
      : super(key: key);

  @override
  _RegesterSpStoreWidgetState createState() => _RegesterSpStoreWidgetState();
}

class _RegesterSpStoreWidgetState extends State<RegesterSpStoreWidget> {
  bool _isApproved = false;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _activityController = TextEditingController();

  final TextEditingController _numberController = TextEditingController();

  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _activityFocusNode = FocusNode();

  final FocusNode _numberFocusNode = FocusNode();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
// store is 2
  final typeId = 2;
  Category? _category;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.size.width / 20),
      child: SingleChildScrollView(
        child: SizedBox(
            height: widget.size.height,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    height: widget.size.height / 16,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        tr('sign_in_as_sp'),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: widget.size.shortestSide / 24),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextFieldWidget(
                              textColor: AppColors.blackColor,
                              textEditingController: _nameController,
                              fieldFocusNode: _nameFocusNode,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              title: tr('institution_name_individual'),
                              isDense: true,
                              onValidate: (text) =>
                                  text != null && text.isNotEmpty
                                      ? null
                                      : tr('required'),
                            ),
                            DropdownButton<Category>(
                              isExpanded: true,
                              value: _category,
                              onChanged: (newVal) {
                                setState(() {
                                  _category = newVal;
                                });
                              },
                              items: widget.model.activityList,
                              hint: Text(tr('النشاط/الخدمة')),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFieldWidget(
                              isReadOnly: true,
                              onTap: pickFile,
                              textColor: AppColors.blackColor,
                              textEditingController: _numberController,
                              fieldFocusNode: _numberFocusNode,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              title: tr(
                                  'commercial_registration_number_residence'),
                              isDense: true,
                              onValidate: (text) =>
                                  text != null && text.isNotEmpty
                                      ? null
                                      : tr('required'),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: <Widget>[
                                Checkbox(
                                  value: _isApproved,
                                  onChanged: (bool) {
                                    setState(() {
                                      _isApproved = !_isApproved;
                                    });
                                  },
                                  activeColor: AppColors.yellowColor,
                                  visualDensity: VisualDensity.compact,
                                ),
                                Text(
                                  tr('agree_terms_conditions'),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: widget.size.shortestSide / 40),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  Center(
                    child: FlatButton(
                      color: AppColors.blackColor,
                      padding: EdgeInsets.symmetric(
                          horizontal: widget.size.width / 5),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          if (_isApproved) {
                            if (_category != null) {
                              widget.model.requestToBeStore(
                                name: _nameController.text,
                                commercialRecord: _numberController.text,
                                categoryId: _category!.id,
                              );
                              _formKey.currentState!.reset();
                            } else {
                              CustomToasts.showMessage(
                                  message:
                                      tr('الرجاء اختيار نوع النشاط/الخدمة'),
                                  messageType: MessageType.alertMessage);
                            }
                          } else {
                            CustomToasts.showMessage(
                                message: tr('check_terms'),
                                messageType: MessageType.alertMessage);
                          }
                        }
                      },
                      child: Text(tr('make_request'),
                          style: TextStyle(
                              color: AppColors.whiteColor,
                              fontSize: widget.size.shortestSide / 26,
                              fontWeight: FontWeight.w400)),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  )
                ])),
      ),
    );
  }

  void pickFile() async {
    final storagePermission = Permission.storage;
    final status = await storagePermission.request();
    if (status.isGranted) {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: ['pdf'],
      );

      if (result != null) {
        _numberController.text = result.files.single.path!;

        File file = File(result.files.single.path!);
      } else {
        // User canceled the picker
      }
    } else {
      CustomToasts.showMessage(
          message: 'يرجى اعطاء صلاحيات الوصول',
          messageType: MessageType.alertMessage);
    }
  }
}
