import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/core/data/models/category.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../core/data/repository/change_request_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../../app/locator.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class RegisterSpViewModel extends BaseViewModel {
  ChangeRequestRepository _changeRequestRepository =
      locator<ChangeRequestRepository>();
  final _storeRepository = locator<StoreRepository>();
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  NavigationService _navigationService = locator<NavigationService>();

  List<Category> _catigoryList = [];
  List<Category> get catigoryList => _catigoryList;

  List<DropdownMenuItem<Category>> _activityList = [];
  List<DropdownMenuItem<Category>> get activityList => _activityList;

  // List<DropdownMenuItem<Category>> _activityList = [];
  // List<DropdownMenuItem<Category>> get activityList => _activityList;

  PickedFile? _residenceImgage;
  PickedFile? get residenceImgage => _residenceImgage;

  changeresidencePicture(ImageSource image) async {
    final picker = ImagePicker();

    var value = await picker.getImage(source: image);
    if (value != null) {
      _residenceImgage = value;
    }

    notifyListeners();
  }

  Future<void> getStoreActivities() async {
    try {
      setBusy(true);
      final commonResponse = await _storeRepository.getCategories();
      if (commonResponse.getStatus) {
        setBusy(false);
        Iterable list = commonResponse.getData;
        _catigoryList = list.map((model) => Category.fromJson(model)).toList();

        for (int i = 0; i < _catigoryList.length; i++) {
          _activityList.add(DropdownMenuItem(
            value: _catigoryList[i],
            child: Text(_catigoryList[i].name),
          ));
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void registerAsSp(
      {required int requestedServiceType,
      required String companyTitle,
      required String fullName,
      required String commercialRegistrationNumber,
      required String residenceNumber}) async {
    try {
      setBusy(true);
      final commonResponse =
          await _changeRequestRepository.requestToBeServiceProvider(
              residencePicture: _residenceImgage?.path ?? '',
              commercialRegistrationNumber: commercialRegistrationNumber,
              companyTitle: companyTitle,
              fullName: fullName,
              requestedServiceType: requestedServiceType,
              residenceNumber: residenceNumber);
      if (commonResponse.getStatus) {
        setBusy(false);
        _navigationService.popRepeated(2);
        CustomToasts.showMessage(
            message: tr('sp_register_succ'),
            messageType: MessageType.successMessage);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void requestToBeStore({
    required String name,
    required String commercialRecord,
    required int categoryId,
  }) async {
    try {
      setBusy(true);
      final commonResponse = await _storeRepository.requestToBeStore(
          name: name,
          commercialRecord: commercialRecord,
          categoryId: categoryId);
      if (commonResponse.getStatus) {
        setBusy(false);
        _navigationService.popRepeated(2);
        CustomToasts.showMessage(
            message: tr('sp_register_succ'),
            messageType: MessageType.successMessage);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
