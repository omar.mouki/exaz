import 'package:exaaz/UI/views/register_sp/widgets/regester_sp_contractor_widget.dart';
import 'package:exaaz/UI/views/register_sp/widgets/regester_sp_house_keeping_widget.dart';
import 'package:exaaz/UI/views/register_sp/widgets/regester_sp_store_widget.dart';
import 'package:exaaz/UI/views/register_sp/widgets/regester_sp_worker_widget.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../shared/custom_widgets/custom_lodaer.dart';
import 'register_sp_view_model.dart';

/*
اذا كان الاي دي ٢ 
اظهر واجهة الستور
*/
class RegisterSpView extends StatefulWidget {
  final int typeId;
  const RegisterSpView({Key? key, required this.typeId}) : super(key: key);

  @override
  _RegisterSpViewState createState() => _RegisterSpViewState();
}

class _RegisterSpViewState extends State<RegisterSpView> {
  // ServiceProviderType _type;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      //drawer: CustomDrawer(),
      body: ViewModelBuilder<RegisterSpViewModel>.reactive(
        viewModelBuilder: () => RegisterSpViewModel(),
        builder: (context, model, child) => model.isBusy
            ? Center(
                child: CustomLoader(),
              )
            : Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/pngs/choose_service_bg.png',
                    fit: BoxFit.fill,
                    width: size.width,
                    height: size.height,
                  ),
                  (() {
                    Widget child = Container();
                    switch (widget.typeId) {
                      // 0 worker
                      case 0:
                        child = RegesterSpWorkerWidget(
                          size: size,
                          model: model,
                        );

                        break;

                      /// 1 contractor
                      case 1:
                        child = RegesterSpContractorWidget();

                        break;
                      // 2 store
                      case 2:
                        child = RegesterSpStoreWidget(
                          size: size,
                          model: model,
                        );

                        break;
                      // 3 house keeping
                      case 3:
                        child = RegesterSpHouseKeepingWidget();

                        break;
                      default:
                        Container();
                    }
                    return child;
                  }()),
                ],
              ),
        onModelReady: (model) async {
          if (widget.typeId == 2) {
            await model.getStoreActivities();
          }
        },
      ),
    );
  }
}
