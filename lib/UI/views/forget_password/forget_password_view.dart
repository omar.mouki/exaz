import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';

import '../../shared/custom_widgets/text_field_widget.dart';
import 'forget_password_view_model.dart';

class ForgetPasswordView extends StatefulWidget {
  ForgetPasswordView({Key? key}) : super(key: key);

  @override
  _ForgetPasswordViewState createState() => _ForgetPasswordViewState();
}

class _ForgetPasswordViewState extends State<ForgetPasswordView> {
  TextEditingController _phonecontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: ViewModelBuilder<ForgetPasswordViewModel>.reactive(
      viewModelBuilder: () => ForgetPasswordViewModel(),
      builder: (context, model, child) => model.isBusy
          ? const Center(child: CircularProgressIndicator())
          : Stack(
              children: <Widget>[
                Image.asset(
                  'assets/pngs/choose_service_bg.png',
                  fit: BoxFit.fill,
                  width: size.width,
                  height: size.height,
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: size.shortestSide / 16,
                      left: size.shortestSide / 16,
                      right: size.shortestSide / 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFieldWidget(
                        textEditingController: _phonecontroller,
                        title: tr('phone_number'),
                        isDense: true,
                        underlineColor: AppColors.grey300Color,
                        underlineWidth: 2,
                        textColor: AppColors.blackColor,
                        keyboardType: TextInputType.phone,
                        maxLength: 10,
                      ),
                      SizedBox(
                        height: size.height / 20,
                      ),
                      FlatButton(
                        color: AppColors.blackColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        onPressed: () {
                          model.forgetPassword(
                              phoneNumber: _phonecontroller.text);
                        },
                        child: SizedBox(
                          width: size.width / 1.2,
                          child: Center(
                            child: Text(tr('send_code'),
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontSize: size.shortestSide / 21,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
    ));
  }
}
