import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:stacked/stacked.dart';

import 'forget_password_view_model.dart';

class VerifyCodeView extends StatelessWidget {
  final String phoneNumber;

  const VerifyCodeView({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: ViewModelBuilder<ForgetPasswordViewModel>.reactive(
        viewModelBuilder: () => ForgetPasswordViewModel(),
        builder: (context, model, child) => model.isBusy
            ? const Center(child: CircularProgressIndicator())
            : Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/pngs/choose_service_bg.png',
                    fit: BoxFit.fill,
                    width: size.width,
                    height: size.height,
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: size.shortestSide / 16,
                        left: size.shortestSide / 16,
                        right: size.shortestSide / 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        PinCodeTextField(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: size.shortestSide / 20),
                          length: 4,
                          onCompleted: (code) {
                            model.verifyPhoneNumber(
                                phoneNumber: phoneNumber, code: code);
                          },

                          // activeFillColor: Colors.red,
                          backgroundColor: Colors.transparent,
                          // textInputType: TextInputType.phone,
                          // inputFormatters: [
                          //   FilteringTextInputFormatter.digitsOnly
                          // ],
                          // activeColor: AppColors.yellowColor,
                          // selectedColor: AppColors.grey200Color,
                          // inactiveColor: AppColors.grey300Color,
                          animationType: AnimationType.fade,
                          // shape: PinCodeFieldShape.underline,
                          animationDuration: Duration(milliseconds: 300),
                          // borderRadius: BorderRadius.circular(5),
                          // fieldHeight: size.longestSide / 12,
                          // fieldWidth: size.shortestSide / 8,
                          onChanged: (value) {},
                          appContext: context,
                        ),
                        SizedBox(
                          height: size.height / 20,
                        ),
                        InkWell(
                          onTap: () {},
                          child: Text(
                            tr('resend_code'),
                            style: const TextStyle(
                                color: AppColors.blackColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: size.height / 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
