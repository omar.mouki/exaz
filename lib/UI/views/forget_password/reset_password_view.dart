import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';

import '../../shared/custom_widgets/custom_toasts.dart';
import '../../shared/custom_widgets/text_field_widget.dart';

import 'forget_password_view_model.dart';

class ResetPasswordView extends StatefulWidget {
  final String phoneNumber;
  final String code;

  const ResetPasswordView(
      {Key? key, required this.phoneNumber, required this.code})
      : super(key: key);

  @override
  _ResetPasswordViewState createState() => _ResetPasswordViewState();
}

class _ResetPasswordViewState extends State<ResetPasswordView> {
  TextEditingController _passwordController = TextEditingController();

  TextEditingController _confirmPasswordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: ViewModelBuilder<ForgetPasswordViewModel>.reactive(
      viewModelBuilder: () => ForgetPasswordViewModel(),
      builder: (context, model, child) => model.isBusy
          ? const Center(child: CircularProgressIndicator())
          : Form(
              key: _formKey,
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    'assets/pngs/choose_service_bg.png',
                    fit: BoxFit.fill,
                    width: size.width,
                    height: size.height,
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: size.shortestSide / 16,
                        left: size.shortestSide / 16,
                        right: size.shortestSide / 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextFieldWidget(
                          textEditingController: _passwordController,
                          title: tr('new_password'),
                          isDense: true,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                          textColor: AppColors.blackColor,
                          keyboardType: TextInputType.visiblePassword,
                          onValidate: (text) => text != null && text.isNotEmpty
                              ? null
                              : tr('required'),
                        ),
                        TextFieldWidget(
                          textEditingController: _confirmPasswordController,
                          title: tr('confirm_new_password'),
                          isDense: true,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                          textColor: AppColors.blackColor,
                          keyboardType: TextInputType.visiblePassword,
                          onValidate: (text) => text != null && text.isNotEmpty
                              ? null
                              : tr('required'),
                        ),
                        SizedBox(
                          height: size.height / 20,
                        ),
                        FlatButton(
                          color: AppColors.blackColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              if (_passwordController.text ==
                                  _confirmPasswordController.text) {
                                model.resetPassword(
                                    phoneNumber: widget.phoneNumber,
                                    code: widget.code,
                                    password: _passwordController.text);
                              } else {
                                CustomToasts.showMessage(
                                    message: tr('password_not_matced'),
                                    messageType: MessageType.errorMessage);
                              }
                            }
                          },
                          child: SizedBox(
                            width: size.width / 1.2,
                            child: Center(
                              child: Text(tr('send_code'),
                                  style: TextStyle(
                                      color: AppColors.whiteColor,
                                      fontSize: size.shortestSide / 21,
                                      fontWeight: FontWeight.w500)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    ));
  }
}
