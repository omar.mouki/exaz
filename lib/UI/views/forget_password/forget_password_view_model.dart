import 'package:exaaz/UI/shared/custom_widgets/custom_toasts.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:exaaz/core/data/repository/auth_repository.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ForgetPasswordViewModel extends BaseViewModel {
  NavigationService _navigationService = locator<NavigationService>();
  String _maskedMobileNumber = '';
  String _mobileNumber = '';
  String _passwordCode = '';

  String get mobileNumber => _mobileNumber;

  String get passwordCode => _passwordCode;

  String get maskedMobileNumber => _maskedMobileNumber;

  void mobileNumberSetter(String value) {
    _mobileNumber = value;
  }

  void maskedMobileNumberSetter(String value) {
    _maskedMobileNumber = value;
  }

  void passwordCodeSetter(String value) {
    _passwordCode = value;
  }

  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();

  void forgetPassword({required String phoneNumber}) async {
    if (phoneNumber.isNotEmpty) {
      try {
        setBusy(true);
        final commonResponse = await _authenticationRepository.forgetPassword(
            emailOrPhonenumber: phoneNumber);
        if (commonResponse.getStatus) {
          setBusy(false);

          locator<NavigationService>().navigateTo(Routes.verifyCodeView,
              arguments: VerifyCodeViewArguments(phoneNumber: phoneNumber));
        } else {
          setBusy(false);
          CustomToasts.showMessage(
              message: commonResponse.getError.values.elementAt(0),
              messageType: MessageType.errorMessage);
        }
      } catch (e) {
        setBusy(false);
        CustomToasts.showMessage(
            message: e.toString(), messageType: MessageType.alertMessage);
        print(e);
      }
    } else {}
  }

  void verifyPhoneNumber(
      {required String phoneNumber, required String code}) async {
    if (phoneNumber.isNotEmpty) {
      try {
        setBusy(true);
        final commonResponse =
            await _authenticationRepository.verifyForgetPasswordCode(
                emailOrPhonenumber: phoneNumber, verificationCode: code);
        if (commonResponse.getStatus) {
          setBusy(false);
          passwordCodeSetter(commonResponse.data);
          //  stepperStateSetter(StepperState.CodeActive);

          locator<NavigationService>().replaceWith(Routes.resetPasswordView,
              arguments: ResetPasswordViewArguments(
                  code: code, phoneNumber: phoneNumber));
        } else {
          setBusy(false);
          CustomToasts.showMessage(
              message: commonResponse.getError.values.elementAt(0),
              messageType: MessageType.errorMessage);
        }
      } catch (e) {
        setBusy(false);
        CustomToasts.showMessage(
            message: e.toString(), messageType: MessageType.alertMessage);
        print(e);
      }
    } else {}
  }

  void resetPassword(
      {required String phoneNumber,
      required String password,
      required String code}) async {
    if (phoneNumber.isNotEmpty) {
      try {
        setBusy(true);
        final commonResponse = await _authenticationRepository.resetPassword(
            emailOrPhonenumber: phoneNumber,
            password: password,
            verificationCode: code);

        if (commonResponse.getStatus) {
          setBusy(false);
          locator<NavigationService>().popRepeated(2);
        } else {
          setBusy(false);
          CustomToasts.showMessage(
              message: commonResponse.getError.values.elementAt(0),
              messageType: MessageType.errorMessage);
        }
      } catch (e) {
        setBusy(false);
        CustomToasts.showMessage(
            message: e.toString(), messageType: MessageType.alertMessage);
        print(e);
      }
    }
  }

  void resendCode({required String phoneNumber, required String type}) async {
    if (phoneNumber.isNotEmpty) {
      // try {
      //   setBusy(true);
      //   final commonResponse = await _authenticationRepository.resendCode(
      //       phoneNumber: phoneNumber);
      //   if (commonResponse.getStatus) {
      //     setBusy(false);
      //     CustomToasts.showMessage(
      //         message: tr(
      //           "authentication_resend_code_request",
      //         ),
      //         messageType: MessageType.successMessage);
      //     // locator<NavigationService>().navigateTo(Routes.activationCodeView,
      //     //     arguments: ActivationCodeViewArguments(phoneNumber: phoneNumber));
      //   } else {
      //     setBusy(false);
      //     CustomToasts.showMessage(
      //         message: commonResponse.getError.values.elementAt(0),
      //         messageType: MessageType.errorMessage);
      //   }
      // } catch (e) {
      //   setBusy(false);
      //   print(e);
      // }
    } else {}
  }

//------------------------------------------------------------------------------
// Alert Dialogue

  Future showDialog() async {
    final _response = await locator<DialogService>().showDialog(
        dialogPlatform: DialogPlatform.Cupertino,
        title: "Are you sure !",
        description: 'If you click yes, you will lose reset password data',
        buttonTitle: "Yes",
        cancelTitle: "Cancel");
    if (_response!.confirmed) {
      _navigationService.back();
    } else {}
  }
}
