import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/core/data/models/city.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CityDropDown extends StatelessWidget {
  final List<City> cityList;
  final Function(int, String) onChanged;

  const CityDropDown({required this.cityList, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.shortestSide / 10, vertical: size.shortestSide / 10),
      child: Column(
        children: [
          Text('الرجاء اختيار المدينة',
              style: TextStyle(
                  color: AppColors.blue150Color,
                  fontSize: size.shortestSide / 20,
                  fontWeight: FontWeight.bold)),
          const SizedBox(
            height: 25,
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: cityList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  onChanged(cityList[index].id, cityList[index].name);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(cityList[index].name,
                        style: TextStyle(
                            color: AppColors.blackColor,
                            fontSize: size.shortestSide / 26,
                            fontWeight: FontWeight.bold)),
                    const Divider(color: Colors.black),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
