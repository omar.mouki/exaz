import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/locator.dart';
import '../../../core/data/models/city.dart';
import '../../../core/data/models/profile_info.dart';
import '../../../core/data/repository/account_repository.dart';
import '../../../core/data/repository/address_repository.dart';
import '../../../core/data/repository/auth_repository.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../shared/custom_widgets/custom_toasts.dart';

class EditProfileViewModel extends BaseViewModel {
  AuthenticationRepository _authenticationRepository =
      locator<AuthenticationRepository>();
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();
  AddressRepository addressRepository = locator<AddressRepository>();
  AccountRepository _accountRepository = locator<AccountRepository>();
  NavigationService _navigationService = locator<NavigationService>();

  List<City> _cityList = [];
  List<City> get cityList => _cityList;

  int _cityId = 0;
  int get cityId => _cityId;
  set cityIdSet(int value) {
    _cityId = value;
  }

  PickedFile? _avatarImgage;
  PickedFile? get avatarImgage => _avatarImgage;

  changeAvatarPicture(ImageSource image) async {
    final picker = ImagePicker();

    var value = await picker.getImage(source: image);
    if (value != null) {
      _avatarImgage = value;
    }

    notifyListeners();
  }

  void getCities(int cityId) async {
    setBusy(true);
    try {
      final commonResponse = await addressRepository.getCities();
      if (commonResponse.getStatus) {
        setBusy(false);

        Iterable list = commonResponse.getData;
        _cityList = list.map((model) => City.fromJson(model)).toList();
        cityIdSet = cityId;
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError['general'],
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void isPhoneNumberExist(
      {required String fullName,
      required String email,
      required String phoneNumber,
      required String currentPassword,
      required String newPassword}) async {
    try {
      final commonResponse = await _authenticationRepository.isPhoneNumberExist(
        phoneNumber: phoneNumber,
      );
      if (commonResponse.getStatus) {
        if (commonResponse.data) {
          CustomToasts.showMessage(
              message: tr('phone_number_exist'),
              messageType: MessageType.alertMessage);
        } else {
          // updateMyAccount(
          //     fullName, email, phoneNumber, currentPassword, newPassword);
        }
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void updateMyAccount(
      {required String fullName,
      required String email,
      required String phoneNumber,
      required String currentPassword,
      required String newPassword}) async {
    try {
      setBusy(true);
      final commonResponse = await _accountRepository.updateMyAccount(
          accountImage: _avatarImgage != null ? _avatarImgage!.path : '',
          cityId: _cityId,
          email: email,
          fullName: fullName,
          currentPassword: currentPassword,
          newPassword: newPassword,
          phoneNumber: phoneNumber,
          removeAccountImage: false);
      if (commonResponse.getStatus) {
        getMyAccountInformation();
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.alertMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }

  void getMyAccountInformation() async {
    try {
      setBusy(true);
      final commonResponse = await _accountRepository.getMyAccountInformation();
      if (commonResponse.getStatus) {
        ProfileInfo profile = ProfileInfo.fromJson(commonResponse.getData);
        sharedPreferencesRepository.saveProfileInfo(profile: profile);
        setBusy(false);

        CustomToasts.showMessage(
            message: tr('update_account_succ'),
            messageType: MessageType.successMessage);
        _navigationService.popRepeated(2);
      } else {
        setBusy(false);
        CustomToasts.showMessage(
            message: commonResponse.getError.values.elementAt(0),
            messageType: MessageType.errorMessage);
      }
    } catch (e) {
      setBusy(false);
      CustomToasts.showMessage(
          message: e.toString(), messageType: MessageType.alertMessage);
      print(e);
    }
  }
}
