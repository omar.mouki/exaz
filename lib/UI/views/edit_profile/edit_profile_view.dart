import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:exaaz/UI/shared/shared_widgets/text_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/locator.dart';
import '../../../core/data/models/profile_info.dart';
import '../../../core/data/repository/shared_prefrence_repository.dart';
import '../../shared/custom_widgets/custom_image_network.dart';
import '../../shared/custom_widgets/custom_lodaer.dart';
import '../../shared/custom_widgets/ios_bottom_sheet.dart';
import 'edit_profile_view_model.dart';
import 'edit_profile_widgets/city_dropdown.dart';

class EditProfileView extends StatefulWidget {
  @override
  _EditProfileViewState createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView> {
  late ProfileInfo profile;
  SharedPreferencesRepository sharedPreferencesRepository =
      locator<SharedPreferencesRepository>();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _currentPasswordController = TextEditingController();
  TextEditingController _newPasswordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    profile = sharedPreferencesRepository.getProfileInfo()!;
    _nameController.text = profile.fullName ?? '';
    _emailController.text = profile.email ?? '';
    _cityController.text = profile.city ?? '';
    _mobileController.text = profile.phoneNumber ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: ViewModelBuilder<EditProfileViewModel>.reactive(
      viewModelBuilder: () => EditProfileViewModel(),
      onModelReady: (model) {
        model.getCities(profile.cityId);
      },
      builder: (context, model, child) => model.isBusy
          ? Center(
              child: CustomLoader(),
            )
          : Stack(
              children: <Widget>[
                Image.asset(
                  'assets/pngs/choose_service_bg.png',
                  fit: BoxFit.fill,
                  width: size.width,
                  height: size.height,
                ),
                Form(
                  key: _formKey,
                  child: ListView(
                      padding: EdgeInsets.only(
                          top: size.height / 7,
                          left: size.width / 22,
                          right: size.width / 22),
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            locator<NavigationService>().back();
                          },
                          child: Row(
                            children: [
                              SvgPicture.asset('assets/icons/arrow_forward.svg',
                                  color: AppColors.blackColor,
                                  width: size.width / 16),
                              const SizedBox(
                                width: 8,
                              ),
                              Text(tr('edit_profile'),
                                  style: TextStyle(
                                      color: AppColors.blackColor,
                                      fontSize: size.shortestSide / 26,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 16, horizontal: size.width / 4),
                          child: SizedBox(
                            height: 150,
                            width: 150,
                            child: CircularImageNetwork(
                              imgFit: BoxFit.fill,
                              imageOnTab: () {
                                IosBottomSheet.show(
                                    context: context,
                                    bottomSheetWidgets: [
                                      BottomSheetIosWidget(
                                        title: tr('camera'),
                                        textColor: AppColors.blueColor,
                                        onPressed: () {
                                          model.changeAvatarPicture(
                                              ImageSource.camera);
                                        },
                                      ),
                                      BottomSheetIosWidget(
                                        title: tr('gallery'),
                                        textColor: AppColors.blueColor,
                                        onPressed: () {
                                          model.changeAvatarPicture(
                                              ImageSource.gallery);
                                        },
                                      ),
                                    ]);
                              },
                              imageFile: model.avatarImgage,
                              isSquare: true,
                              imagePath: profile.picture ?? '',
                              imageSize: size.width / 2,
                              // imageErrorBackroundColor: AppColors.grey100Color,
                              // imageErrorColor: AppColors.grey200Color,
                              placeHolderSize: size.width / 12,
                              placeHolderSvgPath:
                                  'assets/icons/navbar_profile.svg',
                            ),
                          ),
                        ),
                        CustomTextFieldWidget(
                          onValidate: (text) => text != null && text.isNotEmpty
                              ? null
                              : tr('required'),
                          textEditingController: _nameController,
                          title: tr('full_name'),
                          isDense: true,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                          suffixConstraint:
                              BoxConstraints(maxHeight: 20, maxWidth: 20),
                          suffixWidget: InkWell(
                            onTap: () {},
                            child: SvgPicture.asset(
                              'assets/icons/navbar_profile.svg',
                              color: AppColors.grey200Color,
                            ),
                          ),
                        ),
                        CustomTextFieldWidget(
                          onValidate: (text) => text != null && text.isNotEmpty
                              ? null
                              : tr('required'),
                          textEditingController: _emailController,
                          title: tr('email'),
                          isDense: true,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                          suffixConstraint:
                              BoxConstraints(maxHeight: 20, maxWidth: 20),
                          suffixWidget: InkWell(
                            onTap: () {},
                            child: SvgPicture.asset(
                              'assets/icons/messages.svg',
                              color: AppColors.grey200Color,
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {
                                  return CityDropDown(
                                    cityList: model.cityList,
                                    onChanged: (cityId, cityName) {
                                      locator<NavigationService>().back();
                                      setState(() {
                                        model.cityIdSet = cityId;
                                        _cityController.text = cityName;
                                      });
                                    },
                                  );
                                });
                          },
                          child: IgnorePointer(
                            child: CustomTextFieldWidget(
                              textEditingController: _cityController,
                              title: tr('city'),
                              isDense: true,
                              underlineColor: AppColors.grey300Color,
                              underlineWidth: 2,
                              suffixConstraint:
                                  BoxConstraints(maxHeight: 20, maxWidth: 20),
                              suffixWidget: InkWell(
                                onTap: () {},
                                child: SvgPicture.asset(
                                  'assets/icons/location.svg',
                                  color: AppColors.grey200Color,
                                ),
                              ),
                            ),
                          ),
                        ),
                        CustomTextFieldWidget(
                          onValidate: (text) => text != null && text.isNotEmpty
                              ? null
                              : tr('required'),
                          textEditingController: _mobileController,
                          title: tr('phone_number'),
                          isDense: true,
                          keyboardType: TextInputType.phone,
                          maxLength: 10,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                          suffixConstraint:
                              BoxConstraints(maxHeight: 20, maxWidth: 20),
                          suffixWidget: InkWell(
                            onTap: () {},
                            child: SvgPicture.asset(
                              'assets/icons/phone.svg',
                              color: AppColors.grey200Color,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        CustomTextFieldWidget(
                          onValidate: (text) => text != null &&
                                  text.isNotEmpty &&
                                  _newPasswordController.text.isNotEmpty
                              ? tr('required')
                              : null,
                          textEditingController: _currentPasswordController,
                          keyboardType: TextInputType.visiblePassword,
                          title: tr('current_password'),
                          isDense: true,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        CustomTextFieldWidget(
                          onValidate: (text) =>
                              _currentPasswordController.text.isNotEmpty &&
                                      text != null &&
                                      text.isNotEmpty
                                  ? tr('required')
                                  : null,
                          textEditingController: _newPasswordController,
                          keyboardType: TextInputType.visiblePassword,
                          title: tr('new_password'),
                          isDense: true,
                          underlineColor: AppColors.grey300Color,
                          underlineWidth: 2,
                        ),
                        const SizedBox(
                          height: 32,
                        ),
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: size.width / 6),
                          child: FlatButton(
                            color: AppColors.blackColor,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(size.width / 20),
                            ),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                model.updateMyAccount(
                                    fullName: _nameController.text,
                                    email: _emailController.text,
                                    phoneNumber: _mobileController.text,
                                    currentPassword:
                                        _currentPasswordController.text,
                                    newPassword: _newPasswordController.text);

                                _formKey.currentState!.reset();
                              }
                            },
                            child: Text(tr('save_data'),
                                style: TextStyle(
                                    color: AppColors.whiteColor,
                                    fontSize: size.shortestSide / 26,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ),
                        const SizedBox(
                          height: 64,
                        ),
                      ]),
                ),
              ],
            ),
    ));
  }
}
