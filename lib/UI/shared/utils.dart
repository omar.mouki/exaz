import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:exaaz/core/data/repository/shared_prefrence_repository.dart';
import 'package:flutter/material.dart';

import 'package:stacked_services/stacked_services.dart';

import '../../app/locator.dart';
import 'custom_widgets/slide_up_animation.dart';

// bool get isLoggedIn => locator<SharedPreferencesRepository>().getLoggedIn();

// int get userId => isLoggedIn
//     ? locator<SharedPreferencesRepository>().getLoginInfo().userId!
//     : 0;

// UserInfo get userInfo => isLoggedIn
//     ? locator<SharedPreferencesRepository>().getUserInfo()
//     : UserInfo();

NavigationService get navigationService => locator<NavigationService>();
SharedPreferencesRepository get sharedprefRepo =>
    locator<SharedPreferencesRepository>();
// AuthRepository get authRepo => locator<AuthRepository>();

//====================== Error Widget =================================
void showErrorAlertDialog(
  BuildContext context,
  String errorMessage, {
  VoidCallback? cancelButtonCallBack,
}) {
  BotToast.cleanAll();
  BotToast.showAnimationWidget(
      clickClose: true,
      allowClick: true,
      onlyOne: true,
      crossPage: true,
      // backButtonBehavior: backButtonBehavior,
      wrapToastAnimation: (controller, cancel, child) => Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  cancel();
                  // backgroundReturn?.call();
                  cancelButtonCallBack?.call();
                },
                //The DecoratedBox here is very important,he will fill the entire parent component
                child: AnimatedBuilder(
                  builder: (_, child) => Opacity(
                    opacity: controller.value,
                    child: child,
                  ),
                  child: const DecoratedBox(
                    decoration: BoxDecoration(color: Colors.black26),
                    child: SizedBox.expand(),
                  ),
                  animation: controller,
                ),
              ),
              SlideUpAnimation(
                controller: controller,
                child: child,
              )
            ],
          ),
      toastBuilder: (cancelFunc) => AlertDialog(
            backgroundColor: Colors.blue[50],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            title: const Text(
              'حدث خطأ',
              textAlign: TextAlign.center,
            ),
            content: SizedBox(
              width: MediaQuery.of(context).size.width * 0.5,
              height: MediaQuery.of(context).size.height * 0.5,
              // color: Colors.blue[50],
              child: Column(
                children: [
                  AutoSizeText(
                    errorMessage,
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                    maxLines: 3,
                  )
                ],
              ),
            ),
            // actionsAlignment: MainAxisAlignment.start,
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  cancelFunc();
                  cancelButtonCallBack?.call();
                },
                // highlightColor: const Color(0x55FF8A80),
                // splashColor: const Color(0x99FF8A80),
                child: const Text(
                  'حسنا',
                  style: const TextStyle(color: Colors.redAccent),
                ),
              ),
            ],
          ),
      animationDuration: const Duration(milliseconds: 300));
}

extension CapExtension on String {
  String get inCaps =>
      this.isEmpty ? '' : '${this[0].toUpperCase()}${this.substring(1)}';

  String get allInCaps => this.isEmpty ? '' : this.toUpperCase();

  String get capitalizeFirstofEach =>
      this.isEmpty ? '' : this.split(" ").map((str) => str.inCaps).join(" ");
}
