import 'package:flutter/painting.dart';

class AppColors {
  static const Color darkGreyColor = Color.fromRGBO(34, 34, 34, 1);
  static const Color blackColor = Color.fromRGBO(22, 22, 22, 1);
  static const Color whiteColor = Color.fromRGBO(255, 255, 255, 1);
  static const Color blueColor = Color.fromRGBO(49, 121, 242, 1);
  static const Color yellowColor = Color.fromRGBO(255, 196, 0, 1);

  static const Color redColor = Color.fromRGBO(230, 69, 82, 1);
  static const Color greenColor = Color.fromRGBO(43, 159, 0, 1);
  static const Color greenLightColor = Color.fromRGBO(108, 183, 6, 1);

  static const Color greyIosBottomSheetBackgroundColor =
      Color.fromRGBO(248, 248, 248, 0.82);

  static const Color transparentColor = Color.fromRGBO(0, 0, 0, 0);

  static const Color blue150Color = Color.fromRGBO(0, 47, 108, 1);
  static const Color blue300Color = Color.fromRGBO(57, 45, 93, 1);
  static const Color blueAccentColor = Color.fromRGBO(0, 180, 225, 1);
  static const Color turquoiseColor = Color.fromRGBO(0, 157, 175, 1);
  static const Color cyanColor = Color.fromRGBO(0, 175, 162, 1);

  static const Color grey25Color = Color.fromRGBO(248, 248, 248, 1);
  static const Color grey50Color = Color.fromRGBO(246, 246, 246, 1);
  static const Color grey75Color = Color.fromRGBO(241, 241, 241, 1);
  static const Color grey100Color = Color.fromRGBO(230, 230, 230, 1);
  static const Color grey200Color = Color.fromRGBO(165, 165, 165, 1);
  static const Color grey300Color = Color.fromRGBO(112, 112, 112, 1);
  static const Color grey500Color = Color.fromRGBO(117, 120, 123, 1);

  static const Color markerGreenColor = Color.fromRGBO(0, 175, 162, 1);

  static const Color lightOrangeColor = Color.fromRGBO(255, 221, 0, 1);
  static const Color orangeColor = Color.fromRGBO(255, 143, 28, 1);
  static const Color darkOrangeColor = Color.fromRGBO(255, 196, 0, 1);
  static const Color brightRedColor = Color.fromRGBO(255, 88, 93, 1);

  static const Color grey = Color.fromRGBO(0, 47, 108, 0.38);
  static const Color greyColor = Color(0xFFAAADB3);
}
