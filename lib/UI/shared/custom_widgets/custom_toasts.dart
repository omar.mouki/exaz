import 'package:bot_toast/bot_toast.dart';
import 'package:exaaz/UI/shared/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../colors.dart';

enum MessageType { successMessage, errorMessage, alertMessage, infoMessage }

class CustomToasts {
  static showMessage(
      {required String message, required MessageType messageType}) {
    switch (messageType) {
      case MessageType.successMessage:
        BotToast.showCustomText(
          duration: const Duration(seconds: 3),
          onlyOne: true,
          toastBuilder: (f) => Container(
            margin: const EdgeInsets.symmetric(horizontal: 22),
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/approved2-01.svg',
                  width: 44,
                ),
                SizedBox(
                  width: 300,
                  child: Center(
                    child: Text(
                      message.inCaps,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.greenColor.withOpacity(0.3),
                  offset: const Offset(0, 7),
                  blurRadius: 21,
                )
              ],
              color: AppColors.whiteColor,
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        );
        break;
      case MessageType.errorMessage:
        BotToast.showCustomText(
          duration: const Duration(seconds: 3),
          onlyOne: true,
          toastBuilder: (f) => Container(
            margin: const EdgeInsets.symmetric(horizontal: 22),
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/rejected-01.svg',
                  width: 38,
                ),
                SizedBox(
                  width: 300,
                  child: Center(
                    child: Text(
                      message.inCaps,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.redColor.withOpacity(0.3),
                  offset: const Offset(0, 7),
                  blurRadius: 21,
                )
              ],
              color: AppColors.whiteColor,
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        );
        break;
      case MessageType.alertMessage:
        BotToast.showCustomText(
          duration: const Duration(seconds: 3),
          onlyOne: true,
          toastBuilder: (f) => Container(
            margin: const EdgeInsets.symmetric(horizontal: 22),
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/warning.svg',
                  color: AppColors.orangeColor,
                  width: 32,
                ),
                SizedBox(
                  width: 300,
                  child: Center(
                    child: Text(
                      message.inCaps,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.orangeColor.withOpacity(0.3),
                  offset: const Offset(0, 7),
                  blurRadius: 21,
                )
              ],
              color: AppColors.whiteColor,
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        );
        break;
      case MessageType.infoMessage:
        BotToast.showCustomText(
          duration: const Duration(seconds: 5),
          onlyOne: true,
          toastBuilder: (f) => Container(
            margin: const EdgeInsets.symmetric(horizontal: 22),
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/info.svg',
                  width: 38,
                ),
                SizedBox(
                  width: 300,
                  child: Center(
                    child: Text(
                      message.inCaps,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.grey500Color.withOpacity(0.3),
                  offset: const Offset(0, 7),
                  blurRadius: 21,
                )
              ],
              color: AppColors.whiteColor,
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        );
        break;

      default:
        BotToast.showCustomText(
          onlyOne: true,
          toastBuilder: (f) => Container(
            margin: const EdgeInsets.symmetric(horizontal: 22),
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/info.svg',
                  width: 38,
                ),
                SizedBox(
                  width: 300,
                  child: Center(
                    child: Text(
                      message.inCaps,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.grey500Color.withOpacity(0.3),
                  offset: const Offset(0, 7),
                  blurRadius: 21,
                )
              ],
              color: AppColors.whiteColor,
              borderRadius: BorderRadius.circular(12),
            ),
          ),
        );
    }
  }
}
