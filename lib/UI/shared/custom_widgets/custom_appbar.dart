import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';

import 'package:flutter/material.dart';

import '../colors.dart';

class CustomAppBar extends StatelessWidget {
  final Function? backOnTab;
  final Function? saveOnTab;

  const CustomAppBar({Key? key, this.backOnTab, this.saveOnTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.longestSide / 6,
      color: AppColors.darkGreyColor,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.symmetric(horizontal: size.width / 20, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            onTap: () {
              backOnTab?.call();
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                SvgIconButton(
                  svgPath: 'assets/icons/arrow_forward.svg',
                  width: size.width / 18,
                ),
                const SizedBox(
                  width: 4,
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      tr('back'),
                      style: TextStyle(
                          color: AppColors.yellowColor,
                          fontWeight: FontWeight.w400,
                          fontSize: size.shortestSide / 24),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                  ],
                )
              ],
            ),
          ),
          if (saveOnTab != null)
            FlatButton(
              color: AppColors.yellowColor,
              padding: EdgeInsets.symmetric(horizontal: size.width / 24),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(size.width / 20),
              ),
              onPressed: () {
                saveOnTab?.call();
              },
              child: Text(tr('save_end'),
                  style: TextStyle(
                      color: AppColors.whiteColor,
                      fontSize: size.shortestSide / 26,
                      fontWeight: FontWeight.w400)),
            ),
        ],
      ),
    );
  }
}
