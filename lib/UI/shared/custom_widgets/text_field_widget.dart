import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class TextFieldWidget extends StatefulWidget {
  final String title;
  final String? titleOnFocus;
  final int linesNumber;
  final bool isDense;
  final InputBorder? enabledBorder;
  final Color? cursorColor;
  final Color? underlineColor;
  final double? underlineWidth;
  //use linesNumber instead
  final bool isBig;
  final Color? textColor;
  final Color? goodToGoMessageColor;
  final TextStyle textCustomStyle;
  final BoxConstraints? prefixConstraint;
  //use textCustomStyle instead
  final bool isBigTextSize;
  // final double height;
  final Widget? suffixWidget;
  final Widget? prefixWidget;
  final Function(String?)? onSaved;
  final TextInputType keyboardType;
  final FocusNode? fieldFocusNode;
  final FocusNode? nextFocusNode;
  final String? goodToGoMessage;
  final String? errorMessage;
  final String? Function(String?)? onValidate;
  final Function(String)? onChange;
  final List<TextInputFormatter>? inputFormatters;
  final int? maxLength;
  final TextEditingController? textEditingController;
  final bool isReadOnly;
  final void Function()? onTap;

  TextFieldWidget(
      {Key? key,
      this.underlineColor,
      this.underlineWidth,
      this.enabledBorder,
      this.isDense = false,
      this.onSaved,
      // this.height,
      this.cursorColor,
      this.prefixConstraint,
      this.textCustomStyle = const TextStyle(),
      this.isBigTextSize = false,
      this.prefixWidget,
      this.titleOnFocus,
      this.linesNumber = 1,
      this.textEditingController,
      this.goodToGoMessage,
      this.errorMessage,
      this.textColor,
      this.fieldFocusNode,
      this.nextFocusNode,
      required this.title,
      this.onValidate,
      this.suffixWidget,
      this.isBig = false,
      this.onChange,
      this.keyboardType = TextInputType.text,
      this.inputFormatters,
      this.maxLength,
      this.isReadOnly = false,
      this.goodToGoMessageColor,
      this.onTap})
      : super(key: key);

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  int foucs = 0;
  FocusNode _focusNode = FocusNode();
  TextEditingController _textEditingController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (!_focusNode.hasFocus) {
        setState(() {
          foucs += 1;
        });
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return TextFormField(
        onTap: widget.onTap,
        key: widget.key,
        buildCounter: (context,
                {required currentLength, maxLength, required isFocused}) =>
            null,
        controller: widget.textEditingController ?? _textEditingController,
        validator: widget.onValidate,
        focusNode: widget.fieldFocusNode ?? _focusNode,
        cursorColor: widget.cursorColor ?? AppColors.blueColor,
        maxLines: widget.isBig ? 3 : widget.linesNumber,
        minLines: 1,
        readOnly: widget.isReadOnly,
        onChanged: widget.onChange,
        onFieldSubmitted: (value) {
          if (widget.nextFocusNode != null) {
            widget.nextFocusNode?.requestFocus();
          }
        },
        keyboardType: widget.keyboardType,
        inputFormatters: widget.inputFormatters,
        maxLength: widget.maxLength,
        obscureText: widget.keyboardType == TextInputType.visiblePassword,
        onSaved: widget.onSaved,
        style: TextStyle(
                fontSize: widget.isBigTextSize
                    ? size.shortestSide / 16
                    : size.shortestSide / 23,
                color: widget.textColor ?? AppColors.whiteColor,
                fontWeight: FontWeight.w500)
            .merge(widget.textCustomStyle.copyWith(inherit: true)),
        decoration: InputDecoration(
            prefixIconConstraints: widget.prefixConstraint,
            errorText: foucs > 0 ? widget.errorMessage : null,
            helperText: widget.goodToGoMessage,
            counterText: ' ',
            isDense: widget.isDense,
            contentPadding: widget.isDense
                ? EdgeInsets.only(
                    bottom: size.longestSide / 130,
                    top: widget.suffixWidget != null
                        ? size.shortestSide / 20
                        : 0)
                : null,
            errorStyle: TextStyle(
              fontSize: size.shortestSide / 32,
            ),
            helperStyle: TextStyle(
              color: widget.goodToGoMessageColor,
              fontSize: size.shortestSide / 32,
            ),
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                    color: widget.underlineColor ?? AppColors.grey300Color,
                    width: 0.5)),
            border: UnderlineInputBorder(
                borderSide: BorderSide(color: widget.underlineColor ?? AppColors.grey300Color, width: widget.underlineWidth ?? 0.5)),
            enabledBorder: widget.enabledBorder ?? UnderlineInputBorder(borderSide: BorderSide(color: widget.goodToGoMessage != null ? AppColors.grey300Color : widget.underlineColor ?? AppColors.grey200Color.withOpacity(0.3), width: 0.5)),
            suffixIcon: widget.suffixWidget,
            prefixIcon: widget.prefixWidget,
            fillColor: AppColors.whiteColor,
            hintText: widget.titleOnFocus != null && (_focusNode.hasFocus || widget.fieldFocusNode != null && widget.fieldFocusNode!.hasFocus) ? widget.titleOnFocus : widget.title,
            hintStyle: TextStyle(
              color: AppColors.grey300Color,
              fontSize: widget.isBigTextSize
                  ? size.shortestSide / 16
                  : size.shortestSide / 24,
              fontWeight: FontWeight.w400,
            ).merge(
              widget.textCustomStyle.copyWith(inherit: true),
            )));
  }
}

class SuffixWidget extends StatelessWidget {
  final FocusNode focusNode;
  final Widget child;
  final Function onPressed;
  final bool isError;
  final bool withoutContainer;
  const SuffixWidget({
    Key? key,
    this.isError = false,
    required this.focusNode,
    required this.child,
    required this.onPressed,
    this.withoutContainer = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        focusNode.unfocus();

        focusNode.canRequestFocus = false;
        onPressed();
        Future.delayed(const Duration(milliseconds: 100), () {
          focusNode.canRequestFocus = true;
        });
      },
      child: SizedBox(
        height: size.longestSide / 20,
        width: size.shortestSide / 4.5,
        child: withoutContainer
            ? Align(
                alignment: AlignmentDirectional.centerEnd,
                child: Padding(
                    padding: const EdgeInsetsDirectional.only(end: 8),
                    child: child),
              )
            : Center(
                child: Container(
                  height: size.shortestSide / 16,
                  decoration: BoxDecoration(
                      color:
                          isError ? AppColors.blueColor : AppColors.yellowColor,
                      borderRadius:
                          BorderRadius.circular(size.shortestSide / 24)),
                  child: child,
                ),
              ),
      ),
    );
  }
}
