import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';
import 'package:flutter/material.dart';

import '../colors.dart';

class ShareAppBar extends StatelessWidget {
  final Function? backOnTab;
  final Function? shareOnTab;
  final String? title;
  const ShareAppBar({Key? key, this.backOnTab, this.shareOnTab, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.longestSide / 6,
      color: AppColors.darkGreyColor,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.symmetric(horizontal: size.width / 32, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          if (backOnTab != null)
            SvgIconButton(
              svgPath: 'assets/icons/arrow_forward.svg',
              width: size.width / 20,
              onPressed: backOnTab!,
            ),
          if (title != null)
            Text(
              title!,
              style: TextStyle(
                  color: AppColors.yellowColor,
                  fontWeight: FontWeight.w500,
                  fontSize: size.width / 19),
            ),
          SvgIconButton(
            svgPath: 'assets/icons/share.svg',
            width: size.width / 15,
            color: AppColors.yellowColor,
            onPressed: shareOnTab,
          ),
        ],
      ),
    );
  }
}
