import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ListNoDataWidget extends StatelessWidget {
  const ListNoDataWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(25),
      child: Text(
        tr('no_data_fount'),
        style: const TextStyle(fontSize: 20, color: AppColors.yellowColor),
      ),
      alignment: Alignment.topCenter,
    );
  }
}
