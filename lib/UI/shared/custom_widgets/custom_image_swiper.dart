import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_tv/flutter_swiper.dart';

import '../colors.dart';

class CustomImageSwiper extends StatelessWidget {
  const CustomImageSwiper(
      {Key? key,
      required this.size,
      req,
      required this.itemCount,
      required this.imageUrlList})
      : super(key: key);

  final Size size;
  final int itemCount;
  final List<String?> imageUrlList;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 350,
      child: Swiper(
        outer: false,
        itemBuilder: (c, i) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
            margin: EdgeInsets.only(bottom: 30),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: imageUrlList.length == 0
                  ? Image.asset('assets/pngs/image_placeholder.png')
                  : CachedNetworkImage(
                      imageUrl: imageUrlList[i]!,
                      fit: BoxFit.cover,
                      width: size.width / 2.6,
                      height: size.width / 4,
                      placeholder: (context, s) =>
                          const Center(child: CircularProgressIndicator()),
                      errorWidget: (context, s, o) =>
                          Image.asset('assets/pngs/image_placeholder.png'),
                    ),
            ),
          );
        },
        pagination: SwiperPagination(
          builder: DotSwiperPaginationBuilder(
            activeColor: AppColors.yellowColor,
            color: Colors.white,
            // space: 6
          ),
          margin: EdgeInsets.all(0.0),
        ),
        itemCount: itemCount == 0 ? 1 : itemCount,
      ),
    );
  }
}
