import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class ListErrorWidget extends StatelessWidget {
  const ListErrorWidget({
    Key? key,
    required this.pagingController,
  }) : super(key: key);
  final PagingController pagingController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(25),
          child: Text(
            tr('error_loading_data'),
            style: const TextStyle(fontSize: 20, color: AppColors.yellowColor),
          ),
          alignment: Alignment.topCenter,
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(primary: AppColors.yellowColor),
          onPressed: () {
            pagingController.refresh();
          },
          child: Text(
            tr('load_data'),
          ),
        )
      ],
    );
  }
}
