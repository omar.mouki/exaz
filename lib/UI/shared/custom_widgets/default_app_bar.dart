import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class DefaultAppBar extends StatelessWidget {
  final Function? backOnTab;
  final String title;
  final Widget? action;
  final bool showBack;
  const DefaultAppBar(
      {Key? key,
      this.backOnTab,
      required this.title,
      this.action,
      required this.showBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.longestSide / 6,
      color: AppColors.darkGreyColor,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.symmetric(horizontal: size.width / 32, vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // Spacer(),
          Expanded(
            flex: 1,
            child: action ?? Container(),
          ),
          Expanded(
            flex: action == null ? 1 : 2,
            child: Row(
              mainAxisAlignment: action != null
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.start,
              children: [
                Text(
                  title,
                  // textAlign: TextAlign.end,
                  style: TextStyle(
                      color: AppColors.yellowColor,
                      fontWeight: FontWeight.w500,
                      fontSize: size.width / 18),
                ),
              ],
            ),
          ),
          // Spacer(),
          Expanded(
            flex: 1,
            child: !showBack
                ? Container()
                : InkWell(
                    onTap: () {
                      if (backOnTab != null) backOnTab!();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          'رجوع',
                          style: TextStyle(
                              color: AppColors.yellowColor,
                              fontWeight: FontWeight.w500,
                              fontSize: size.width / 18),
                        ),
                        SizedBox(width: 5),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: AppColors.yellowColor,
                        )
                      ],
                    ),
                  ),
          )
        ],
      ),
    );
  }
}
