import 'package:flutter/material.dart';

import '../colors.dart';

typedef void RatingChangeCallback(double rating);

class StarRating extends StatelessWidget {
  final int starCount;
  final double rating;
  final RatingChangeCallback? onRatingChanged;
  final Color? color;

  StarRating(
      {this.starCount = 5, this.rating = .0, this.onRatingChanged, this.color});

  Widget buildStar(BuildContext context, int index) {
    final size = MediaQuery.of(context).size;
    Icon icon;
    if (index >= rating) {
      icon = Icon(
        Icons.star,
        color: AppColors.yellowColor,
        size: size.shortestSide / 20,
      );
    } else {
      icon = Icon(
        Icons.star,
        color: color ?? Theme.of(context).primaryColor,
        size: size.shortestSide / 20,
      );
    }
    return InkResponse(
      onTap:
          onRatingChanged == null ? null : () => onRatingChanged!(index + 1.0),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          icon,
          if (index >= rating)
            Icon(
              Icons.star,
              color: AppColors.blackColor,
              size: size.shortestSide / 23,
            )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children:
            List.generate(starCount, (index) => buildStar(context, index)));
  }
}
