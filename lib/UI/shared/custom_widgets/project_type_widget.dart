import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';

class ProjectTypeWidget extends StatelessWidget {
  const ProjectTypeWidget(
      {Key? key,
      this.export = true,
      this.excute = false,
      this.enabled = true,
      this.onExcuteChanged,
      this.onExportChanged})
      : super(key: key);
  final bool export;
  final bool excute;
  final bool enabled;
  final void Function(bool?)? onExportChanged;
  final void Function(bool?)? onExcuteChanged;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          width: 16,
          height: 16,
          child: Checkbox(
            checkColor: AppColors.yellowColor,
            fillColor: MaterialStateColor.resolveWith(
              (states) {
                if (states.contains(MaterialState.selected)) {
                  return AppColors
                      .yellowColor; // the color when checkbox is selected;
                }
                return AppColors
                    .yellowColor; //the color when checkbox is unselected;
              },
            ),
            value: export,
            onChanged: enabled ? onExportChanged : null,
          ),
        ),
        const SizedBox(width: 12),
        Text(
          'توريد',
          style: const TextStyle(color: AppColors.greyColor),
        ),
        const SizedBox(width: 16),
        SizedBox(
          width: 16,
          height: 16,
          child: Checkbox(
            checkColor: AppColors.yellowColor,
            fillColor: MaterialStateColor.resolveWith(
              (states) {
                if (states.contains(MaterialState.selected)) {
                  return AppColors
                      .yellowColor; // the color when checkbox is selected;
                }
                return AppColors
                    .yellowColor; //the color when checkbox is unselected;
              },
            ),

            value: excute,

            // shape: CircleBorder(),
            onChanged: enabled ? onExcuteChanged : null,
          ),
        ),
        const SizedBox(width: 12),
        Text(
          'تنفيذ',
          style: const TextStyle(color: AppColors.greyColor),
        )
      ],
    );
  }
}
