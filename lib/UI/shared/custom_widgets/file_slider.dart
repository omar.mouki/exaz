import 'dart:developer';
import 'dart:io';

import 'package:exaaz/UI/shared/custom_widgets/svg_icon_button.dart';
import 'package:exaaz/app/locator.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:flutter/material.dart';
import 'package:ndialog/ndialog.dart';
import 'package:stacked_services/stacked_services.dart';

import '../colors.dart';

enum FileType { Image, Pdf }
enum ViewType { ReadOnly, Edit }

class FileSliderWidget extends StatelessWidget {
  final List<String> filesUrls;
  final EdgeInsetsGeometry? padding;
  final FileType fileType;
  final ViewType viewType;
  final double? height;
  FileSliderWidget({
    required this.filesUrls,
    Key? key,
    this.height,
    this.padding,
    required this.fileType,
    required this.viewType,
    this.onCancelClicked,
    this.addNewImage,
    this.addNewPdf,
  }) : super(key: key);
  final Function(List<String> list)? onCancelClicked;
  final Function(int type)? addNewImage;
  final Function()? addNewPdf;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width,
      child: Container(
        // margin: EdgeInsets.only(left: 15, right: 15, bottom: 15, top: 0),
        padding: EdgeInsets.only(left: 15, right: 10, bottom: 8, top: 8),
        height: height ?? 150,
        child: Align(
          alignment: Alignment.centerRight,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            itemCount: filesUrls.length + 1,
            reverse: false,
            itemBuilder: (context, index) {
              return index == filesUrls.length && viewType == ViewType.ReadOnly
                  ? Container()
                  : Padding(
                      padding: EdgeInsetsDirectional.only(end: 8),
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius:
                                  BorderRadius.circular(size.width / 28),
                              border: Border.all(color: Colors.grey, width: 1)),
                          child: ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(size.width / 28),
                              child: InkWell(
                                onTap: () {
                                  log('out');
                                  if (index != filesUrls.length) {
                                    if (viewType == ViewType.ReadOnly &&
                                        fileType == FileType.Image) {
                                      locator<NavigationService>().navigateTo(
                                          Routes.photoGalleryView,
                                          arguments: PhotoGalleryViewArguments(
                                              imageUrl: filesUrls[index]));
                                    }
                                    if (viewType == ViewType.ReadOnly &&
                                        fileType == FileType.Pdf) {
                                      // nav to  pdf view
                                      locator<NavigationService>().navigateTo(
                                          Routes.pdfView,
                                          arguments: PdfViewArguments(
                                              pdfUrl: filesUrls[index]));
                                    }
                                  }
                                },
                                child: Stack(
                                  // alignment: Al,
                                  fit: StackFit.passthrough,
                                  children: [
                                    index == filesUrls.length
                                        ? SvgIconButton(
                                            svgPath: 'assets/icons/add.svg',
                                            color: AppColors.blackColor,
                                            width: size.longestSide / 28,
                                            onPressed: () async {
                                              if (fileType == FileType.Image) {
                                                await NDialog(
                                                  dialogStyle: DialogStyle(
                                                      titleDivider: true),
                                                  title: Text(
                                                      'الرجاء اختيار مصدر الصورة'),
                                                  // content:
                                                  //     Text('camera or galary'),
                                                  actions: <Widget>[
                                                    FlatButton(
                                                        child: Text('الكميرا'),
                                                        onPressed: () {
                                                          locator<NavigationService>()
                                                              .back();
                                                          if (addNewImage !=
                                                              null)
                                                            addNewImage!(1);
                                                        }),
                                                    FlatButton(
                                                        child: Text('المعرض'),
                                                        onPressed: () {
                                                          locator<NavigationService>()
                                                              .back();
                                                          if (addNewImage !=
                                                              null)
                                                            addNewImage!(2);
                                                        }),
                                                    FlatButton(
                                                        child:
                                                            Text('الغاء الامر'),
                                                        onPressed: () {
                                                          locator<NavigationService>()
                                                              .back();
                                                        }),
                                                  ],
                                                ).show(context);
                                              } else {
                                                addNewPdf!();
                                              }
                                            },
                                          )
                                        : fileType == FileType.Pdf
                                            ? Icon(Icons.file_copy)
                                            : (filesUrls[index]
                                                    .startsWith('http')
                                                ? Image.network(
                                                    filesUrls[index],
                                                    fit: BoxFit.cover,
                                                  )
                                                : Image.file(
                                                    File(filesUrls[index]),
                                                    fit: BoxFit.cover,
                                                  )),
                                    viewType == ViewType.ReadOnly ||
                                            index == filesUrls.length
                                        ? Container()
                                        : Align(
                                            alignment: Alignment.topRight,
                                            child: InkWell(
                                              onTap: () {
                                                log('cancel');
                                                filesUrls.removeAt(index);
                                                if (onCancelClicked != null) {
                                                  onCancelClicked!(filesUrls);
                                                }
                                              },
                                              child: Icon(Icons.cancel_outlined,
                                                  color: Colors.white),
                                            ),
                                          ),
                                  ],
                                ),
                              )),
                        ),
                      ),
                    );
            },
          ),
        ),
      ),
    );
  }
}
