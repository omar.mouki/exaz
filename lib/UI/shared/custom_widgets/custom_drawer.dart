import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../colors.dart';
import 'custom_image_network.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width / 1.7,
      child: Drawer(
        child: Container(
          padding: EdgeInsets.symmetric(
              horizontal: size.width / 22, vertical: size.height / 20),
          color: AppColors.darkGreyColor,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CircularImageNetwork(
                imagePath: 'https://picsum.photos/100',
                imageSize: size.width / 5,
                placeHolderSvgPath: '',
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                'محمد حسام الطرابلسي',
                style: TextStyle(
                    color: AppColors.whiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: size.shortestSide / 24),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                'لياس',
                style: TextStyle(
                    color: AppColors.whiteColor,
                    fontWeight: FontWeight.bold,
                    fontSize: size.shortestSide / 26),
              ),
              Container(
                height: 1,
                margin: const EdgeInsets.symmetric(vertical: 8),
                color: AppColors.grey300Color,
              ),
              InkWell(
                onTap: () {},
                child: Text(
                  tr('profile'),
                  style: TextStyle(
                      color: AppColors.whiteColor,
                      fontWeight: FontWeight.w400,
                      fontSize: size.shortestSide / 26),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              InkWell(
                onTap: () {},
                child: Text(
                  tr('my_orders'),
                  style: TextStyle(
                      color: AppColors.whiteColor,
                      fontWeight: FontWeight.w400,
                      fontSize: size.shortestSide / 24),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              InkWell(
                onTap: () {},
                child: Text(
                  tr('favorites'),
                  style: TextStyle(
                      color: AppColors.whiteColor,
                      fontWeight: FontWeight.w400,
                      fontSize: size.shortestSide / 24),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                height: 1,
                margin: const EdgeInsets.symmetric(vertical: 8),
                color: AppColors.grey300Color,
              ),
              InkWell(
                onTap: () {},
                child: Column(
                  children: <Widget>[
                    Text(
                      tr('customer_service'),
                      style: TextStyle(
                          color: AppColors.whiteColor,
                          fontWeight: FontWeight.w400,
                          fontSize: size.shortestSide / 32),
                    ),
                    Text(
                      tr('terms_conditions'),
                      style: TextStyle(
                          color: AppColors.whiteColor,
                          fontWeight: FontWeight.w400,
                          fontSize: size.shortestSide / 32),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
