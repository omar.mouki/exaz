import 'dart:io';

import 'package:exaaz/UI/shared/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';

class CircularImageNetwork extends StatelessWidget {
  final String? imagePath;
  final double? imageSize;
  final bool? isActiveSign;
  final Function? imageOnTab;
  final Function? imageCategoryOnTab;
  final PickedFile? imageFile;
  final String? imageCategorySvgPath;
  final BoxFit? imgFit;
  final bool isSquare;
  final double? elevation;
  final Color? shadowColor;
  final String? placeHolderSvgPath;
  final double? placeHolderSize;
  const CircularImageNetwork(
      {Key? key,
      this.placeHolderSize,
      this.shadowColor,
      this.placeHolderSvgPath,
      this.elevation,
      this.isSquare = false,
      this.isActiveSign,
      this.imageCategorySvgPath,
      this.imageOnTab,
      this.imgFit,
      this.imagePath,
      this.imageSize,
      this.imageFile,
      this.imageCategoryOnTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        imageOnTab?.call();
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          imageFile == null
              ? Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        isSquare ? 25 : 200,
                      ),
                      border: Border.all(color: Colors.blueAccent)),
                  child: Material(
                    elevation: elevation ?? 0,
                    borderRadius: BorderRadius.circular(
                      isSquare ? 25 : 200,
                    ),
                    shadowColor:
                        shadowColor ?? AppColors.blackColor.withOpacity(0.2),
                    color: Colors.transparent,
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(
                            isSquare ? 25 : 200,
                          ),
                          child: Image.network(
                            imagePath ?? '',
                            width: imageSize,
                            height: imageSize,
                            fit: imgFit ?? BoxFit.cover,
                            errorBuilder: (context, error, stackTrace) =>
                                ImageErrorBuilder(
                                    placeHolderSize: placeHolderSize,
                                    elevation: elevation ?? 0,
                                    shadowColor: shadowColor ??
                                        AppColors.blackColor.withOpacity(0.2),
                                    imageSize: imageSize,
                                    placeHolderSvgPath: placeHolderSvgPath),
                            loadingBuilder: (context, child, loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                          null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                          loadingProgress.expectedTotalBytes!
                                      : null,
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : ClipRRect(
                  borderRadius: BorderRadius.circular(
                    isSquare ? 25 : 200,
                  ),
                  child: Image.file(
                    File(imageFile!.path),
                    fit: BoxFit.cover,
                    width: imageSize,
                    height: imageSize,
                  ),
                ),
          if (isActiveSign != null)
            Center(
              child: Container(
                margin: imageSize != null
                    ? EdgeInsetsDirectional.only(
                        end: imageSize! / 1.3, bottom: imageSize! / 1.5)
                    : EdgeInsets.all(0),
                width: size.shortestSide / 40,
                height: size.shortestSide / 40,
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: AppColors.whiteColor),
                    shape: BoxShape.circle,
                    color: isActiveSign ?? false
                        ? AppColors.greenColor
                        : AppColors.grey100Color),
              ),
            ),
          if (imageCategorySvgPath != null)
            InkWell(
              onTap: () {
                imageCategoryOnTab?.call();
              },
              child: Center(
                child: Container(
                  padding: EdgeInsets.all(size.width / 44),
                  margin: imageSize != null
                      ? EdgeInsetsDirectional.only(
                          start: imageSize! / 1.3, top: imageSize! / 1.5)
                      : EdgeInsets.all(0),
                  width: size.shortestSide / 10,
                  height: size.shortestSide / 10,
                  decoration: BoxDecoration(
                    color: AppColors.blueAccentColor,
                    borderRadius: BorderRadius.circular(
                      size.width / 28,
                    ),
                  ),
                  child: SvgPicture.asset(
                    imageCategorySvgPath!,
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}

class ImageErrorBuilder extends StatelessWidget {
  const ImageErrorBuilder(
      {Key? key,
      this.elevation,
      this.shadowColor,
      this.imageSize,
      this.placeHolderSize,
      this.placeHolderSvgPath,
      this.isSquare = false})
      : super(key: key);

  final double? elevation;
  final Color? shadowColor;
  final double? imageSize;
  final double? placeHolderSize;
  final String? placeHolderSvgPath;
  final bool isSquare;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Material(
      elevation: elevation ?? 0,
      shadowColor: shadowColor ?? AppColors.blackColor.withOpacity(0.6),
      child: Container(
        width: imageSize,
        height: imageSize,
        decoration: BoxDecoration(
          color: AppColors.grey25Color,
          borderRadius: BorderRadius.circular(
            isSquare ? size.width / 8 : 200,
          ),
        ),
        child: placeHolderSvgPath != null
            ? Center(
                child: SvgPicture.asset(
                placeHolderSvgPath!,
                width: placeHolderSize ?? size.width / 8,
                height: placeHolderSize ?? size.width / 8,
              ))
            : Container(
                color: AppColors.grey75Color,
                child: Center(
                    child: Image.asset(
                  'assets/pngs/error_image.png',
                  width: 56,
                  height: 56,
                )),
              ),
      ),
    );
  }
}

class FillImageNetwork extends StatelessWidget {
  final String imageUrl;
  final double imageSize;
  final Color loadingIndicatorColor;
  final BoxFit? imgFit;
  // final double errorImageSize;
  final double? radius;
  const FillImageNetwork(
      {Key? key,
      // this.errorImageSize,
      required this.imageSize,
      this.imgFit,
      this.radius,
      required this.imageUrl,
      this.loadingIndicatorColor = AppColors.whiteColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius ?? 0),
      child: Image.network(
        imageUrl,
        width: imageSize,
        height: imageSize,
        fit: imgFit ?? BoxFit.cover,
        errorBuilder: (context, error, stackTrace) => Container(
          color: AppColors.grey75Color,
          child: Center(
              child: Image.asset(
            'assets/pngs/error_image.png',
            width: 56,
            height: 56,
          )),
        ),
        loadingBuilder: (context, child, loadingProgress) {
          if (loadingProgress == null) return child;
          return Container(
            color: AppColors.whiteColor,
            alignment: Alignment.center,
            child: CircularProgressIndicator(
              value: loadingProgress.expectedTotalBytes != null
                  ? loadingProgress.cumulativeBytesLoaded /
                      loadingProgress.expectedTotalBytes!
                  : null,
            ),
          );
        },
      ),
    );
  }
}

class CustomNotificationImage extends StatelessWidget {
  final double radius;
  final double imageSize;
  final String imageUrl;
  final String? imageCategorySvgPath;
  final double? categorySvgSize;
  final Color? imageCategoryBackgroundColor;
  const CustomNotificationImage(
      {Key? key,
      this.imageCategoryBackgroundColor,
      this.categorySvgSize,
      required this.radius,
      required this.imageSize,
      required this.imageUrl,
      this.imageCategorySvgPath})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(radius),
          child: Image.network(
            imageUrl,
            width: imageSize,
            height: imageSize * 1.02,
            fit: BoxFit.cover,
            errorBuilder: (context, error, stackTrace) => Container(
              width: imageSize,
              height: imageSize * 1.02,
              color: AppColors.grey75Color,
              child: Center(
                  child: Image.asset(
                'assets/pngs/error_image.png',
                width: imageSize / 2,
                height: imageSize / 2,
              )),
            ),
            loadingBuilder: (context, child, loadingProgress) {
              if (loadingProgress == null) return child;
              return Container(
                color: AppColors.whiteColor,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  value: loadingProgress.expectedTotalBytes != null
                      ? loadingProgress.cumulativeBytesLoaded /
                          loadingProgress.expectedTotalBytes!
                      : null,
                ),
              );
            },
          ),
        ),
        if (imageCategorySvgPath != null)
          Center(
            child: Container(
              padding: EdgeInsets.all(size.width / 44),
              margin: EdgeInsetsDirectional.only(
                  start: imageSize / 1.3, top: imageSize / 1.8),
              width: imageSize / 2.5,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.grey500Color.withOpacity(0.2),
                      offset: const Offset(0, 7),
                      blurRadius: 11,
                    )
                  ],
                  color: imageCategoryBackgroundColor ?? AppColors.whiteColor,
                  shape: BoxShape.circle),
              child: SvgPicture.asset(
                imageCategorySvgPath!,
                width: categorySvgSize,
              ),
            ),
          )
      ],
    );
  }
}
