import 'package:flutter/material.dart';

import '../colors.dart';
import 'svg_icon_button.dart';

enum BottomNavBarActiveTab { Search, Add, Home, Notification, Turbo }

class MyBottomNavigationBar extends StatelessWidget {
  final BottomNavBarActiveTab activeTab;
  MyBottomNavigationBar({Key? key, this.activeTab = BottomNavBarActiveTab.Home})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.longestSide / 14,
      decoration: BoxDecoration(
        color: AppColors.darkGreyColor,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SvgIconButton(
            svgPath: 'assets/icons/navbar_profile.svg',
            width: size.longestSide / 28,
            onPressed: () {
              //  if (activeTab != BottomNavBarActiveTab.Search)
              // Navigator.of(context).pushReplacementNamed(
              //     GeneralRouting.SearchMainViewRouteName);
            },
          ),
          SvgIconButton(
            svgPath: 'assets/icons/navbar_fav.svg',
            width: size.longestSide / 30,
            onPressed: () {
              // if (isLoggedIn) {}
            },
          ),
          SvgIconButton(
            svgPath: 'assets/icons/navbar_add.svg',
            width: size.longestSide / 30,
            onPressed: () {
              // if (activeTab != BottomNavBarActiveTab.Home)
              // Navigator.of(context).pushReplacementNamed(
              //     GeneralRouting.MainHomeViewRouteName);
            },
          ),
          SvgIconButton(
            svgPath: 'assets/icons/navbar_paper.svg',
            width: size.longestSide / 30,
            onPressed: () {
              // if (activeTab != BottomNavBarActiveTab.Notification)
              // if (isLoggedIn) {
              //   Navigator.of(context).pushReplacementNamed(
              //       GeneralRouting.HomeNotificationsViewRouteName);
              // }
            },
          ),
          SvgIconButton(
            svgPath: 'assets/icons/navbar_home.svg',
            width: size.longestSide / 30,
            onLongPressed: () {},
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
