import 'package:exaaz/core/services/connectivity_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../colors.dart';

class NetworkSensitive extends StatelessWidget {
  final Widget child;
  final BuildContext contextx;

  // ignore: use_key_in_widget_constructors
  const NetworkSensitive({
    required this.contextx,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    // Get our connection status from the provider
    var connectionStatus = contextx.watch<ConnectivityStatus>();

    if (connectionStatus == ConnectivityStatus.Online) {
      return child;
    }

    return Stack(
      children: [
        child,
        Container(
          alignment: Alignment.center,
          color: AppColors.grey100Color,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.network_check,
                size: size.width / 4.5,
              ),
              Text(
                'Whoops!',
                style: TextStyle(
                    fontSize: size.width / 15, fontWeight: FontWeight.w700),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                'No internet connection Found,',
                style: TextStyle(
                    height: 1,
                    fontSize: size.width / 20,
                    fontWeight: FontWeight.normal),
              ),
              Text(
                'Please check your internet settings',
                style: TextStyle(
                    fontSize: size.width / 20, fontWeight: FontWeight.normal),
              ),
            ],
          ),
        )
      ],
    );
  }
}
