// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../UI/views/choose_activty_view/choose_activity_view.dart';
import '../UI/views/code_receive/code_receive_view.dart';
import '../UI/views/edit_profile/edit_profile_view.dart';
import '../UI/views/forget_password/forget_password_view.dart';
import '../UI/views/forget_password/reset_password_view.dart';
import '../UI/views/forget_password/verify_code_view.dart';
import '../UI/views/home/home_view.dart';
import '../UI/views/landing_view.dart';
import '../UI/views/pdf_view/pdf_view.dart';
import '../UI/views/photo_galary/photo_gallery_view.dart';
import '../UI/views/products/product_details/product_details_view.dart';
import '../UI/views/products/products_list/products_list_view.dart';
import '../UI/views/projects/my_projects_list/my_projects_list_view.dart';
import '../UI/views/projects/new_project/new_project_view.dart';
import '../UI/views/projects/project_details/project_details_view.dart';
import '../UI/views/projects/service_provider_project_list/service_provider_project_list_view.dart';
import '../UI/views/register_sp/register_sp_view.dart';
import '../UI/views/service_view.dart';
import '../UI/views/side_menu/side_menu_view.dart';
import '../UI/views/singin/signin_view.dart';
import '../UI/views/singup/signup_view.dart';
import '../UI/views/splash_screen/splash_view.dart';
import '../UI/views/store/store_details/store_details_view.dart';
import '../UI/views/store/store_list/stores_list_view.dart';
import '../UI/views/worker/worker_profile_view.dart';
import '../UI/views/worker/workers_view.dart';
import '../UI/views/worker_details/worker_details_view.dart';
import '../core/data/models/category.dart';
import '../core/data/models/service_request/service_request_model.dart';
import '../core/data/models/store.dart';
import '../core/data/models/worker.dart';

class Routes {
  static const String splashView = '/';
  static const String signInView = '/sign-in-view';
  static const String signUpView = '/sign-up-view';
  static const String landingView = '/landing-view';
  static const String codeReceiveView = '/code-receive-view';
  static const String registerSpView = '/register-sp-view';
  static const String homeView = '/home-view';
  static const String editProfileView = '/edit-profile-view';
  static const String sideMenuView = '/side-menu-view';
  static const String workersView = '/workers-view';
  static const String workerProfileView = '/worker-profile-view';
  static const String forgetPasswordView = '/forget-password-view';
  static const String resetPasswordView = '/reset-password-view';
  static const String verifyCodeView = '/verify-code-view';
  static const String serviceView = '/service-view';
  static const String workerDetailsView = '/worker-details-view';
  static const String chooseActivityView = '/choose-activity-view';
  static const String storeListView = '/store-list-view';
  static const String storeDetailsView = '/store-details-view';
  static const String productsListView = '/products-list-view';
  static const String productDetailsView = '/product-details-view';
  static const String myProjectsListView = '/my-projects-list-view';
  static const String projectDetailsView = '/project-details-view';
  static const String newProjectView = '/new-project-view';
  static const String photoGalleryView = '/photo-gallery-view';
  static const String pdfView = '/pdf-view';
  static const String serviceProviderProjectListView =
      '/service-provider-project-list-view';
  static const all = <String>{
    splashView,
    signInView,
    signUpView,
    landingView,
    codeReceiveView,
    registerSpView,
    homeView,
    editProfileView,
    sideMenuView,
    workersView,
    workerProfileView,
    forgetPasswordView,
    resetPasswordView,
    verifyCodeView,
    serviceView,
    workerDetailsView,
    chooseActivityView,
    storeListView,
    storeDetailsView,
    productsListView,
    productDetailsView,
    myProjectsListView,
    projectDetailsView,
    newProjectView,
    photoGalleryView,
    pdfView,
    serviceProviderProjectListView,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splashView, page: SplashView),
    RouteDef(Routes.signInView, page: SignInView),
    RouteDef(Routes.signUpView, page: SignUpView),
    RouteDef(Routes.landingView, page: LandingView),
    RouteDef(Routes.codeReceiveView, page: CodeReceiveView),
    RouteDef(Routes.registerSpView, page: RegisterSpView),
    RouteDef(Routes.homeView, page: HomeView),
    RouteDef(Routes.editProfileView, page: EditProfileView),
    RouteDef(Routes.sideMenuView, page: SideMenuView),
    RouteDef(Routes.workersView, page: WorkersView),
    RouteDef(Routes.workerProfileView, page: WorkerProfileView),
    RouteDef(Routes.forgetPasswordView, page: ForgetPasswordView),
    RouteDef(Routes.resetPasswordView, page: ResetPasswordView),
    RouteDef(Routes.verifyCodeView, page: VerifyCodeView),
    RouteDef(Routes.serviceView, page: ServiceView),
    RouteDef(Routes.workerDetailsView, page: WorkerDetailsView),
    RouteDef(Routes.chooseActivityView, page: ChooseActivityView),
    RouteDef(Routes.storeListView, page: StoreListView),
    RouteDef(Routes.storeDetailsView, page: StoreDetailsView),
    RouteDef(Routes.productsListView, page: ProductsListView),
    RouteDef(Routes.productDetailsView, page: ProductDetailsView),
    RouteDef(Routes.myProjectsListView, page: MyProjectsListView),
    RouteDef(Routes.projectDetailsView, page: ProjectDetailsView),
    RouteDef(Routes.newProjectView, page: NewProjectView),
    RouteDef(Routes.photoGalleryView, page: PhotoGalleryView),
    RouteDef(Routes.pdfView, page: PdfView),
    RouteDef(Routes.serviceProviderProjectListView,
        page: ServiceProviderProjectListView),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    SplashView: (data) {
      var args = data.getArgs<SplashViewArguments>(
        orElse: () => SplashViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => SplashView(key: args.key),
        settings: data,
      );
    },
    SignInView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => const SignInView(),
        settings: data,
      );
    },
    SignUpView: (data) {
      var args = data.getArgs<SignUpViewArguments>(
        orElse: () => SignUpViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => SignUpView(key: args.key),
        settings: data,
      );
    },
    LandingView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => LandingView(),
        settings: data,
      );
    },
    CodeReceiveView: (data) {
      var args = data.getArgs<CodeReceiveViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => CodeReceiveView(
          key: args.key,
          phoneNumber: args.phoneNumber,
        ),
        settings: data,
      );
    },
    RegisterSpView: (data) {
      var args = data.getArgs<RegisterSpViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => RegisterSpView(
          key: args.key,
          typeId: args.typeId,
        ),
        settings: data,
      );
    },
    HomeView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => const HomeView(),
        settings: data,
      );
    },
    EditProfileView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => EditProfileView(),
        settings: data,
      );
    },
    SideMenuView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => SideMenuView(),
        settings: data,
      );
    },
    WorkersView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => WorkersView(),
        settings: data,
      );
    },
    WorkerProfileView: (data) {
      var args = data.getArgs<WorkerProfileViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => WorkerProfileView(
          key: args.key,
          isUpdate: args.isUpdate,
        ),
        settings: data,
      );
    },
    ForgetPasswordView: (data) {
      var args = data.getArgs<ForgetPasswordViewArguments>(
        orElse: () => ForgetPasswordViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => ForgetPasswordView(key: args.key),
        settings: data,
      );
    },
    ResetPasswordView: (data) {
      var args = data.getArgs<ResetPasswordViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => ResetPasswordView(
          key: args.key,
          phoneNumber: args.phoneNumber,
          code: args.code,
        ),
        settings: data,
      );
    },
    VerifyCodeView: (data) {
      var args = data.getArgs<VerifyCodeViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => VerifyCodeView(
          key: args.key,
          phoneNumber: args.phoneNumber,
        ),
        settings: data,
      );
    },
    ServiceView: (data) {
      var args = data.getArgs<ServiceViewArguments>(
        orElse: () => ServiceViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => ServiceView(key: args.key),
        settings: data,
      );
    },
    WorkerDetailsView: (data) {
      var args = data.getArgs<WorkerDetailsViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => WorkerDetailsView(
          key: args.key,
          worker: args.worker,
        ),
        settings: data,
      );
    },
    ChooseActivityView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => const ChooseActivityView(),
        settings: data,
      );
    },
    StoreListView: (data) {
      var args = data.getArgs<StoreListViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => StoreListView(
          key: args.key,
          category: args.category,
        ),
        settings: data,
      );
    },
    StoreDetailsView: (data) {
      var args = data.getArgs<StoreDetailsViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => StoreDetailsView(
          key: args.key,
          stoerId: args.stoerId,
        ),
        settings: data,
      );
    },
    ProductsListView: (data) {
      var args = data.getArgs<ProductsListViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => ProductsListView(
          key: args.key,
          store: args.store,
        ),
        settings: data,
      );
    },
    ProductDetailsView: (data) {
      var args = data.getArgs<ProductDetailsViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => ProductDetailsView(
          key: args.key,
          productId: args.productId,
        ),
        settings: data,
      );
    },
    MyProjectsListView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => const MyProjectsListView(),
        settings: data,
      );
    },
    ProjectDetailsView: (data) {
      var args = data.getArgs<ProjectDetailsViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => ProjectDetailsView(
          key: args.key,
          id: args.id,
          serviceRequestModel: args.serviceRequestModel,
        ),
        settings: data,
      );
    },
    NewProjectView: (data) {
      var args = data.getArgs<NewProjectViewArguments>(
        orElse: () => NewProjectViewArguments(),
      );
      return CupertinoPageRoute<dynamic>(
        builder: (context) => NewProjectView(
          key: args.key,
          serviceRequestModel: args.serviceRequestModel,
        ),
        settings: data,
      );
    },
    PhotoGalleryView: (data) {
      var args = data.getArgs<PhotoGalleryViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => PhotoGalleryView(
          key: args.key,
          imageUrl: args.imageUrl,
        ),
        settings: data,
      );
    },
    PdfView: (data) {
      var args = data.getArgs<PdfViewArguments>(nullOk: false);
      return CupertinoPageRoute<dynamic>(
        builder: (context) => PdfView(
          key: args.key,
          pdfUrl: args.pdfUrl,
        ),
        settings: data,
      );
    },
    ServiceProviderProjectListView: (data) {
      return CupertinoPageRoute<dynamic>(
        builder: (context) => const ServiceProviderProjectListView(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// SplashView arguments holder class
class SplashViewArguments {
  final Key? key;
  SplashViewArguments({this.key});
}

/// SignUpView arguments holder class
class SignUpViewArguments {
  final Key? key;
  SignUpViewArguments({this.key});
}

/// CodeReceiveView arguments holder class
class CodeReceiveViewArguments {
  final Key? key;
  final String phoneNumber;
  CodeReceiveViewArguments({this.key, required this.phoneNumber});
}

/// RegisterSpView arguments holder class
class RegisterSpViewArguments {
  final Key? key;
  final int typeId;
  RegisterSpViewArguments({this.key, required this.typeId});
}

/// WorkerProfileView arguments holder class
class WorkerProfileViewArguments {
  final Key? key;
  final bool isUpdate;
  WorkerProfileViewArguments({this.key, required this.isUpdate});
}

/// ForgetPasswordView arguments holder class
class ForgetPasswordViewArguments {
  final Key? key;
  ForgetPasswordViewArguments({this.key});
}

/// ResetPasswordView arguments holder class
class ResetPasswordViewArguments {
  final Key? key;
  final String phoneNumber;
  final String code;
  ResetPasswordViewArguments(
      {this.key, required this.phoneNumber, required this.code});
}

/// VerifyCodeView arguments holder class
class VerifyCodeViewArguments {
  final Key? key;
  final String phoneNumber;
  VerifyCodeViewArguments({this.key, required this.phoneNumber});
}

/// ServiceView arguments holder class
class ServiceViewArguments {
  final Key? key;
  ServiceViewArguments({this.key});
}

/// WorkerDetailsView arguments holder class
class WorkerDetailsViewArguments {
  final Key? key;
  final WorkerModel worker;
  WorkerDetailsViewArguments({this.key, required this.worker});
}

/// StoreListView arguments holder class
class StoreListViewArguments {
  final Key? key;
  final Category category;
  StoreListViewArguments({this.key, required this.category});
}

/// StoreDetailsView arguments holder class
class StoreDetailsViewArguments {
  final Key? key;
  final int stoerId;
  StoreDetailsViewArguments({this.key, required this.stoerId});
}

/// ProductsListView arguments holder class
class ProductsListViewArguments {
  final Key? key;
  final Store store;
  ProductsListViewArguments({this.key, required this.store});
}

/// ProductDetailsView arguments holder class
class ProductDetailsViewArguments {
  final Key? key;
  final int productId;
  ProductDetailsViewArguments({this.key, required this.productId});
}

/// ProjectDetailsView arguments holder class
class ProjectDetailsViewArguments {
  final Key? key;
  final int id;
  final ServiceRequestModel serviceRequestModel;
  ProjectDetailsViewArguments(
      {this.key, required this.id, required this.serviceRequestModel});
}

/// NewProjectView arguments holder class
class NewProjectViewArguments {
  final Key? key;
  final ServiceRequestModel? serviceRequestModel;
  NewProjectViewArguments({this.key, this.serviceRequestModel});
}

/// PhotoGalleryView arguments holder class
class PhotoGalleryViewArguments {
  final Key? key;
  final String imageUrl;
  PhotoGalleryViewArguments({this.key, required this.imageUrl});
}

/// PdfView arguments holder class
class PdfViewArguments {
  final Key? key;
  final String pdfUrl;
  PdfViewArguments({this.key, required this.pdfUrl});
}
