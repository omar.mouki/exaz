import 'package:exaaz/core/data/repository/product_repository.dart';
import 'package:exaaz/core/data/repository/service_provider_repository.dart';
import 'package:exaaz/core/data/repository/service_request_repository.dart';
import 'package:exaaz/core/data/repository/store_repository.dart';
import 'package:exaaz/core/data/repository/worker_repository.dart';
import 'package:exaaz/core/services/device_info_service.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked_services/stacked_services.dart';

import '../core/data/models/worker_profile_data.dart';
import '../core/data/repository/account_repository.dart';
import '../core/data/repository/address_repository.dart';
import '../core/data/repository/auth_repository.dart';
import '../core/data/repository/change_request_repository.dart';
import '../core/data/repository/shared_prefrence_repository.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  final deviceInfoInstance = await DeviceInfoService.getInstance();
  // String deviceId = await DeviceId.getID;

  // PackageInfo packageInfo = await PackageInfo.fromPlatform();
  locator.registerLazySingleton(() => sharedPreferences);
  locator.registerLazySingleton<DeviceInfoService>(() => deviceInfoInstance);
  //locator.registerLazySingleton(() => packageInfo);
  locator.registerLazySingleton(() => SharedPreferencesRepository());
  locator.registerLazySingleton(() => AuthenticationRepository());
  locator.registerLazySingleton(() => AddressRepository());
  locator.registerLazySingleton(() => ChangeRequestRepository());
  locator.registerLazySingleton(() => AccountRepository());
  locator.registerLazySingleton(() => ServiceProviderRepository());
  locator.registerLazySingleton(() => WorkersRepository());
  locator.registerLazySingleton(() => StoreRepository());
  locator.registerLazySingleton(() => ProductRepository());
  locator.registerLazySingleton(() => WorkerProfileData());
  locator.registerLazySingleton(() => ServiceRequestRepository());
  //locator.registerLazySingleton(() => DataConnectionChecker());
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
  locator.registerLazySingleton<DialogService>(() => DialogService());
}
