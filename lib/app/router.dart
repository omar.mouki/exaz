import 'package:exaaz/UI/views/pdf_view/pdf_view.dart';
import 'package:exaaz/UI/views/photo_galary/photo_gallery_view.dart';
import 'package:exaaz/UI/views/choose_activty_view/choose_activity_view.dart';
import 'package:exaaz/UI/views/code_receive/code_receive_view.dart';
import 'package:exaaz/UI/views/edit_profile/edit_profile_view.dart';
import 'package:exaaz/UI/views/forget_password/forget_password_view.dart';
import 'package:exaaz/UI/views/forget_password/reset_password_view.dart';
import 'package:exaaz/UI/views/forget_password/verify_code_view.dart';
import 'package:exaaz/UI/views/home/home_view.dart';
import 'package:exaaz/UI/views/landing_view.dart';
import 'package:exaaz/UI/views/products/product_details/product_details_view.dart';
import 'package:exaaz/UI/views/products/products_list/products_list_view.dart';
import 'package:exaaz/UI/views/projects/my_projects_list/my_projects_list_view.dart';
import 'package:exaaz/UI/views/projects/new_project/new_project_view.dart';
import 'package:exaaz/UI/views/projects/project_details/project_details_view.dart';

import 'package:exaaz/UI/views/projects/service_provider_project_list/service_provider_project_list_view.dart';
import 'package:exaaz/UI/views/register_sp/register_sp_view.dart';
import 'package:exaaz/UI/views/service_view.dart';
import 'package:exaaz/UI/views/side_menu/side_menu_view.dart';
import 'package:exaaz/UI/views/singin/signin_view.dart';
import 'package:exaaz/UI/views/singup/signup_view.dart';
import 'package:exaaz/UI/views/splash_screen/splash_view.dart';
import 'package:exaaz/UI/views/store/store_details/store_details_view.dart';
import 'package:exaaz/UI/views/store/store_list/stores_list_view.dart';
import 'package:exaaz/UI/views/worker/worker_profile_view.dart';
import 'package:exaaz/UI/views/worker/workers_view.dart';
import 'package:exaaz/UI/views/worker_details/worker_details_view.dart';
import 'package:stacked/stacked_annotations.dart';

@StackedApp(
  routes: [
    CupertinoRoute(
      initial: true,
      page: SplashView,
    ),
    CupertinoRoute(
      page: SignInView,
    ),
    CupertinoRoute(
      page: SignUpView,
    ),
    CupertinoRoute(
      page: LandingView,
    ),
    CupertinoRoute(
      page: CodeReceiveView,
    ),
    CupertinoRoute(
      page: RegisterSpView,
    ),
    CupertinoRoute(
      page: HomeView,
    ),
    CupertinoRoute(
      page: EditProfileView,
    ),
    CupertinoRoute(
      page: SideMenuView,
    ),
    CupertinoRoute(
      page: WorkersView,
    ),
    CupertinoRoute(
      page: WorkerProfileView,
    ),
    CupertinoRoute(
      page: ForgetPasswordView,
    ),
    CupertinoRoute(page: ResetPasswordView),
    CupertinoRoute(
      page: VerifyCodeView,
    ),
    CupertinoRoute(
      page: ServiceView,
    ),
    CupertinoRoute(
      page: WorkerDetailsView,
    ),
    CupertinoRoute(
      page: ChooseActivityView,
    ),
    CupertinoRoute(
      page: StoreListView,
    ),
    CupertinoRoute(
      page: StoreDetailsView,
    ),
    CupertinoRoute(
      page: ProductsListView,
    ),
    CupertinoRoute(
      page: ProductDetailsView,
    ),
    CupertinoRoute(
      page: MyProjectsListView,
    ),
    CupertinoRoute(
      page: ProjectDetailsView,
    ),
    CupertinoRoute(
      page: NewProjectView,
    ),
    CupertinoRoute(
      page: PhotoGalleryView,
    ),
    CupertinoRoute(
      page: PdfView,
    ),
    CupertinoRoute(
      page: ServiceProviderProjectListView,
    ),
  ],
)
class AppRouter {}
