import 'package:bot_toast/bot_toast.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:exaaz/app/router.router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import 'app_view_model.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final botToastBuilder = BotToastInit();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return ViewModelBuilder<AppViewModel>.reactive(
      viewModelBuilder: () => AppViewModel(),
      onModelReady: (model) {},
      builder: (context, model, child) {
        return
            // StreamProvider<ConnectivityStatus>.value(
            //   value: ConnectivityService().connectionStatusController.stream,
            //   initialData: ConnectivityStatus.Online,
            //   child:
            MaterialApp(
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,

          onGenerateRoute: StackedRouter().onGenerateRoute,
          navigatorKey: StackedService.navigatorKey,
          title: 'New App',
          builder: (context, child) {
            child = ExtendedNavigator<StackedRouter>(
              name: 'app_main_router',
              observers: [
                BotToastNavigatorObserver(),
              ],
              navigatorKey: StackedService.navigatorKey,
              router: StackedRouter(),
            );
            child = botToastBuilder(context, child);
            return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                child: Stack(
                  children: [child],
                ));
          },
          // darkTheme: ThemeData(
          //   fontFamily: 'Poppins',
          //   primaryColor: Color(0xFF2C2C2C),
          //   brightness: Brightness.dark,
          //   scaffoldBackgroundColor: Color(0xFF2C2C2C),
          //   accentColor: AppColors.mainDarkColor(1),
          //   hintColor: AppColors.secondDarkColor(1),
          //   focusColor: AppColors.accentDarkColor(1),
          // ),
          theme: ThemeData(
            fontFamily: 'obada',
          ),
          // theme: ThemeData(
          //     fontFamily: GoogleFonts.mada().fontFamily,
          //     primaryColor: AppColors.mainBlueColor3,
          //     brightness: Brightness.light,
          //     accentColor: AppColors.mainAmberColor3,
          //     focusColor: Color(0xFF8C98A8),
          //     hintColor: Color(0xFF4AA045),
          //     canvasColor: Colors.blue[50]),
          debugShowCheckedModeBanner: false,
          // ),
        );
      },
    );
  }
}
